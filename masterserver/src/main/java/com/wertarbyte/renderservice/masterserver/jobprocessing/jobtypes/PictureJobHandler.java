/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes;

import com.wertarbyte.renderservice.api.RenderJobStatus;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.util.RenderDump;
import com.wertarbyte.renderservice.util.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Creates pictures out of a task's render dump.
 */
class PictureJobHandler extends JobHandler {
    private static Logger LOGGER = LogManager.getLogger(PictureJobHandler.class);

    @Override
    public List<RenderTask> createTasks(RenderJob job, Object data) throws IOException {
        try {
            return Arrays.asList(MasterMain.getDatabase().addTask(
                    job, job.getWorld(), job.getX(), job.getY(), job.getZ(), job.getPitch(), job.getYaw(),
                    job.isFocusSet() ? job.getFocus() : null, job.getChunkRadius(), job.getWidth(), job.getHeight(),
                    job.getPreset(), MasterMain.getDefaults().getTargetSpp()));
        } catch (SQLException e) {
            throw new IOException("Could not create task", e);
        }
    }

    @Override
    public List<RenderTask> createForkedTasks(RenderJob newJob, RenderJob originalJob) throws IOException {
        return createTasks(newJob, null);
    }

    @Override
    public void onTaskUpdated(RenderTask task) throws IOException {
        RenderDump dump = RenderDump.loadDump(MasterMain.getSceneFileProvider().getDumpFile(task));

        try {
            File pictureFile = MasterMain.getSceneFileProvider().getLatestPictureFile(task);
            dump.savePicture(pictureFile, MasterMain.getPresetDataProvider().getPreset(task.getPreset()));
            LOGGER.info("Updated image for " + task);

            RenderJob job = task.getJob();
            File jobPictureFile = MasterMain.getSceneFileProvider().getLatestPictureFile(job);
            FileUtil.copy(pictureFile, jobPictureFile);
            LOGGER.info("Updated image for " + job);

            try {
                MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.READY);
                MasterMain.getDatabase().setJobLastPictureDate(job, new Date());
            } catch (SQLException e) {
                LOGGER.warn("Could not update status of " + job, e);
            }
        } catch (IOException e) {
            LOGGER.warn("Could not create new picture for " + task, e);
        }
    }

    @Override
    public void onAllTasksFinished(RenderJob job) throws IOException {
        try {
            MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.FINISHED);
        } catch (SQLException e) {
            LOGGER.warn("Could not update status of " + job, e);
        }
    }
}
