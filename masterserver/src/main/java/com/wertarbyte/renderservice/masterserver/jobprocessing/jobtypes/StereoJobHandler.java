/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes;

import com.wertarbyte.renderservice.api.RenderJobStatus;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.util.RenderDump;
import com.wertarbyte.renderservice.masterserver.util.Vector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Creates a stereoscopic 3d-image from two {@link com.wertarbyte.renderservice.api.RenderJob}s.
 */
class StereoJobHandler extends JobHandler {
    private static Logger LOGGER = LogManager.getLogger(StereoJobHandler.class);

    @Override
    public List<RenderTask> createTasks(RenderJob job, Object data) throws IOException {
        RenderTask left;
        try {
            left = MasterMain.getDatabase().addTask(
                    job, job.getWorld(), job.getX(), job.getY(), job.getZ(), job.getPitch(), job.getYaw(),
                    job.isFocusSet() ? job.getFocus() : null, job.getChunkRadius(), job.getWidth(), job.getHeight(),
                    job.getPreset(), MasterMain.getDefaults().getTargetSpp());
        } catch (SQLException e) {
            throw new IOException("Could not create left task", e);
        }

        Vector direction = new Vector((job.getPitch() + 90) * Math.PI / 180, (job.getYaw() + 90) * Math.PI / 180);
        Vector toRight = new Vector(0, 1, 0).cross(direction).scale(-0.5).normalize();
        double x = job.getX() + toRight.getX();
        double y = job.getY() + toRight.getY();
        double z = job.getZ() + toRight.getZ();

        RenderTask right;
        try {
            right = MasterMain.getDatabase().addTask(
                    job, job.getWorld(), x, y, z, job.getPitch(), job.getYaw(),
                    job.isFocusSet() ? job.getFocus() : null, job.getChunkRadius(), job.getWidth(), job.getHeight(),
                    job.getPreset(), MasterMain.getDefaults().getTargetSpp());
        } catch (SQLException e) {
            throw new IOException("Could not create right task", e);
        }

        return Arrays.asList(left, right);
    }

    @Override
    public List<RenderTask> createForkedTasks(RenderJob newJob, RenderJob originalJob) throws IOException {
        return createTasks(newJob, null);
    }

    @Override
    public void onTaskUpdated(RenderTask task) throws IOException {
        RenderJob job = task.getJob();
        List<RenderTask> tasks = job.getTasks();
        File targetFile = MasterMain.getSceneFileProvider().getLatestPictureFile(job);

        try {
            RenderDump dump = RenderDump.loadDump(MasterMain.getSceneFileProvider().getDumpFile(task));
            File pictureFile = MasterMain.getSceneFileProvider().getLatestPictureFile(task);
            dump.savePicture(pictureFile, MasterMain.getPresetDataProvider().getPreset(task.getPreset()));
            LOGGER.info("Updated image for " + task);
        } catch (IOException e) {
            LOGGER.warn("Could not update image for " + task, e);
        }

        try {
            File leftFile = MasterMain.getSceneFileProvider().getDumpFile(tasks.get(0));
            File rightFile = MasterMain.getSceneFileProvider().getDumpFile(tasks.get(1));
            if (!leftFile.exists() || !rightFile.exists()) {
                LOGGER.info("3D picture not updated, at least one image is still missing");
                return;
            }

            RenderDump leftDump = RenderDump.loadDump(leftFile);
            RenderDump rightDump = RenderDump.loadDump(rightFile);

            double[] samplesL = leftDump.getSamples();
            double[] samplesR = rightDump.getSamples();

            RenderDump stereoscopic = new RenderDump(leftDump.getWidth(), leftDump.getHeight());
            double[] samples = stereoscopic.getSamples();

            for (int y = 0; y < stereoscopic.getHeight(); y++) {
                for (int x = 0; x < stereoscopic.getWidth(); x++) {
                    samples[(y * stereoscopic.getWidth() + x) * 3] = samplesL[(y * stereoscopic.getWidth() + x) * 3];
                    samples[(y * stereoscopic.getWidth() + x) * 3 + 1] = samplesR[(y * stereoscopic.getWidth() + x) * 3 + 1];
                    samples[(y * stereoscopic.getWidth() + x) * 3 + 2] = samplesR[(y * stereoscopic.getWidth() + x) * 3 + 2];
                }
            }

            stereoscopic.savePicture(targetFile, MasterMain.getPresetDataProvider().getPreset(job.getPreset()));
            try {
                MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.READY);
                MasterMain.getDatabase().setJobLastPictureDate(job, new Date());
            } catch (SQLException e) {
                LOGGER.warn("Could not update status of " + job, e);
            }
            LOGGER.info("Updated 3D picture for " + job);
        } catch (FileNotFoundException e) {
            LOGGER.info("Left/right images not found", e);
        }
    }

    @Override
    public void onAllTasksFinished(RenderJob job) throws IOException {
        try {
            MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.FINISHED);
        } catch (SQLException e) {
            LOGGER.warn("Could not update status of " + job, e);
        }
    }
}
