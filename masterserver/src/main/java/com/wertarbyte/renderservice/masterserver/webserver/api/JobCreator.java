/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.api;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import com.wertarbyte.renderservice.api.JobConfiguration;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.api.VideoJobConfiguration;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.webserver.util.apiversion.RequirePlugin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.sql.SQLException;

@RequirePlugin
@Path("/jobs")
public class JobCreator {
    private static final Logger LOGGER = LogManager.getLogger(JobCreator.class);
    private static final Genson GENSON = new GensonBuilder().useConstructorWithArguments(true).create();

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public RenderJob submitJob(@FormDataParam("data") String jsonData,
                               @FormDataParam("regions") InputStream regions) {
        try {
            LOGGER.info("Creating new job...");
            JobConfiguration config = GENSON.deserialize(jsonData, JobConfiguration.class);
            if (!config.getPreset().isEmpty() && !MasterMain.getPresetDataProvider().presetExists(config.getPreset())) {
                throw new BadRequestException("Preset '" + config.getPreset() + "' doesn't exist.");
            }
            RenderJob job;
            try {
                job = MasterMain.getDatabase().addJob(config, MasterMain.getDefaults().getWidth(), MasterMain.getDefaults().getHeight());
            } catch (SQLException e) {
                LOGGER.error("Could not create job", e);
                throw new InternalServerErrorException(e);
            }

            if (job.getType() == RenderJobType.VIDEO || job.getType() == RenderJobType.SPHERICAL_VIDEO) {
                VideoJobConfiguration videoConfig = GENSON.deserialize(jsonData, VideoJobConfiguration.class);
                MasterMain.getJobProcessor().addNewJob(job, videoConfig, regions);
            } else {
                MasterMain.getJobProcessor().addNewJob(job, config, regions);
            }

            LOGGER.info(String.format("%s created %s", job.getCreator(), job));
            return job;
        } catch (Exception e) {
            LOGGER.error("Could not create job", e);
            throw new InternalServerErrorException(e);
        }
    }
}
