/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.api;

import com.wertarbyte.renderservice.api.PlayerData;
import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collections;

@Path("/players")
public class PlayerController {
    private static final Logger LOGGER = LogManager.getLogger(PlayerController.class);

    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public PlayerData getPlayer(@PathParam("name") String name) {
        try {
            return new PlayerData(Collections.<RenderJob>unmodifiableList(MasterMain.getDatabase().getJobsByCreator(name)));
        } catch (SQLException e) {
            LOGGER.error("Could not get player data for " + name, e);
            throw new InternalServerErrorException(e);
        }
    }
}
