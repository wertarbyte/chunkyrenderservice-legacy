/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver;

import com.wertarbyte.consoleapplication.ConsoleApplication;
import com.wertarbyte.consoleapplication.commands.Command;
import com.wertarbyte.consoleapplication.commands.annotation.CommandHandler;
import com.wertarbyte.renderservice.libchunky.remote.UnmodifiableRemoteRenderer;
import com.wertarbyte.renderservice.masterserver.accounts.AccountManager;
import com.wertarbyte.renderservice.masterserver.data.DefaultsProvider;
import com.wertarbyte.renderservice.masterserver.data.PresetDataProvider;
import com.wertarbyte.renderservice.masterserver.data.SceneFileProvider;
import com.wertarbyte.renderservice.masterserver.database.Database;
import com.wertarbyte.renderservice.masterserver.jobprocessing.JobProcessor;
import com.wertarbyte.renderservice.masterserver.jobprocessing.scheduler.ScheduledTask;
import com.wertarbyte.renderservice.masterserver.stats.RendererStatistics;
import com.wertarbyte.renderservice.masterserver.webserver.util.CORSResponseFilter;
import com.wertarbyte.renderservice.masterserver.webserver.util.GensonProvider;
import com.wertarbyte.renderservice.masterserver.webserver.util.apiversion.RenderServiceApiVersionFilter;
import com.wertarbyte.util.config.Settings;
import com.wertarbyte.util.config.YamlSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.message.DeflateEncoder;
import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;
import org.glassfish.jersey.simple.SimpleContainerFactory;

import javax.ws.rs.core.UriBuilder;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;

public class MasterMain extends ConsoleApplication {
    /**
     * The supported renderer version. Incremented on breaking changes.
     */
    public static final int SUPPORTED_RENDERER_VERSION = 5;

    /**
     * The supported plugin version. Incremented on breaking changes.
     */
    public static final int SUPPORTED_PLUGIN_VERSION = 1;

    private static final Logger LOGGER = LogManager.getLogger(MasterMain.class);
    private static final String SETTINGS_FILENAME = "MasterSettings.yml";
    private static Database database;
    private Closeable webserver;

    private static PresetDataProvider presetDataProvider;
    private static SceneFileProvider sceneFileProvider;
    private static DefaultsProvider defaultsProvider;
    private static AccountManager accountManager;

    private static RendererStatistics rendererStatistics;

    private static JobProcessor jobProcessor;

    public MasterMain() {
        super(new YamlSettings());
        addCommandHandler(new JobCommandHandler());
    }

    public static PresetDataProvider getPresetDataProvider() {
        return presetDataProvider;
    }

    public static SceneFileProvider getSceneFileProvider() {
        return sceneFileProvider;
    }

    public static DefaultsProvider getDefaults() {
        return defaultsProvider;
    }

    public static RendererStatistics getRendererStatistics() {
        return rendererStatistics;
    }

    public static JobProcessor getJobProcessor() {
        return jobProcessor;
    }

    public static AccountManager getAccountManager() {
        return accountManager;
    }

    @Override
    public void start() {
        LOGGER.info("ChunkyRenderService Master Server - Copyright (C) 2014-2015 Wertarbyte GbR");
        LOGGER.info("Starting...");
        super.start();
        loadSettings();

        String host = getSettings().getString("database.host");
        int port = getSettings().getInteger("database.port", 3306);
        String user = getSettings().getString("database.user");
        String password = getSettings().getString("database.password");
        String database = getSettings().getString("database.database");

        MasterMain.database = new Database(host, port, database, user, password);

        presetDataProvider = new PresetDataProvider(new File(getSettings().getString("paths.presets")));
        sceneFileProvider = new SceneFileProvider(new File(getSettings().getString("paths.jobs")));
        defaultsProvider = new DefaultsProvider(getSettings().getInteger("default.spp", 5000),
                getSettings().getInteger("default.width", 640), getSettings().getInteger("default.height", 360));
        accountManager = new AccountManager();

        rendererStatistics = new RendererStatistics();

        jobProcessor = new JobProcessor(
                getSettings().getInteger("initializingThreads", 2),
                getSettings().getInteger("rendererPoolPort", 19876)
        );

        URI baseUri = UriBuilder.fromUri("http://" + getSettings().getString("webserver.address", "localhost") + "/")
                .port(getSettings().getInteger("webserver.port", 9876))
                .build();
        ResourceConfig config = new ResourceConfig()
                .packages("com.wertarbyte.renderservice.masterserver.webserver.api")
                .register(GensonProvider.class)
                .register(CORSResponseFilter.class)
                .registerClasses(
                        EncodingFilter.class,
                        GZipEncoder.class,
                        DeflateEncoder.class,
                        MultiPartFeature.class,
                        RenderServiceApiVersionFilter.class);
        webserver = SimpleContainerFactory.create(baseUri, config, 50, 5);

        //Initialize uninitialized tasks
        try {
            jobProcessor.addUninitializedTasks(getDatabase().getNewTasks());
        } catch (SQLException e) {
            LOGGER.error("Could not get new jobs", e);
        }

        //Add unfinished tasks to scheduler
        try {
            jobProcessor.addUnfinishedTasks(getDatabase().getUnfinishedTasks());
        } catch (SQLException e) {
            LOGGER.error("Could not get unfinished tasks", e);
        }

        LOGGER.info("Started");
    }

    @CommandHandler(value = "stop", description = "Stop the server")
    @Override
    public void stop() {
        LOGGER.info("Stopping job processor (this might take a moment)...");
        jobProcessor.stop();

        try {
            getSettings().save(new File(SETTINGS_FILENAME));
        } catch (IOException e) {
            LOGGER.warn("Error while saving the settings");
        }
        super.stop();
        try {
            webserver.close();
        } catch (IOException e) {
            LOGGER.warn("An exception occurred while closing the webserver", e);
        }

        LOGGER.info("Server should stop now...");
    }

    public static Database getDatabase() {
        return database;
    }

    public static void main(String[] args) {
        MasterMain main = new MasterMain();
        main.start();
    }

    @Override
    protected void onCommandUnknown(Command command) {
        LOGGER.info("Unknown command. Use \"help\" to get a list of all commands.");
    }

    @CommandHandler(value = "help", description = "Show all commands")
    @Override
    public void printHelp() {
        LOGGER.info("Available commands:");
        for (CommandHandler handler : getCommandHandlers()) {
            LOGGER.info(handler.value() + "  -  " + handler.description());
        }
    }

    @CommandHandler(value = "stats", description = "Show statistics")
    public void showStatistics() {
        LOGGER.info("Renderer clients:    " + getRendererStatistics().getRenderersCount());
        LOGGER.info("Active renderers:    " + getRendererStatistics().getRunningRenderersCount());
        LOGGER.info("Chunky instances:    " + getRendererStatistics().getProcessCount());
        LOGGER.info("Threads:             " + getRendererStatistics().getThreadCount());
        LOGGER.info("kSamples per second: " + String.format("%.2f", getRendererStatistics().getSamplesPerSecond() / 1000));
    }

    @CommandHandler(value = "tasks", description = "Show all scheduled tasks")
    public void showTasks() {
        for (ScheduledTask task : jobProcessor.getScheduledTasks()) {
            LOGGER.info(task.getTask() + ": " + task.getUnscheduledSpp() + " unscheduled SPP");
        }
    }

    @CommandHandler(value = "running", description = "Show the progress of all currently running renderers")
    public void showRunningRenderers() {
        for (UnmodifiableRemoteRenderer task : jobProcessor.getPool().getActiveRenderers()) {
            LOGGER.info(String.format("Task %s (Job %s): %s",
                    task.getAdditionalData("taskId"),
                    task.getAdditionalData("jobId"),
                    task.getStatus()
            ));
        }
    }

    private void loadSettings() {
        LOGGER.info("Loading settings...");
        try {
            getSettings().load(new File(SETTINGS_FILENAME));
            LOGGER.info("Settings loaded.");
        } catch (FileNotFoundException e) {
            LOGGER.warn("Settings file not found, will generate default settings.");
            try {
                getSettings().save(new File(SETTINGS_FILENAME));
                LOGGER.info("Default settings saved.");
            } catch (IOException e1) {
                LOGGER.error("Error while saving default settings.");
            }
            stop();
        } catch (Settings.SettingsCorruptedException e) {
            LOGGER.error("The settings file is invalid.", e);
            stop();
        }
    }
}
