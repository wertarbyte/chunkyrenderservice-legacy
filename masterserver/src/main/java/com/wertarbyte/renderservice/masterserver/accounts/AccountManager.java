/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.accounts;

import com.wertarbyte.renderservice.api.Assignment;
import com.wertarbyte.renderservice.masterserver.MasterMain;

import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Manager for accounts and points.
 */
public class AccountManager {
    public int givePoints(String account, Assignment assignment) throws IOException {
        UUID uuid;
        if (account != null) {
            try {
                uuid = UUID.fromString(account);
            } catch (IllegalArgumentException e) {
                uuid = null;
            }
        } else {
            uuid = null;
        }

        try {
            int points = getPoints(assignment);
            if (uuid != null) {
                MasterMain.getDatabase().givePointsTo(uuid, points, assignment.getTask().getJobId());
            } else {
                MasterMain.getDatabase().givePointsToDefault(points, assignment.getTask().getJobId());
            }
            return points;
        } catch (SQLException e) {
            throw new IOException("Could not give points", e);
        }
    }

    private int getPoints(Assignment assignment) {
        return assignment.getSpp() * assignment.getTask().getWidth() * assignment.getTask().getHeight() / 7680000;
    }
}
