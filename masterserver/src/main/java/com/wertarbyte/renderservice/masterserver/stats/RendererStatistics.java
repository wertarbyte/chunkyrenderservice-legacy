/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.stats;

import com.wertarbyte.renderservice.api.RendererInfo;
import com.wertarbyte.renderservice.libchunky.UnmodifiableChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.remote.UnmodifiableRemoteRenderer;
import com.wertarbyte.renderservice.masterserver.MasterMain;

import java.util.*;

/**
 * Observer class for running renderer instances.
 */
public class RendererStatistics {
    public int getRunningRenderersCount() {
        Set<UUID> rendererIds = new HashSet<>();
        for (UnmodifiableRemoteRenderer renderer : MasterMain.getJobProcessor().getPool().getActiveRenderers()) {
            String clientId = renderer.getAdditionalData("clientId");
            rendererIds.add(clientId != null ? UUID.fromString(clientId) : UUID.randomUUID());
        }
        return rendererIds.size();
    }

    public int getRenderersCount() {
        Set<UUID> rendererIds = new HashSet<>();
        for (UnmodifiableRemoteRenderer renderer : MasterMain.getJobProcessor().getPool().getActiveRenderers()) {
            String clientId = renderer.getAdditionalData("clientId");
            rendererIds.add(clientId != null ? UUID.fromString(clientId) : UUID.randomUUID());
        }
        for (UnmodifiableRemoteRenderer renderer : MasterMain.getJobProcessor().getPool().getUnusedRenderers()) {
            String clientId = renderer.getAdditionalData("clientId");
            rendererIds.add(clientId != null ? UUID.fromString(clientId) : UUID.randomUUID());
        }
        return rendererIds.size();
    }

    public int getProcessCount() {
        return MasterMain.getJobProcessor().getPool().getActiveRenderersCount();
    }

    public int getThreadCount() {
        int threads = 0;
        for (UnmodifiableChunkyWrapper renderer : MasterMain.getJobProcessor().getPool().getActiveRenderers()) {
            threads += renderer.getThreadCount();
        }
        return threads;
    }

    public double getSamplesPerSecond() {
        double samplesPerSecond = 0;
        for (UnmodifiableChunkyWrapper renderer : MasterMain.getJobProcessor().getPool().getActiveRenderers()) {
            samplesPerSecond += renderer.getSamplesPerSecond();
        }
        return samplesPerSecond;
    }

    public Map<UUID, RendererInfo> getStatistics() {
        Map<UUID, RendererInfo> statistics = new HashMap<>();
        for (UnmodifiableRemoteRenderer renderer : MasterMain.getJobProcessor().getPool().getActiveRenderers()) {
            UUID id = renderer.getAdditionalData("clientId") != null ?
                    UUID.fromString(renderer.getAdditionalData("clientId")) : UUID.randomUUID();
            RendererInfo old = statistics.get(id);
            if (old != null) {
                statistics.put(id, new RendererInfo(
                        old.getSamplesPerSecond() + renderer.getSamplesPerSecond(),
                        old.getRunningProcesses() + 1,
                        old.getRunningThreads() + renderer.getThreadCount(),
                        old.getUnusedProcesses()));
            } else {
                statistics.put(id, new RendererInfo(renderer.getSamplesPerSecond(), 1, renderer.getThreadCount(), 0));
            }
        }

        for (UnmodifiableRemoteRenderer renderer : MasterMain.getJobProcessor().getPool().getUnusedRenderers()) {
            UUID id = renderer.getAdditionalData("clientId") != null ?
                    UUID.fromString(renderer.getAdditionalData("clientId")) : UUID.randomUUID();
            RendererInfo old = statistics.get(id);
            if (old != null) {
                statistics.put(id, new RendererInfo(
                        old.getSamplesPerSecond(),
                        old.getRunningProcesses(),
                        old.getRunningThreads(),
                        old.getUnusedProcesses() + 1));
            } else {
                statistics.put(id, new RendererInfo(0, 0, 0, 1));
            }
        }
        return statistics;
    }
}
