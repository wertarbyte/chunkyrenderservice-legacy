/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver;


import com.wertarbyte.consoleapplication.commands.CommandListener;
import com.wertarbyte.consoleapplication.commands.annotation.CommandHandler;
import com.wertarbyte.consoleapplication.commands.annotation.CommandParam;
import com.wertarbyte.renderservice.libchunky.UnmodifiableChunkyWrapper;
import com.wertarbyte.renderservice.masterserver.entities.JobStatus;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;

public class JobCommandHandler implements CommandListener {
    private static final Logger LOGGER = LogManager.getLogger(JobCommandHandler.class);

    @CommandHandler(value = "settarget <job> <spp>", description = "Set the SPP target of a job")
    public void setJobTarget(@CommandParam("job") int jobId, @CommandParam("spp") int targetSpp) {
        RenderJob job;
        try {
            job = MasterMain.getDatabase().getJobById(jobId);
        } catch (SQLException e) {
            LOGGER.warn(String.format("Could not get job %d", jobId), e);
            return;
        }
        if (job != null) {
            try {
                MasterMain.getJobProcessor().setJobSppTarget(job, targetSpp);
            } catch (IOException e) {
                LOGGER.error("Setting SPP target for " + job + " failed", e);
            }
        } else {
            LOGGER.info(String.format("Job #%d doesn't exist.", jobId));
        }
    }

    @CommandHandler(value = "fork <job> <width> <height> <preset>", description = "Fork a job")
    public void forkJob(@CommandParam("job") int jobId, @CommandParam("width") int width,
                        @CommandParam("height") int height, @CommandParam("preset") String preset) {
        RenderJob original;
        try {
            original = MasterMain.getDatabase().getJobById(jobId);
        } catch (SQLException e) {
            LOGGER.warn("Could not get job #" + jobId, e);
            return;
        }
        if (original != null) {
            try {
                MasterMain.getJobProcessor().forkJob(original, width, height, preset);
                LOGGER.info("Successfully forked " + original);
            } catch (IOException e) {
                LOGGER.error("Could not fork " + original, e);
            }
        } else {
            LOGGER.info(String.format("Job #%d doesn't exist.", jobId));
        }
    }

    @CommandHandler(value = "cancel <job>", description = "Cancel the job with the given ID")
    public void cancelJob(@CommandParam("job") int id) {
        RenderJob job;
        try {
            job = MasterMain.getDatabase().getJobById(id);
        } catch (SQLException e) {
            LOGGER.error("Could not get job #" + id, e);
            return;
        }

        try {
            MasterMain.getJobProcessor().cancelJob(job);
            LOGGER.info("Successfully canceled " + job);
        } catch (IOException e) {
            LOGGER.error("Canceling " + job + " failed", e);
        }
    }

    @CommandHandler(value = "list", description = "Show all initializing tasks")
    public void listInitializingTasks() {
        for (UnmodifiableChunkyWrapper task : MasterMain.getJobProcessor().getRunningInitializationProcesses()) {
            LOGGER.info(task.getStatus());
        }
    }

    @CommandHandler(value = "jobs", description = "Show all unfinished jobs")
    public void listUnfinishedJobs() {
        try {
            for (JobStatus status : MasterMain.getJobProcessor().getCurrentJobStatuses()) {
                LOGGER.info(String.format("%s: %.2f%% (%.2f%%)", status.getJob(), status.getProgressPercentage(),
                        status.getSafeProgressPercentage()));
            }
        } catch (IOException e) {
            LOGGER.error("Could not get job statuses", e);
        }
    }

    @CommandHandler(value = "reset <job>", description = "Reset a job")
    public void resetJob(@CommandParam("job") int jobId) {
        RenderJob job;
        try {
            job = MasterMain.getDatabase().getJobById(jobId);
            LOGGER.info("Successfully reset " + job);
        } catch (SQLException e) {
            LOGGER.warn("Could not get job #" + jobId, e);
            return;
        }
        if (job != null) {
            try {
                MasterMain.getJobProcessor().resetJob(job);
            } catch (IOException e) {
                LOGGER.error("Could not reset " + job, e);
            }
        } else {
            LOGGER.info(String.format("Job #%d doesn't exist.", jobId));
        }
    }
}
