/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes;

import com.wertarbyte.renderservice.api.RenderJobStatus;
import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.scene.ProjectionMode;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes.video.SequenceEncoder;
import com.wertarbyte.renderservice.masterserver.util.RenderDump;
import com.wertarbyte.renderservice.util.video.InterpolatedFrames;
import com.wertarbyte.renderservice.util.video.KeyframeList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Handles spherical video jobs.
 */
public class SphericalVideoJobHandler extends JobHandler {
    private static final Logger LOGGER = LogManager.getLogger(SphericalVideoJobHandler.class);
    private static final double FPS = 25;

    @Override
    public List<RenderTask> createTasks(RenderJob job, Object data) throws IOException {
        if (!(data instanceof KeyframeList)) {
            throw new IllegalArgumentException("data is not a keyframe list");
        }
        KeyframeList keyframes = (KeyframeList) data;
        InterpolatedFrames frames = keyframes.interpolate();

        List<RenderTask> tasks = new ArrayList<>();
        try {
            for (double t = 0; t <= frames.getLength(); t += 1.0 / FPS) {
                tasks.add(MasterMain.getDatabase().addTask(
                        job, job.getWorld(),
                        frames.getX(t), frames.getY(t), frames.getZ(t),
                        frames.getPitch(t), frames.getYaw(t), job.isFocusSet() ? job.getFocus() : null,
                        job.getChunkRadius(), job.getWidth(), job.getHeight(), job.getPreset(),
                        MasterMain.getDefaults().getTargetSpp()));
            }
        } catch (SQLException e) {
            LOGGER.error("Could not create tasks", e);
        }

        return tasks;
    }

    @Override
    public List<RenderTask> createForkedTasks(RenderJob newJob, RenderJob originalJob) throws IOException {
        List<RenderTask> tasks = new ArrayList<>();
        try {
            for (RenderTask originalTask : originalJob.getTasks()) {
                tasks.add(MasterMain.getDatabase().addTask(
                        newJob, newJob.getWorld(),
                        originalTask.getX(), originalTask.getY(), originalTask.getZ(),
                        originalTask.getPitch(), originalTask.getYaw(), newJob.isFocusSet() ? newJob.getFocus() : null,
                        newJob.getChunkRadius(), newJob.getWidth(), newJob.getHeight(), newJob.getPreset(),
                        MasterMain.getDefaults().getTargetSpp()));
            }
        } catch (SQLException e) {
            LOGGER.error("Could not create tasks", e);
        }

        return tasks;
    }

    @Override
    public void onTaskUpdated(RenderTask task) throws IOException {
        RenderDump dump = RenderDump.loadDump(MasterMain.getSceneFileProvider().getDumpFile(task));
        File pictureFile = MasterMain.getSceneFileProvider().getLatestPictureFile(task);
        dump.savePicture(pictureFile, MasterMain.getPresetDataProvider().getPreset(task.getPreset()));
        LOGGER.info("Updated image for " + task);
    }

    @Override
    public void onAllTasksFinished(RenderJob job) throws IOException {
        File f = MasterMain.getSceneFileProvider().getVideoFile(job);
        SequenceEncoder encoder = new SequenceEncoder(f, 25, 2 * job.getHeight(), job.getHeight());
        for (RenderTask task : job.getTasks()) {
            BufferedImage bi = ImageIO.read(MasterMain.getSceneFileProvider().getLatestPictureFile(task));
            encoder.encodeImage(bi);
        }
        encoder.finish();

        try {
            MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.FINISHED);
            MasterMain.getDatabase().setJobLastPictureDate(job, new Date());
        } catch (SQLException e) {
            LOGGER.warn("Could not update status of " + job, e);
        }
    }

    @Override
    public SceneDescription getSceneDescription(RenderTask task, SceneDescription sceneDescription) {
        sceneDescription.setWidth(2 * sceneDescription.getHeight());
        sceneDescription.getCamera().getOrientation().setYaw(0);
        sceneDescription.getCamera().getOrientation().setPitch(-90 * Math.PI / 180);
        sceneDescription.getCamera().setProjectionMode(ProjectionMode.PANORAMIC);
        sceneDescription.getCamera().setFov(180.0);
        sceneDescription.getCamera().setDof(Double.POSITIVE_INFINITY);
        return sceneDescription;
    }
}
