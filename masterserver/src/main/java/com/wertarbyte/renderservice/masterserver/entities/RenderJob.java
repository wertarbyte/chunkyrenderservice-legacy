/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.entities;

import com.owlike.genson.annotation.JsonIgnore;
import com.wertarbyte.renderservice.api.RenderJobStatus;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.masterserver.MasterMain;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * A render job that contains {@link com.wertarbyte.renderservice.masterserver.entities.RenderTask}s.
 */
public class RenderJob extends com.wertarbyte.renderservice.api.RenderJob {
    private List<RenderTask> tasks;

    public RenderJob(int id, String creator, Date createTime, String world, double x, double y, double z, double pitch, double yaw, double focus, boolean hasFocus, int chunkRadius, RenderJobStatus status, int targetSpp, String preset, Date lastPicTime, int width, int height, RenderJobType type) {
        super(id, creator, createTime, world, x, y, z, pitch, yaw, focus, hasFocus, chunkRadius, status, targetSpp, preset, lastPicTime, width, height, type);
    }

    /**
     * Gets the tasks of this job, sorted ascending by ID.
     *
     * @return tasks of this job, sorted ascending by ID
     * @throws java.io.IOException if getting the tasks fails
     */
    @JsonIgnore
    public List<RenderTask> getTasks() throws IOException {
        if (tasks == null) {
            try {
                tasks = MasterMain.getDatabase().getTasksByJob(this);
            } catch (SQLException e) {
                throw new IOException(e);
            }
        }
        return tasks;
    }

    /**
     * Gets the progress percentage of this job, based on the progress of its tasks.
     *
     * @return progress percentage of this job
     * @throws IOException if getting the tasks fails
     */
    @JsonIgnore
    public double getProgressPercentage() throws IOException {
        double sumPercentage = 0;
        List<RenderTask> tasks = getTasks();
        if (tasks.isEmpty()) {
            return 100;
        }
        for (RenderTask task : tasks) {
            sumPercentage += task.getProgressPercentage();
        }
        return sumPercentage / tasks.size();
    }
}
