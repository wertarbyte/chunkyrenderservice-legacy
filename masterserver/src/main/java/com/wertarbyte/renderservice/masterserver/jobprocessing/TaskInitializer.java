/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing;

import com.wertarbyte.renderservice.libchunky.UnmodifiableChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfiguration;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The pooled initializer for render jobs.
 */
class TaskInitializer {
    private static final Logger LOGGER = LogManager.getLogger(TaskInitializer.class);
    private final ExecutorService pool;
    private final List<InitTaskProcess> running;

    public TaskInitializer(int threadCount) {
        pool = Executors.newFixedThreadPool(threadCount);
        running = new ArrayList<>();
    }

    public void addTask(final RenderTask task) {
        final InitTaskProcess process = new InitTaskProcess(task, MasterMain.getSceneFileProvider().getWorldFile(task.getJob()), getChunkyConfig(task));

        pool.submit(new Runnable() {
            @Override
            public void run() {
                running.add(process);
                try {
                    process.render();
                    running.remove(process);
                    MasterMain.getJobProcessor().getTaskScheduler().scheduleTask(task);
                } catch (IOException e) {
                    LOGGER.error("Initializing " + task + " failed", e);
                    running.remove(process);
                }
            }
        });
    }

    private ChunkyConfiguration getChunkyConfig(RenderTask task) {
        return ChunkyConfiguration.builder()
                .setJvmMinMemory(1024)
                .setJvmMaxMemory(2048)
                .setDefaultTexturePack(null)//TODO texture-pack
                .setSceneDirectory(MasterMain.getSceneFileProvider().getSceneDirectory(task))
                .setSceneName(MasterMain.getSceneFileProvider().getSceneName(task))
                .setThreadCount(1)
                .setTargetSpp(1)
                .setDumpFrequency(1)
                .build();
    }

    public void stop() {
        pool.shutdown();
    }

    /**
     * Gets all running processes.
     *
     * @return running processes
     */
    public Collection<UnmodifiableChunkyWrapper> getRunningProcesses() {
        return new ArrayList<UnmodifiableChunkyWrapper>(running);
    }
}
