/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.util;

import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import com.wertarbyte.renderservice.libchunky.scene.PostprocessMethod;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * A render dump.
 * <p/>
 * Code adapted from:
 * https://github.com/llbit/chunky/blob/master/chunky/src/java/se/llbit/chunky/renderer/scene/Scene.java
 */
public class RenderDump extends ChunkyRenderDump {
    public RenderDump(int width, int height) {
        super(width, height);
    }

    public RenderDump(File dumpFile) throws IOException {
        super(dumpFile);
    }

    public RenderDump(ChunkyRenderDump dump) {
        super(dump.getWidth(), dump.getHeight(),
                Arrays.copyOf(dump.getSamples(), dump.getSamples().length), dump.getSpp(),
                dump.getRenderTime());
    }

    @Deprecated
    public static synchronized RenderDump loadDump(File dumpFile) throws IOException {
        return new RenderDump(dumpFile);
    }

    public synchronized BufferedImage getPicture(RenderPreset preset) {
        PostprocessMethod postprocessMethod;
        switch (preset.getPostprocess()) {
            case GAMMA_CORRECTION:
                postprocessMethod = PostprocessMethod.GammaCorrection;
                break;
            case TONE_MAP:
                postprocessMethod = PostprocessMethod.ToneMap;
                break;
            default:
                postprocessMethod = PostprocessMethod.None;
                break;
        }

        return getPicture(preset.getExposure(), postprocessMethod);
    }

    /**
     * Writes this dump as image into a stream.
     *
     * @param target stream to write the PNG image into
     * @param preset preset of the job (used for exposure and post-processing)
     * @throws java.io.IOException if saving the image fails
     */
    public synchronized void writePicture(OutputStream target, RenderPreset preset) throws IOException {
        ImageIO.write(getPicture(preset), "PNG", target);
    }

    /**
     * Saves this dump as image.
     *
     * @param targetFile file to save the image
     * @param preset     preset of the job (used for exposure and post-processing)
     * @throws java.io.IOException if saving the image fails
     */
    public synchronized void savePicture(File targetFile, RenderPreset preset) throws IOException {
        ImageIO.write(getPicture(preset), "PNG", targetFile);
    }
}
