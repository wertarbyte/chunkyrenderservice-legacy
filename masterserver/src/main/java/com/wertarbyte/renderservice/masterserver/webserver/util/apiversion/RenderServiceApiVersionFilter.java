/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.util.apiversion;

import org.glassfish.jersey.server.ExtendedUriInfo;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A filter that enforces specific client versions for API requests.
 */
public class RenderServiceApiVersionFilter implements ContainerRequestFilter {
    private static final Pattern RENDERER_PATTERN = Pattern.compile("RenderService Renderer v(\\d+)");
    private static final Pattern PLUGIN_PATTERN = Pattern.compile("RenderService Plugin v(\\d+)");

    @Override
    public void filter(ContainerRequestContext context) throws IOException {
        ExtendedUriInfo extendedUriInfo = (ExtendedUriInfo) context.getUriInfo();
        Method method = extendedUriInfo.getMatchedResourceMethod().getInvocable().getHandlingMethod();

        RequirePlugin requirePlugin = method.getAnnotation(RequirePlugin.class);
        if (requirePlugin == null) {
            requirePlugin = method.getDeclaringClass().getAnnotation(RequirePlugin.class);
        }
        if (requirePlugin != null) {
            verifyIsPlugin(context.getHeaderString("User-Agent"), requirePlugin.version(), context);
        }

        RequireRenderer requireRenderer = method.getAnnotation(RequireRenderer.class);
        if (requireRenderer == null) {
            requireRenderer = method.getDeclaringClass().getAnnotation(RequireRenderer.class);
        }
        if (requireRenderer != null) {
            verifyIsRenderer(context.getHeaderString("User-Agent"), requireRenderer.version(), context);
        }
    }

    private void verifyIsPlugin(String userAgent, int requiredVersion, ContainerRequestContext context) {
        Matcher m = PLUGIN_PATTERN.matcher(userAgent);

        if (requiredVersion > 0) {
            if (!m.matches() || Integer.parseInt(m.group(1)) < requiredVersion) {
                context.abortWith(Response
                        .status(Response.Status.FORBIDDEN)
                        .header("x-renderservice", "deprecated client")
                        .type(MediaType.TEXT_PLAIN_TYPE)
                        .entity("Deprecated plugin version.")
                        .build());
            }
        } else if (!m.matches()) {
            context.abortWith(Response
                    .status(Response.Status.FORBIDDEN)
                    .build());
        }
    }

    private void verifyIsRenderer(String userAgent, int requiredVersion, ContainerRequestContext context) {
        Matcher m = RENDERER_PATTERN.matcher(userAgent);

        if (requiredVersion > 0) {
            if (!m.matches() || Integer.parseInt(m.group(1)) < requiredVersion) {
                context.abortWith(Response
                        .status(Response.Status.FORBIDDEN)
                        .header("x-renderservice", "deprecated client")
                        .type(MediaType.TEXT_PLAIN_TYPE)
                        .entity("Deprecated renderer version.")
                        .build());
            }
        } else if (!m.matches()) {
            context.abortWith(Response
                    .status(Response.Status.FORBIDDEN)
                    .build());
        }
    }
}
