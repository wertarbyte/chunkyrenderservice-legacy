/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing;

import com.wertarbyte.renderservice.api.*;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import com.wertarbyte.renderservice.libchunky.UnmodifiableChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.remote.server.RendererPool;
import com.wertarbyte.renderservice.libchunky.scene.ChunkPosition;
import com.wertarbyte.renderservice.libchunky.util.ChunkyValueConverter;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.JobStatus;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes.JobHandler;
import com.wertarbyte.renderservice.masterserver.jobprocessing.scheduler.ScheduledTask;
import com.wertarbyte.renderservice.masterserver.jobprocessing.scheduler.TaskScheduler;
import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.util.FileUtil;
import com.wertarbyte.renderservice.util.MinecraftUtil;
import com.wertarbyte.renderservice.util.video.KeyframeList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * The processor for tasks and jobs. This is the handler for all job- or task-related actions, including job creation,
 * forking and canceling.
 */
public class JobProcessor {
    private static final Logger LOGGER = LogManager.getLogger(JobProcessor.class);
    private TaskInitializer taskInitializer;
    private DumpProcessor dumpProcessor;
    private TaskScheduler taskScheduler;
    private final Thread taskSchedulerThread;

    public JobProcessor(int initializerThreads, int poolPort) {
        taskInitializer = new TaskInitializer(initializerThreads);
        dumpProcessor = new DumpProcessor();
        taskScheduler = new TaskScheduler(poolPort);
        taskSchedulerThread = new Thread(taskScheduler);
        taskSchedulerThread.start();
    }

    TaskScheduler getTaskScheduler() {
        return taskScheduler;
    }

    public void addNewJob(RenderJob job, JobConfiguration configuration, InputStream regionsFileStream) throws IOException {
        try {
            File jobPath = MasterMain.getSceneFileProvider().getSceneDirectory(job);
            if (!jobPath.mkdirs()) {
                throw new IOException("Could not create job directory");
            }
            FileUtil.writeFile(regionsFileStream, MasterMain.getSceneFileProvider().getWorldFile(job));
        } catch (IOException e) {
            LOGGER.error("Could not save world files for " + job, e);
            throw e;
        }

        Object data = null;
        if (job.getType() == RenderJobType.VIDEO || job.getType() == RenderJobType.SPHERICAL_VIDEO) {
            data = new KeyframeList(((VideoJobConfiguration) configuration).getKeyframes());
        }

        JobHandler handler = JobHandler.forJob(job);
        try {
            List<RenderTask> tasks = handler.createTasks(job, data);
            for (RenderTask task : tasks) {
                taskInitializer.addTask(task);
            }
            LOGGER.info(tasks.size() + " tasks created for " + job);
        } catch (IOException e) {
            LOGGER.error("Could not create task for " + job, e);
            throw e;
        }
    }

    public RenderJob forkJob(RenderJob originalJob, int width, int height, String preset) throws IOException {
        RenderJob job;
        try {
            job = MasterMain.getDatabase().addJob(new JobConfiguration(
                    originalJob.getCreator(), originalJob.getWorld(),
                    originalJob.getX(), originalJob.getY(), originalJob.getZ(),
                    originalJob.getPitch(), originalJob.getYaw(),
                    originalJob.getChunkRadius(), originalJob.isFocusSet() ? originalJob.getFocus() : null,
                    preset, originalJob.getType()
            ), width, height);
        } catch (SQLException e) {
            throw new IOException("Could not create new job", e);
        }

        if (!MasterMain.getSceneFileProvider().getSceneDirectory(job).mkdirs()) {
            throw new IOException("Could not create job directory");
        }
        FileUtil.copy(MasterMain.getSceneFileProvider().getWorldFile(originalJob), MasterMain.getSceneFileProvider().getWorldFile(job));

        JobHandler handler = JobHandler.forJob(originalJob);
        List<RenderTask> tasks = handler.createForkedTasks(job, originalJob);
        for (RenderTask task : tasks) {
            taskInitializer.addTask(task);
        }
        LOGGER.info(tasks.size() + " tasks created for " + job);

        return job;
    }

    public void addUninitializedTasks(Iterable<RenderTask> tasks) {
        for (RenderTask task : tasks) {
            taskInitializer.addTask(task);
        }
    }

    public void addUnfinishedTasks(Iterable<RenderTask> tasks) {
        for (RenderTask task : tasks) {
            taskScheduler.scheduleTask(task);
        }
    }

    public void stop() {
        LOGGER.info("Stopping task initializer...");
        taskInitializer.stop();

        LOGGER.info("Stopping dump processor...");
        dumpProcessor.stop();

        LOGGER.info("Stopping task scheduler...");
        taskSchedulerThread.interrupt();
    }

    public void cancelJob(RenderJob job) throws IOException {
        try {
            MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.CANCELED);
            for (RenderTask task : job.getTasks()) {
                MasterMain.getDatabase().setTaskStatus(task, com.wertarbyte.renderservice.api.RenderTask.Status.CANCELED);
                taskScheduler.removeTask(task);
            }
        } catch (SQLException | IOException e) {
            LOGGER.error("Could not cancel " + job, e);
            throw new IOException("Could not cancel " + job, e);
        }
    }

    /**
     * Resets the given job.
     *
     * @param job job to reset
     */
    public void resetJob(RenderJob job) throws IOException {
        try {
            for (RenderTask task : job.getTasks()) {
                taskScheduler.removeTask(task);
                FileUtil.deleteDirectory(MasterMain.getSceneFileProvider().getSceneDirectory(task));
                MasterMain.getDatabase().setTaskStatus(task, com.wertarbyte.renderservice.api.RenderTask.Status.NEW);
                MasterMain.getDatabase().setTaskCurrentSpp(task, 0, false);
                MasterMain.getSceneFileProvider().getDumpFile(task).delete();
                taskInitializer.addTask(task);
            }

            MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.NEW);
        } catch (SQLException | IOException e) {
            LOGGER.error("Could not reset " + job, e);
            throw new IOException("Could not reset " + job, e);
        }
    }

    public void addAssignmentResults(RenderTask task, UUID assignmentId, ChunkyRenderDump dump) {
        dumpProcessor.addDump(task, assignmentId, dump);
    }

    public void setJobSppTarget(RenderJob job, int targetSpp) throws IOException {
        try {
            MasterMain.getDatabase().setJobSppTarget(job.getId(), targetSpp);
            for (RenderTask task : job.getTasks()) {
                if (task.getStatus() != com.wertarbyte.renderservice.api.RenderTask.Status.NEW) {
                    MasterMain.getDatabase().setTaskStatus(task, com.wertarbyte.renderservice.api.RenderTask.Status.READY);
                }
            }
            if (job.getStatus() != RenderJobStatus.NEW) {
                MasterMain.getDatabase().setJobStatus(job, RenderJobStatus.READY);
            }
            for (RenderTask task : MasterMain.getDatabase().getTasksByJob(job)) {
                taskScheduler.scheduleTask(task); //re-schedule task
            }

            LOGGER.info(String.format("Target of %s was set to %d SPP", job, targetSpp));
        } catch (SQLException e) {
            LOGGER.error("Could not set SPP target", e);
            throw new IOException("Could not set SPP target", e);
        } catch (IOException e) {
            LOGGER.error("Could not get tasks", e);
            throw e;
        }
    }

    public Collection<UnmodifiableChunkyWrapper> getRunningInitializationProcesses() {
        return taskInitializer.getRunningProcesses();
    }

    public RendererPool getPool() {
        return getTaskScheduler().getPool();
    }

    public SceneDescription getSceneDescription(RenderTask task) throws IOException {
        RenderPreset preset = MasterMain.getPresetDataProvider().getPreset(task.getPreset());
        SceneDescription sd = new SceneDescription();
        sd.applyPreset(preset);
        if (preset.isFocusSupported()) {
            sd.getCamera().setFocalOffset(task.isFocusSet() ? task.getFocus() : Double.POSITIVE_INFINITY);
        }

        //...add some other parameters from the job...
        sd.getCamera().getPosition().setX(task.getX());
        sd.getCamera().getPosition().setY(task.getY());
        sd.getCamera().getPosition().setZ(task.getZ());
        sd.getCamera().getOrientation().setPitch(ChunkyValueConverter.getPitchFromMinecraft(task.getPitch()));
        sd.getCamera().getOrientation().setYaw(ChunkyValueConverter.getYawFromMinecraft(task.getYaw()));

        List<ChunkPosition> chunks = new ArrayList<>();
        for (int[] cp : MinecraftUtil.getChunksAround((int) task.getX(), (int) task.getZ(), task.getChunkRadius())) {
            chunks.add(new ChunkPosition(cp[0], cp[1]));
        }
        sd.setChunkList(chunks);

        sd.setWidth(task.getWidth());
        sd.setHeight(task.getHeight());

        return JobHandler.forTask(task).getSceneDescription(task, sd);
    }

    /**
     * Gets all assignments.
     *
     * @return all assignments
     */
    public List<Assignment> getAssignments() {
        return taskScheduler.getRunningTasks();
    }

    /**
     * Gets all tasks known to the scheduler.
     *
     * @return all tasks
     */
    public List<ScheduledTask> getScheduledTasks() {
        return taskScheduler.getTasks();
    }

    public List<JobStatus> getCurrentJobStatuses() throws IOException {
        List<JobStatus> statuses = new ArrayList<>();

        try {
            for (RenderJob job : MasterMain.getDatabase().getUnfinishedJobs()) {
                int targetSpp = 0;
                int currentSpp = 0;
                int currentSavedSpp = 0;
                for (RenderTask task : job.getTasks()) {
                    targetSpp += task.getTargetSpp();
                    currentSpp += task.getCurrentSpp();
                    currentSavedSpp += task.getCurrentSpp();
                    for (Assignment assignment : getAssignments()) {
                        if (assignment.getTask().getId() == task.getId()) {
                            currentSpp += assignment.getCurrentSpp();
                        }
                    }
                }
                statuses.add(new JobStatus(job, currentSpp * 100.0 / targetSpp, currentSavedSpp * 100.0 / targetSpp));
            }
        } catch (SQLException e) {
            throw new IOException(e);
        }

        return statuses;
    }
}
