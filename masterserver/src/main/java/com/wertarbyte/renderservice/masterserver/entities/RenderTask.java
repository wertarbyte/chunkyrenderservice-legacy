/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.entities;

import com.owlike.genson.annotation.JsonIgnore;
import com.wertarbyte.renderservice.masterserver.MasterMain;

import java.sql.SQLException;
import java.util.Date;

/**
 * A more convenient implementation of {@link com.wertarbyte.renderservice.api.RenderTask}.
 */
public class RenderTask extends com.wertarbyte.renderservice.api.RenderTask {
    private RenderJob job;

    public RenderTask(int id, int jobId, double x, double y, double z, String world, double pitch, double yaw, double focus, boolean isFocusSet, int chunkRadius, String preset, int width, int height, Status status, int targetSpp, int currentSpp, Date lastPicDate) {
        super(id, jobId, x, y, z, world, pitch, yaw, focus, isFocusSet, chunkRadius, preset, width, height, status, targetSpp, currentSpp, lastPicDate);
    }

    @JsonIgnore
    public RenderJob getJob() {
        if (job != null) {
            return job;
        }
        try {
            return job = MasterMain.getDatabase().getJobById(getJobId());
        } catch (SQLException e) {
            throw new RuntimeException("Could not get job " + getJobId(), e);
        }
    }

    /**
     * Gets the progress percentage of this job.
     *
     * @return progress percentage of this job, always lower or equal to 100, even if current spp is greater than target spp
     */
    public double getProgressPercentage() {
        return Math.min(100, 100.0 * getCurrentSpp() / getTargetSpp());
    }
}
