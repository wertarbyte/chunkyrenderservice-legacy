/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.scheduler;

import com.wertarbyte.renderservice.api.Assignment;
import com.wertarbyte.renderservice.libchunky.remote.UnmodifiableRemoteRenderer;
import com.wertarbyte.renderservice.libchunky.remote.server.RemoteRenderer;
import com.wertarbyte.renderservice.libchunky.remote.server.RemoteRendererPool;
import com.wertarbyte.renderservice.libchunky.remote.server.RendererPool;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

/**
 * A scheduler that distributes tasks to multiple rendering computers.
 */
public class TaskScheduler implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(TaskScheduler.class);

    private final Map<Integer, ScheduledTask> taskList = Collections.synchronizedMap(new HashMap<Integer, ScheduledTask>());
    private final Map<UUID, Assignment> runningTasks = Collections.synchronizedMap(new HashMap<UUID, Assignment>());
    private final int poolPort;
    private RemoteRendererPool pool;

    public TaskScheduler(int poolPort) {
        this.poolPort = poolPort;
    }

    /**
     * Schedules the given task. If it is already scheduled, it is updated.
     *
     * @param task task to schedule
     */
    public synchronized void scheduleTask(RenderTask task) {
        ScheduledTask scheduledTask = taskList.get(task.getId());
        if (scheduledTask != null) {
            scheduledTask.updateTask(task);
        } else {
            int spp = task.getTargetSpp() - task.getCurrentSpp();
            if (spp > 0) {
                taskList.put(task.getId(), new ScheduledTask(task, spp));
            }
        }
    }

    void rescheduleAssignment(RenderTask task, Assignment assignment, int spp) {
        runningTasks.remove(assignment.getId());
        ScheduledTask scheduledTask = taskList.get(task.getId());
        scheduledTask.unschedule(spp);
    }

    /**
     * Commits the given amount of rendered samples per pixel for the given task. This needs to be called whenever a
     * task is updated.
     *
     * @param task task to update
     * @param spp  samples per pixel that were added to the task
     */
    public synchronized void commitRenderedTask(UUID assignmentId, RenderTask task, int spp) {
        //No assignment exisst

        ScheduledTask scheduled = taskList.get(task.getId());
        if (scheduled != null) {
            scheduled.commit(spp);
            if (scheduled.isCompleted()) {
                taskList.remove(task.getId());
            }
        }
    }

    /**
     * Removes the given task so that it won't be assigned anymore.
     *
     * @param task task to remove
     */
    public void removeTask(RenderTask task) {
        taskList.remove(task.getId());

        for (UnmodifiableRemoteRenderer renderer : getPool().getActiveRenderers()) {
            if (renderer.getAdditionalData("taskId") != null && Integer.parseInt(renderer.getAdditionalData("taskId")) == task.getId()) {
                getPool().stopRenderer(renderer);
            }
        }
    }

    public List<Assignment> getRunningTasks() {
        return new ArrayList<>(runningTasks.values());
    }

    public List<ScheduledTask> getTasks() {
        return new ArrayList<>(taskList.values());
    }

    public RendererPool getPool() {
        return pool;
    }

    public int getPoolPort() {
        return poolPort;
    }

    /**
     * Get the next task that is not completely scheduled, yet.
     *
     * @return task
     */
    private ScheduledTask getNextTask() throws InterruptedException {
        while (!Thread.interrupted()) {
            while (taskList.isEmpty()) {
                Thread.sleep(1000);
            }

            for (ScheduledTask task : taskList.values()) {
                if (task.getUnscheduledSpp() > 0) {
                    return task;
                }
            }
        }
        throw new InterruptedException();
    }

    @Override
    public void run() {
        pool = new RemoteRendererPool(getPoolPort());
        pool.start();
        LOGGER.info("Pool started on port " + getPoolPort());

        while (!Thread.interrupted()) {
            final ScheduledTask task;
            try {
                task = getNextTask();
            } catch (InterruptedException e) {
                LOGGER.info("Interrupted while waiting for a task", e);
                break;
            }

            final RemoteRenderer chunky;
            try {
                chunky = pool.getChunkyInstance();
            } catch (RuntimeException e) {
                if (e.getCause() instanceof InterruptedException) {
                    break;
                } else {
                    continue;
                }
            }

            final int scheduledSpp = task.schedule(Math.min(task.getUnscheduledSpp(),
                    Math.max(10, 500 * (640 * 360) / task.getPixels())));

            UUID rendererId = chunky.getAdditionalData("clientId") != null ?
                    UUID.fromString(chunky.getAdditionalData("clientId")) : UUID.randomUUID();
            final Assignment assignment = new Assignment(UUID.randomUUID(), rendererId, task.getTask(), scheduledSpp);
            runningTasks.put(assignment.getId(), assignment);

            Thread observer = new Thread(new RendererObserver(this, task, chunky, assignment, scheduledSpp));
            observer.setDaemon(true);
            observer.start();
        }

        LOGGER.info("Stopping pool...");
        try {
            pool.stop();
            LOGGER.info("Pool stopped");
        } catch (IOException e) {
            LOGGER.warn("Could not stop renderer pool", e);
        }
    }

    void removeRunningAssignment(Assignment assignment) {
        runningTasks.remove(assignment.getId());
    }
}
