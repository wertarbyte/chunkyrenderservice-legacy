/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.entities;

import com.owlike.genson.annotation.JsonProperty;

public class JobStatus {
    private final RenderJob job;
    private final double progressPercentage;
    private double safeProgressPercentage;

    public JobStatus(RenderJob job, double progressPercentage, double safeProgressPercentage) {
        this.job = job;
        this.progressPercentage = progressPercentage;
        this.safeProgressPercentage = safeProgressPercentage;
    }

    public RenderJob getJob() {
        return job;
    }

    @JsonProperty("progress")
    public double getProgressPercentage() {
        return progressPercentage;
    }

    @JsonProperty("safeProgress")
    public double getSafeProgressPercentage() {
        return safeProgressPercentage;
    }
}
