/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes;

import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;

import java.io.IOException;
import java.util.List;

/**
 * A handler for job updates.
 */
public abstract class JobHandler {
    /**
     * Creates all tasks for the given job and returns them.
     *
     * @param job  job to create the tasks for
     * @param data additional data for the job
     * @return tasks for the job
     * @throws java.io.IOException                if something goes wrong
     * @throws java.lang.IllegalArgumentException if <code>data</code> is invalid for this job type
     */
    public abstract List<RenderTask> createTasks(RenderJob job, Object data) throws IOException, IllegalArgumentException;

    /**
     * Creates all task for the given new job based on the given original job and returns them.
     *
     * @param newJob      new job to create the tasks for
     * @param originalJob original job the new job is based on
     * @return tasks for the new job
     * @throws java.io.IOException if something goes wrong
     */
    public abstract List<RenderTask> createForkedTasks(RenderJob newJob, RenderJob originalJob) throws IOException;

    /**
     * Gets called when the progress of a task changes. It is up to this method to update the job's status. The status
     * of the task is already updated.
     *
     * @param task task that was updated
     * @throws java.io.IOException if something goes wrong
     */
    public abstract void onTaskUpdated(RenderTask task) throws IOException;

    /**
     * Gets called when all tasks of the given job finished. It is up to this method to update the job's status.
     *
     * @param job job
     * @throws java.io.IOException if something goes wrong
     */
    public abstract void onAllTasksFinished(RenderJob job) throws IOException;

    public static JobHandler forJob(RenderJob job) {
        switch (job.getType()) {
            case PICTURE:
                return new PictureJobHandler();
            case STEREO_PICTURE:
                return new StereoJobHandler();
            case VIDEO:
                return new VideoJobHandler();
            case SPHERE:
                return new SphereJobHandler();
            case SPHERICAL_VIDEO:
                return new SphericalVideoJobHandler();
        }
        throw new IllegalArgumentException("Unsupported job type: " + job.getType());
    }

    public static JobHandler forTask(RenderTask task) {
        return forJob(task.getJob());
    }

    /**
     * Gets the scene description for the given task.
     *
     * @param task             task
     * @param sceneDescription original scene description, should be modified and returned
     * @return new or modified scene description
     */
    public SceneDescription getSceneDescription(RenderTask task, SceneDescription sceneDescription) {
        return sceneDescription;
    }
}
