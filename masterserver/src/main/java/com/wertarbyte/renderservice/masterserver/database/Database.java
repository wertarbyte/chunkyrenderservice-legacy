/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.database;

import com.jolbox.bonecp.BoneCPConfig;
import com.jolbox.bonecp.BoneCPDataSource;
import com.wertarbyte.renderservice.api.JobConfiguration;
import com.wertarbyte.renderservice.api.RenderJobStatus;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.api.account.Account;
import com.wertarbyte.renderservice.api.account.Transaction;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.entities.TaskStatistics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class Database {
    private static final Logger LOGGER = LogManager.getLogger(Database.class);
    private DataSource source;

    public Database(String host, int port, String database,
                    String user, String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            LOGGER.error("JDBC not found", e);
        }

        BoneCPConfig config = new BoneCPConfig();
        config.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database + "?noAccessToProcedureBodies=true");
        config.setUsername(user);
        config.setPassword(password);
        config.setMinConnectionsPerPartition(2);
        config.setMaxConnectionsPerPartition(10);
        config.setPartitionCount(1);
        source = new BoneCPDataSource(config);
    }

    /**
     * Gets all unfinished jobs. This are jobs that are neither finished nor cancelled.
     *
     * @return all unfinished jobs
     * @throws SQLException if getting the jobs fails
     */
    public List<RenderJob> getUnfinishedJobs() throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,createdBy,createTime,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,latestPicTime,preset,width,height,type FROM render_jobs WHERE status = 'READY' OR status = 'NEW'")) {
            ResultSet result = ps.executeQuery();
            List<RenderJob> jobList = new ArrayList<>();
            while (result.next()) {
                RenderJob job = new RenderJob(
                        result.getInt("id"),
                        result.getString("createdBy"),
                        new Date(result.getTimestamp("createTime").getTime()),
                        result.getString("world"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkradius"),
                        RenderJobStatus.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getObject("preset") != null ? result.getString("preset") : null,
                        result.getObject("latestPicTime") != null ? new Date(result.getTimestamp("latestPicTime").getTime()) : null,
                        result.getInt("width"),
                        result.getInt("height"),
                        RenderJobType.valueOf(result.getString("type")));
                jobList.add(job);
            }
            return jobList;
        }
    }

    /**
     * Gets all jobs.
     *
     * @return all jobs
     * @throws SQLException if getting the jobs fails
     */
    public List<RenderJob> getJobList() throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,createdBy,createTime,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,latestPicTime,preset,width,height,type FROM render_jobs")) {
            ResultSet result = ps.executeQuery();
            List<RenderJob> jobList = new ArrayList<>();
            while (result.next()) {
                RenderJob job = new RenderJob(
                        result.getInt("id"),
                        result.getString("createdBy"),
                        new Date(result.getTimestamp("createTime").getTime()),
                        result.getString("world"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkradius"),
                        RenderJobStatus.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getObject("preset") != null ? result.getString("preset") : null,
                        result.getObject("latestPicTime") != null ? new Date(result.getTimestamp("latestPicTime").getTime()) : null,
                        result.getInt("width"),
                        result.getInt("height"),
                        RenderJobType.valueOf(result.getString("type")));
                jobList.add(job);
            }
            return jobList;
        }
    }

    public List<RenderTask> getUnfinishedTasks() throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,currentSpp,lastPicDate,preset,width,height,job FROM render_tasks WHERE status='READY'")) {
            ResultSet result = ps.executeQuery();
            List<RenderTask> taskList = new ArrayList<>();
            while (result.next()) {
                RenderTask task = new RenderTask(
                        result.getInt("id"),
                        result.getInt("job"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getString("world"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkRadius"),
                        result.getObject("preset") != null ? (result.getString("preset").equals("") ? "default" : result.getString("preset")) : "default",
                        result.getInt("width"),
                        result.getInt("height"),
                        com.wertarbyte.renderservice.api.RenderTask.Status.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getInt("currentSpp"),
                        result.getObject("lastPicDate") != null ? new Date(result.getTimestamp("lastPicDate").getTime()) : null);
                taskList.add(task);
            }

            return taskList;
        }
    }

    public void setJobStatus(RenderJob job, RenderJobStatus status) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE render_jobs SET status = ? WHERE id = ?")) {
            ps.setString(1, status.name());
            ps.setInt(2, job.getId());
            ps.executeUpdate();
        }
    }

    public void setJobSppTarget(int jobID, int targetSpp) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE render_jobs SET targetSpp = ? WHERE id = ?")) {
            ps.setInt(1, targetSpp);
            ps.setInt(2, jobID);
            ps.executeUpdate();
        }
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE render_tasks SET targetSpp = ? WHERE job = ?")) {
            ps.setInt(1, targetSpp);
            ps.setInt(2, jobID);
            ps.executeUpdate();
        }
    }

    public void setJobLastPictureDate(RenderJob job, Date date) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE render_jobs SET latestPicTime = ? WHERE id = ?")) {
            ps.setTimestamp(1, new Timestamp(date.getTime()));
            ps.setInt(2, job.getId());
            ps.executeUpdate();
        }
    }

    public void setTaskCurrentSpp(RenderTask task, int currentSpp, boolean updatePicTime) throws SQLException {
        try (Connection con = source.getConnection()) {
            if (updatePicTime) {
                Timestamp time = new Timestamp(Calendar.getInstance().getTimeInMillis());
                try (PreparedStatement ps = con.prepareStatement("UPDATE render_tasks SET currentSpp = ?, lastPicDate = ? WHERE id = ?")) {
                    ps.setInt(1, currentSpp);
                    ps.setTimestamp(2, time);
                    ps.setInt(3, task.getId());
                    ps.executeUpdate();
                }
            } else {
                try (PreparedStatement ps = con.prepareStatement("UPDATE render_tasks SET currentSpp = ? WHERE id = ?")) {
                    ps.setInt(1, currentSpp);
                    ps.setInt(2, task.getId());
                    ps.executeUpdate();
                }
            }
        }
    }

    /**
     * Gets the render job with the given id, without render settings.
     *
     * @param id id of the render job to get
     * @return RenderJob or null if no render job with the given id exists
     * @throws SQLException if the request fails
     */
    public RenderJob getJobById(int id) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,createdBy,createTime,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,currentSpp,latestPicTime,preset,width,height,type FROM render_jobs WHERE id = ?")) {
            ps.setInt(1, id);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                return new RenderJob(
                        result.getInt("id"),
                        result.getString("createdBy"),
                        new Date(result.getTimestamp("createTime").getTime()),
                        result.getString("world"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkradius"),
                        RenderJobStatus.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getObject("preset") != null ? (result.getString("preset").equals("") ? "default" : result.getString("preset")) : "default",
                        result.getObject("latestPicTime") != null ? new Date(result.getTimestamp("latestPicTime").getTime()) : null,
                        result.getInt("width"),
                        result.getInt("height"),
                        RenderJobType.valueOf(result.getString("type")));
            } else {
                return null;
            }
        }
    }

    public RenderJob addJob(JobConfiguration config, int width, int height) throws SQLException {
        Timestamp time = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("INSERT INTO render_jobs (createdBy, createTime, world, x, y, z, pitch, yaw, focus, chunkradius, status,  preset, width, height, type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, config.getCreator());
            ps.setTimestamp(2, time);
            ps.setString(3, config.getWorld());
            ps.setDouble(4, config.getX());
            ps.setDouble(5, config.getY());
            ps.setDouble(6, config.getZ());
            ps.setDouble(7, config.getPitch());
            ps.setDouble(8, config.getYaw());
            if (config.getFocusDistance() != null) {
                ps.setDouble(9, config.getFocusDistance());
            } else {
                ps.setNull(9, Types.DOUBLE);
            }
            ps.setInt(10, config.getChunkRadius());
            ps.setString(11, RenderJobStatus.NEW.name());
            ps.setString(12, config.getPreset() == null || config.getPreset().trim().isEmpty() ? "default" : config.getPreset());
            ps.setInt(13, width);
            ps.setInt(14, height);
            ps.setString(15, config.getType().name());

            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            int newId = keys.getInt(1);

            return new RenderJob(
                    newId,
                    config.getCreator(),
                    new Date(time.getTime()),
                    config.getWorld(), config.getX(), config.getY(), config.getZ(),
                    config.getPitch(), config.getYaw(),
                    config.getFocusDistance() == null ? 0 : config.getFocusDistance(), config.getFocusDistance() != null,
                    config.getChunkRadius(),
                    RenderJobStatus.NEW,
                    5000,//TODO
                    config.getPreset() != null && !config.getPreset().isEmpty() ? config.getPreset() : "default", null,
                    width, height,
                    config.getType());
        }
    }

    public TaskStatistics getTaskStatistics() throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT " +
                     "(SELECT count(*) FROM render_tasks WHERE status = ?)new," +
                     "(SELECT count(*) FROM render_tasks WHERE status = ?)ready," +
                     "(SELECT count(*) FROM render_tasks WHERE status = ?)finished")) {
            ps.setString(1, RenderTask.Status.NEW.name());
            ps.setString(2, RenderTask.Status.READY.name());
            ps.setString(3, RenderTask.Status.FINISHED.name());
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                return new TaskStatistics(
                        result.getInt("new"),
                        result.getInt("ready"),
                        result.getInt("finished")
                );
            }
            return null;
        }
    }

    public List<RenderJob> getJobsByCreator(String name) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,createdBy,createTime,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,latestPicTime,preset,width,height,type FROM render_jobs WHERE createdBy=?")) {
            ps.setString(1, name);
            ResultSet result = ps.executeQuery();
            List<RenderJob> jobList = new ArrayList<>();
            while (result.next()) {
                RenderJob job = new RenderJob(
                        result.getInt("id"),
                        result.getString("createdBy"),
                        new Date(result.getTimestamp("createTime").getTime()),
                        result.getString("world"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkradius"),
                        RenderJobStatus.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getObject("preset") != null ? result.getString("preset") : null,
                        result.getObject("latestPicTime") != null ? new Date(result.getTimestamp("latestPicTime").getTime()) : null,
                        result.getInt("width"),
                        result.getInt("height"),
                        RenderJobType.valueOf(result.getString("type")));
                jobList.add(job);
            }

            return jobList;
        }
    }

    public List<RenderTask> getNewTasks() throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,currentSpp,lastPicDate,preset,width,height,job FROM render_tasks WHERE status='NEW'")) {
            ResultSet result = ps.executeQuery();
            List<RenderTask> taskList = new ArrayList<>();
            while (result.next()) {
                RenderTask task = new RenderTask(
                        result.getInt("id"),
                        result.getInt("job"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getString("world"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkRadius"),
                        result.getObject("preset") != null ? (result.getString("preset").equals("") ? "default" : result.getString("preset")) : "default",
                        result.getInt("width"),
                        result.getInt("height"),
                        com.wertarbyte.renderservice.api.RenderTask.Status.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getInt("currentSpp"),
                        result.getObject("lastPicDate") != null ? new Date(result.getTimestamp("lastPicDate").getTime()) : null);
                taskList.add(task);
            }

            return taskList;
        }
    }

    public RenderTask addTask(RenderJob job, String world, double x, double y, double z, double pitch, double yaw, Double focusDistance, int chunkRadius, int width, int height, String preset, int targetSpp) throws SQLException {
        Timestamp time = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("INSERT INTO render_tasks (x, y, z, pitch, yaw, world, focus, chunkRadius, preset, width, height, status, targetSpp, currentSpp, job) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
            ps.setDouble(1, x);
            ps.setDouble(2, y);
            ps.setDouble(3, z);
            ps.setDouble(4, pitch);
            ps.setDouble(5, yaw);
            ps.setString(6, world);
            if (focusDistance != null) {
                ps.setDouble(7, focusDistance);
            } else {
                ps.setNull(7, Types.DOUBLE);
            }
            ps.setInt(8, chunkRadius);
            ps.setString(9, preset);
            ps.setInt(10, width);
            ps.setInt(11, height);
            ps.setString(12, RenderTask.Status.NEW.name());
            ps.setInt(13, targetSpp);
            ps.setInt(14, 0);
            ps.setInt(15, job.getId());

            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            int newId = keys.getInt(1);

            return new RenderTask(
                    newId,
                    job.getId(),
                    x, y, z,
                    world,
                    pitch, yaw,
                    focusDistance == null ? 0 : focusDistance, focusDistance != null,
                    chunkRadius,
                    preset,
                    width, height,
                    RenderTask.Status.NEW,
                    targetSpp,
                    0,
                    null);
        }
    }

    public void setTaskStatus(com.wertarbyte.renderservice.api.RenderTask task, RenderTask.Status status) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE render_tasks SET status = ? WHERE id = ?")) {
            ps.setString(1, status.toString());
            ps.setInt(2, task.getId());
            ps.executeUpdate();
        }
    }

    public RenderTask getTaskById(int id) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,currentSpp,lastPicDate,preset,width,height,job FROM render_tasks WHERE id = ?")) {
            ps.setInt(1, id);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                return new RenderTask(
                        result.getInt("id"),
                        result.getInt("job"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getString("world"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkRadius"),
                        result.getObject("preset") != null ? (result.getString("preset").equals("") ? "default" : result.getString("preset")) : "default",
                        result.getInt("width"),
                        result.getInt("height"),
                        com.wertarbyte.renderservice.api.RenderTask.Status.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getInt("currentSpp"),
                        result.getObject("lastPicDate") != null ? new Date(result.getTimestamp("lastPicDate").getTime()) : null);
            } else {
                return null;
            }
        }
    }

    /**
     * Gets the task of the given render job, sorted ascending by ID.
     *
     * @param renderJob render job to get the tasks of
     * @return tasks of the given render job, sorted ascending by ID
     * @throws java.sql.SQLException if getting the tasks fails
     */
    public List<RenderTask> getTasksByJob(RenderJob renderJob) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT id,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,currentSpp,lastPicDate,preset,width,height,job FROM render_tasks WHERE job=? ORDER BY id")) {
            ps.setInt(1, renderJob.getId());
            ResultSet result = ps.executeQuery();
            List<RenderTask> taskList = new ArrayList<>();
            while (result.next()) {
                RenderTask task = new RenderTask(
                        result.getInt("id"),
                        result.getInt("job"),
                        result.getDouble("x"),
                        result.getDouble("y"),
                        result.getDouble("z"),
                        result.getString("world"),
                        result.getDouble("pitch"),
                        result.getDouble("yaw"),
                        result.getDouble("focus"),
                        result.getObject("focus") != null,
                        result.getInt("chunkRadius"),
                        result.getObject("preset") != null ? (result.getString("preset").equals("") ? "default" : result.getString("preset")) : "default",
                        result.getInt("width"),
                        result.getInt("height"),
                        com.wertarbyte.renderservice.api.RenderTask.Status.valueOf(result.getString("status")),
                        result.getInt("targetSpp"),
                        result.getInt("currentSpp"),
                        result.getObject("lastPicDate") != null ? new Date(result.getTimestamp("lastPicDate").getTime()) : null);
                taskList.add(task);
            }

            return taskList;
        }
    }

    public Account getAccount(UUID uuid) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT acc.uuid, acc.points, t.fromto, t.points, t.time, \n" +
                     "job.id,createdBy,createTime,world,x,y,z,pitch,yaw,focus,chunkradius,status,targetSpp,latestPicTime,preset,width,height,type\n" +
                     "FROM accounts acc\n" +
                     "LEFT OUTER JOIN accounts_transactions t ON (uuid = account) \n" +
                     "LEFT OUTER JOIN render_jobs job ON job = job.id\n" +
                     "WHERE uuid = ?")) {
            ps.setString(1, uuid.toString());
            ResultSet result = ps.executeQuery();
            List<Transaction> transactions = new ArrayList<>();
            int points = 0;
            while (result.next()) {
                points = result.getInt(2);
                if (result.getObject(3) != null) { //transaction from/to other player
                    transactions.add(new Transaction(
                            result.getInt(4),
                            UUID.fromString(result.getString(3)),
                            new Date(result.getTimestamp(5).getTime())
                    ));
                } else if (result.getObject(6) != null) { //transaction from job
                    transactions.add(new Transaction(
                            result.getInt(4),
                            result.getInt("id"),
                            new Date(result.getTimestamp(5).getTime())
                    ));
                }
            }
            return new Account(uuid, points, transactions);
        }
    }

    public void givePointsTo(UUID account, int points, int jobId) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement call = con.prepareCall("{call GIVE_POINTS_FOR_JOB(?,?,?)}")) {
            call.setString(1, account.toString());
            call.setInt(2, points);
            call.setInt(3, jobId);
            call.execute();
        }
    }

    public void givePointsToDefault(int points, int jobId) throws SQLException {
        try (Connection con = source.getConnection();
             PreparedStatement call = con.prepareCall("{call GIVE_POINTS_FOR_JOB(?,?,?)}")) {
            call.setString(1, "x-default");
            call.setInt(2, points);
            call.setInt(3, jobId);
            call.execute();
        }
    }
}
