/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.api;

import com.google.gson.Gson;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.webserver.util.apiversion.RequireRenderer;
import com.wertarbyte.renderservice.util.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.simpleframework.http.Request;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@RequireRenderer(version = 2)
@Path("/tasks")
public class TaskController {
    private static final Logger LOGGER = LogManager.getLogger(TaskController.class);

    @GET
    @Path("/unfinished")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RenderTask> getTasks() {
        try {
            return MasterMain.getDatabase().getUnfinishedTasks();
        } catch (SQLException e) {
            throw new InternalServerErrorException(e);
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RenderTask getJob(@PathParam("id") int id) {
        return getTaskById(id);
    }

    @GET
    @Path("{id}/latest.png")
    @Produces("image/png")
    public Response getLatestImage(@PathParam("id") int id) {
        RenderTask task = getTaskById(id);

        File picture = MasterMain.getSceneFileProvider().getLatestPictureFile(task);
        if (!picture.exists()) {
            throw new NotFoundException();
        }

        return Response.ok(picture, "image/png")
                .lastModified(task.getLastPicDate())
                .build();
    }

    @GET
    @Path("{id}/scene.json")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSceneDescription(@PathParam("id") int id) {
        RenderTask task = getTaskById(id);
        try {
            //we need to manually convert the scene description to json as some fields are not type-safe (i.e. infinity becomes 'inf')
            return Response.ok(new Gson().toJson(MasterMain.getJobProcessor().getSceneDescription(task).toJson()), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (IOException e) {
            throw new InternalServerErrorException(e);
        }
    }

    private RenderTask getTaskById(int id) throws InternalServerErrorException, NotFoundException {
        RenderTask task;
        try {
            task = MasterMain.getDatabase().getTaskById(id);
        } catch (SQLException e) {
            LOGGER.error("Getting task " + id + " failed", e);
            throw new InternalServerErrorException(e);
        }
        if (task == null) {
            throw new NotFoundException("Task " + id + " not found");
        }
        return task;
    }
}
