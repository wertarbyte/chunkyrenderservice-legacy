/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.api;

import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Provides presets and skymaps.
 */
@Path("/presets")
public class PresetProvider {
    private static final Logger LOGGER = LogManager.getLogger(PresetProvider.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<RenderPreset> getPresets() {
        return MasterMain.getPresetDataProvider().getPresets();
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public RenderPreset getPreset(@PathParam("name") String name) {
        try {
            RenderPreset preset = MasterMain.getPresetDataProvider().getPreset(name);
            if (preset == null) {
                throw new NotFoundException("Preset not found");
            }
            return preset;
        } catch (IOException e) {
            LOGGER.error("Could not get preset " + name, e);
            throw new InternalServerErrorException(e);
        }
    }

    @GET
    @Path("/{name}/skymap")
    public Response getSkymap(@PathParam("name") String name) {
        if (getPreset(name) == null) {
            throw new NotFoundException("Preset not found");
        }

        File skymap = MasterMain.getPresetDataProvider().getSkymap(name);
        if (skymap != null) {
            String contentType;
            if (skymap.getName().endsWith(".png")) {
                contentType = "image/png";
            } else {
                contentType = "image/jpeg";
            }

            return Response.ok(skymap, contentType)
                    .lastModified(new Date(skymap.lastModified()))
                    .build();
        } else {
            return Response.noContent().build();
        }
    }
}
