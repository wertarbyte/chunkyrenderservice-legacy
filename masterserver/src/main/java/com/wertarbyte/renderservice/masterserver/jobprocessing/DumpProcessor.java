/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing;

import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes.JobHandler;
import com.wertarbyte.renderservice.masterserver.util.RenderDump;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A processor for incoming render dumps.
 */
class DumpProcessor {
    private static final Logger LOGGER = LogManager.getLogger(DumpProcessor.class);
    private ExecutorService pool = Executors.newSingleThreadExecutor();

    /**
     * Asynchronously merges the given dump into the given job's dump.
     *
     * @param task         task the dump belongs to
     * @param assignmentId ID of the assignment that was completed
     * @param dumpToMerge  dump to merge
     */
    public synchronized void addDump(final RenderTask task, final UUID assignmentId, final ChunkyRenderDump dumpToMerge) {
        pool.execute(new Runnable() {
            @Override
            public void run() {
                File dumpFile = MasterMain.getSceneFileProvider().getDumpFile(task);
                RenderDump dump;
                try {
                    if (dumpFile.exists()) {
                        dump = new RenderDump(dumpFile);
                        int oldSpp = dump.getSpp();
                        dump.mergeDump(dumpToMerge);
                        dump.saveDump(dumpFile);
                        LOGGER.info("Merged dump for " + task + ": " + oldSpp + " -> " + dump.getSpp());
                        MasterMain.getJobProcessor().getTaskScheduler().commitRenderedTask(assignmentId, task, dump.getSpp() - oldSpp);
                    } else {
                        dumpToMerge.saveDump(dumpFile);
                        dump = new RenderDump(dumpToMerge);
                        LOGGER.info("Created dump for " + task + ": 0 -> " + dump.getSpp());
                        MasterMain.getJobProcessor().getTaskScheduler().commitRenderedTask(assignmentId, task, dump.getSpp());
                    }
                } catch (IOException e) {
                    LOGGER.warn("Could not merge dump for " + task, e);
                    return;
                }

                try {
                    JobHandler.forTask(task).onTaskUpdated(task);
                } catch (IOException e) {
                    LOGGER.warn("Could not create new picture for " + task, e);
                }

                try {
                    MasterMain.getDatabase().setTaskCurrentSpp(task, dump.getSpp(), true);
                } catch (SQLException e) {
                    LOGGER.warn("Could not update current spp of " + task, e);
                }
                if (dump.getSpp() >= task.getTargetSpp()) {
                    LOGGER.info(task + " finished");
                    try {
                        MasterMain.getDatabase().setTaskStatus(task, RenderTask.Status.FINISHED);
                    } catch (SQLException e) {
                        LOGGER.warn("Could not update status of " + task, e);
                    }
                }

                try {
                    RenderJob job = MasterMain.getDatabase().getJobById(task.getJobId());
                    List<RenderTask> tasks = job.getTasks();
                    boolean allFinished = true;
                    for (RenderTask task : tasks) {
                        if (task.getStatus() != RenderTask.Status.FINISHED) {
                            allFinished = false;
                            break;
                        }
                    }

                    if (allFinished) {
                        LOGGER.info(job + " finished");
                        JobHandler.forJob(job).onAllTasksFinished(job);
                    }
                } catch (IOException e) {
                    LOGGER.error("Could not get tasks of " + task.getJob(), e);
                } catch (SQLException e) {
                    LOGGER.error("Could not get job " + task.getJobId(), e);
                }
            }
        });
    }

    public void stop() {
        pool.shutdown();
    }
}
