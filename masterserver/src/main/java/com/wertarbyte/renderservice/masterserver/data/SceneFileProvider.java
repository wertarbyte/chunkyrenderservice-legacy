/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.data;

import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.api.RenderTask;

import java.io.File;

/**
 * A provider for scene files and files that are required by scenes.
 */
public class SceneFileProvider {
    private final File sceneDirectory;

    public SceneFileProvider(File sceneDirectory) {
        this.sceneDirectory = sceneDirectory;
    }

    public File getWorldFile(RenderJob job) {
        return new File(getSceneDirectory(job), job.getId() + "_" + job.getCreator() + ".tar.gz");
    }

    public File getSceneDirectory(RenderTask job) {
        return new File(sceneDirectory, "task_" + job.getId());
    }

    public File getSceneDirectory(RenderJob job) {
        return new File(sceneDirectory, "job_" + job.getId());
    }

    public File getDumpFile(RenderTask task) {
        return new File(getSceneDirectory(task), "task_" + task.getId() + ".dump");
    }

    public File getLatestPictureFile(RenderJob job) {
        return new File(getSceneDirectory(job), "latest.png");
    }

    public File getLatestPictureFile(RenderTask task) {
        return new File(getSceneDirectory(task), "latest.png");
    }

    public File getRenderDataFile(RenderTask task) {
        return new File(getSceneDirectory(task), "data.tar.gz");
    }

    public String getSceneName(RenderTask task) {
        return "task_" + task.getId();
    }

    public File getVideoFile(RenderJob job) {
        return new File(getSceneDirectory(job), "latest.mp4");
    }
}
