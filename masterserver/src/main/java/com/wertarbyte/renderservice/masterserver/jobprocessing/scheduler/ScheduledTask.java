/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.scheduler;

import com.wertarbyte.renderservice.masterserver.entities.RenderTask;

/**
 * A {@link com.wertarbyte.renderservice.masterserver.entities.RenderTask} combined with an amount of
 * samples per pixel to render.
 */
public class ScheduledTask {
    private RenderTask task;
    private int missingSpp;
    private int scheduledSpp;

    ScheduledTask(RenderTask task, int missingSpp) {
        this.task = task;
        this.missingSpp = missingSpp;
        scheduledSpp = 0;
    }

    int schedule(int spp) {
        int schedule = Math.min(spp, getUnscheduledSpp());
        scheduledSpp += schedule;
        return schedule;
    }

    void unschedule(int spp) {
        scheduledSpp -= spp;
    }

    void commit(int spp) {
        missingSpp -= spp;
        scheduledSpp -= spp;
    }

    /**
     * Gets the number of samples per pixel that are missing and not scheduled.
     *
     * @return number of samples per pixel that are missing and not scheduled
     */
    public int getUnscheduledSpp() {
        return missingSpp - scheduledSpp;
    }

    synchronized void updateTask(RenderTask task) {
        missingSpp += task.getTargetSpp() - this.task.getTargetSpp();
        this.task = task;
    }

    public int getPixels() {
        return task.getWidth() * task.getHeight();
    }

    public RenderTask getTask() {
        return task;
    }

    public boolean isCompleted() {
        return missingSpp <= 0;
    }
}
