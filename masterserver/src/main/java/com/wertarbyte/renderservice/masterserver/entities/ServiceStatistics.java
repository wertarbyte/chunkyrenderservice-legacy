/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.entities;

import com.wertarbyte.renderservice.api.Assignment;
import com.wertarbyte.renderservice.api.RendererInfo;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Data class for render service statistics.
 */
public class ServiceStatistics {
    private Map<UUID, RendererInfo> rendererStatistics;
    private TaskStatistics taskStatistics;
    private int renderersCount;
    private List<JobStatus> runningJobs;
    private List<Assignment> assignments;

    public ServiceStatistics(Map<UUID, RendererInfo> rendererStatistics, TaskStatistics taskStatistics, int renderersCount,
                             List<JobStatus> unfinishedJobs, List<Assignment> assignments) {
        this.rendererStatistics = rendererStatistics;
        this.taskStatistics = taskStatistics;
        this.renderersCount = renderersCount;
        this.runningJobs = unfinishedJobs;
        this.assignments = assignments;
    }

    /**
     * Gets statistics about all running renderers.
     *
     * @return statistics about all running renderers
     */
    public Map<UUID, RendererInfo> getRendererStatistics() {
        return rendererStatistics;
    }

    /**
     * Gets statistic about the render tasks.
     *
     * @return statistics of the render tasks
     */
    public TaskStatistics getTaskStatistics() {
        return taskStatistics;
    }

    /**
     * Gets the number of running renderer instances.
     *
     * @return number of running renderer instances
     */
    public int getRenderersCount() {
        return renderersCount;
    }

    /**
     * Gets all running jobs.
     *
     * @return running jobs
     */
    public List<JobStatus> getRunningJobs() {
        return runningJobs;
    }

    /**
     * Gets all assignments.
     *
     * @return assignments
     */
    public List<Assignment> getAssignments() {
        return assignments;
    }
}
