/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.jobtypes;

import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.scene.ProjectionMode;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;

/**
 * Handles sphere jobs that use equirectangular projection.
 */
public class SphereJobHandler extends PictureJobHandler {
    @Override
    public SceneDescription getSceneDescription(RenderTask task, SceneDescription sceneDescription) {
        sceneDescription.setWidth(2 * sceneDescription.getHeight());
        sceneDescription.getCamera().getOrientation().setYaw(0);
        sceneDescription.getCamera().getOrientation().setPitch(-90 * Math.PI / 180);
        sceneDescription.getCamera().setProjectionMode(ProjectionMode.PANORAMIC);
        sceneDescription.getCamera().setFov(180.0);
        sceneDescription.getCamera().setDof(Double.POSITIVE_INFINITY);
        return sceneDescription;
    }
}
