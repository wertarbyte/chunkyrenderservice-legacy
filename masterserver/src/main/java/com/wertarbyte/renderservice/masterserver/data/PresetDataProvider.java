/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.data;

import com.google.gson.Gson;
import com.wertarbyte.renderservice.api.presets.RenderPreset;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A provider for skymaps.
 */
public class PresetDataProvider {
    private Gson gson = new Gson();
    private final File presetsDir;

    public PresetDataProvider(File presetsDir) {
        this.presetsDir = presetsDir;
    }

    public File getSkymap(String name) {
        if (name == null || name.isEmpty()) {
            name = "default";
        }

        File skymap = new File(presetsDir, name + "-skymap.png");
        if (skymap.exists()) {
            return skymap;
        } else {
            skymap = new File(presetsDir, name + "-skymap.jpg");
            return skymap.exists() ? skymap : null;
        }
    }

    public RenderPreset getPreset(String name) throws IOException {
        if (name == null || name.isEmpty()) {
            name = "default";
        }

        File presetFile = new File(presetsDir, name + ".json");
        try (FileReader reader = new FileReader(presetFile)) {
            return gson.fromJson(reader, RenderPreset.class);
        } catch (IOException e) {
            throw e;
        }

    }

    public boolean presetExists(String name) {
        return new File(presetsDir, name + ".json").exists();
    }

    public List<RenderPreset> getPresets() {
        List<RenderPreset> presets = new ArrayList<>();
        for (File preset : presetsDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".json");
            }
        })) {
            String name = preset.getName().substring(0, preset.getName().length() - ".json".length());
            try {
                presets.add(getPreset(name));
            } catch (IOException e) { //TODO log error?
            }
        }
        return presets;
    }
}
