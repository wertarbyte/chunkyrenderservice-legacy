/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.api;

import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderJob;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.masterserver.util.RenderDump;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Path("/jobs")
public class JobController {
    private static final Logger LOGGER = LogManager.getLogger(JobController.class);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RenderJob getJob(@PathParam("id") int id) {
        return getJobById(id);
    }

    @GET
    @Path("/{id}/tasks")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RenderTask> getTasks(@PathParam("id") int id) {
        try {
            return getJobById(id).getTasks();
        } catch (IOException e) {
            throw new InternalServerErrorException(e);
        }
    }

    @GET
    @Path("{id}/latest.png")
    @Produces("image/png")
    public Response getLatestImage(@PathParam("id") int id) {
        RenderJob job = getJobById(id);

        File picture = MasterMain.getSceneFileProvider().getLatestPictureFile(job);
        if (job.getType() == RenderJobType.VIDEO || job.getType() == RenderJobType.SPHERICAL_VIDEO) {
            try {
                picture = MasterMain.getSceneFileProvider().getLatestPictureFile(job.getTasks().get(0));
            } catch (IOException e) {
                throw new InternalServerErrorException();
            }
        }

        if (!picture.exists()) {
            return Response.noContent().build(); //signal user that no picture exists but the job exists
        }

        return Response.ok(picture, "image/png")
                .lastModified(new Date(picture.lastModified()))
                .build();
    }

    @GET
    @Path("{id}/leftright.png")
    @Produces("image/png")
    public Response getLatestLeftRightImage(@PathParam("id") int id) {
        RenderJob job = getJobById(id);

        if (job.getType() == RenderJobType.STEREO_PICTURE) {
            File picture = MasterMain.getSceneFileProvider().getLatestPictureFile(job);
            if (!picture.exists()) {
                return Response.noContent().build(); //signal user that no picture exists but the job exists
            }

            RenderPreset preset;
            try {
                preset = MasterMain.getPresetDataProvider().getPreset(job.getPreset());
            } catch (IOException e) {
                LOGGER.error("Could not read preset", e);
                throw new InternalServerErrorException(e);
            }

            try {
                BufferedImage img = new BufferedImage(job.getWidth() * 2, job.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = img.getGraphics();
                List<RenderTask> tasks = job.getTasks();
                LOGGER.info(MasterMain.getSceneFileProvider().getLatestPictureFile(tasks.get(0)).getAbsolutePath());
                g.drawImage(new RenderDump(MasterMain.getSceneFileProvider().getDumpFile(tasks.get(0))).getPicture(preset), 0, 0, null);
                g.drawImage(new RenderDump(MasterMain.getSceneFileProvider().getDumpFile(tasks.get(1))).getPicture(preset), job.getWidth(), 0, null);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "png", baos);

                return Response.ok(new ByteArrayInputStream(baos.toByteArray()), "image/png").build();
            } catch (IOException e) {
                LOGGER.warn("Could not create left-right picture", e);
                throw new InternalServerErrorException(e);
            }
        } else {
            throw new NotFoundException();
        }
    }

    @GET
    @Path("{id}/latest.mp4")
    @Produces("video/mp4")
    public Response getVideo(@PathParam("id") int id) {
        RenderJob job = getJobById(id);
        if (job.getType() != RenderJobType.VIDEO && job.getType() != RenderJobType.SPHERICAL_VIDEO) {
            throw new NotFoundException();
        }

        File video = MasterMain.getSceneFileProvider().getVideoFile(job);
        if (!video.exists()) {
            return Response.noContent().build(); //signal user that no picture exists but the job exists
        }

        return Response.ok(video, "video/mp4")
                .lastModified(job.getLastPicDate())
                .build();
    }

    private RenderJob getJobById(int id) throws InternalServerErrorException, NotFoundException {
        RenderJob job;
        try {
            job = MasterMain.getDatabase().getJobById(id);
        } catch (SQLException e) {
            LOGGER.error("Getting job " + id + " failed", e);
            throw new InternalServerErrorException(e);
        }
        if (job == null) {
            throw new NotFoundException("Job " + id + " not found");
        }
        return job;
    }
}
