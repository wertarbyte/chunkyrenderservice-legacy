/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing.scheduler;

import com.wertarbyte.renderservice.api.Assignment;
import com.wertarbyte.renderservice.libchunky.RenderListenerAdapter;
import com.wertarbyte.renderservice.libchunky.remote.server.RemoteRenderer;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * An observer for remote renderers.
 */
class RendererObserver implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(RendererObserver.class);
    private TaskScheduler scheduler;
    private final ScheduledTask scheduledTask;
    private final RemoteRenderer chunky;
    private final Assignment assignment;
    private final int scheduledSpp;

    public RendererObserver(TaskScheduler scheduler, ScheduledTask scheduledTask, RemoteRenderer chunky,
                            Assignment assignment, int scheduledSpp) {
        this.scheduler = scheduler;
        this.scheduledTask = scheduledTask;
        this.chunky = chunky;
        this.assignment = assignment;
        this.scheduledSpp = scheduledSpp;
    }

    @Override
    public void run() {
        RenderTask task = scheduledTask.getTask();

        try {
            //TODO set texturepack
            SceneDescription scene = MasterMain.getJobProcessor().getSceneDescription(task);
            scene.setName(MasterMain.getSceneFileProvider().getSceneName(task));
            File skymap = MasterMain.getPresetDataProvider().getSkymap(task.getPreset());
            if (skymap != null) {
                scene.getSky().setSkymap(skymap.getAbsolutePath());
            } else {
                LOGGER.warn("Skymap not found for preset " + task.getPreset());
            }

            chunky.addListener(new RenderListenerAdapter() {
                @Override
                public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                    assignment.setCurrentSpp(currentSpp);
                }
            });

            chunky.setSceneDirectory(MasterMain.getSceneFileProvider().getSceneDirectory(task));
            chunky.setScene(scene);
            chunky.setTargetSpp(scheduledSpp);
            chunky.putData("assignmentId", assignment.getId().toString());
            chunky.putData("jobId", String.valueOf(task.getJobId()));
            chunky.putData("taskId", String.valueOf(task.getId()));

            LOGGER.info("Start rendering " + assignment.getSpp() + " SPP for " + task);
            chunky.render();

            LOGGER.info("Finished rendering " + assignment.getSpp() + " for " + task);
            MasterMain.getJobProcessor().addAssignmentResults(task, assignment.getId(), chunky.getDump());

            try {
                MasterMain.getAccountManager().givePoints(chunky.getAdditionalData("account"), assignment);
            } catch (IOException e) {
                LOGGER.warn("Could not give points for completed assignment", e);
            }
        } catch (IOException e) {
            LOGGER.warn("Rendering with remote instance failed", e);
            scheduler.rescheduleAssignment(task, assignment, scheduledSpp);
        }

        chunky.done();
        scheduler.removeRunningAssignment(assignment);
    }
}
