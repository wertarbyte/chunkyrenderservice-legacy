/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.webserver.api;

import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.ServiceStatistics;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Provides statistics and status via the API.
 */
@Path("/status")
public class Status {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceStatistics getStatus() {
        try {
            return new ServiceStatistics(MasterMain.getRendererStatistics().getStatistics(),
                    MasterMain.getDatabase().getTaskStatistics(),
                    MasterMain.getRendererStatistics().getRenderersCount(),
                    MasterMain.getJobProcessor().getCurrentJobStatuses(),
                    MasterMain.getJobProcessor().getAssignments());
        } catch (IOException | SQLException e) {
            throw new InternalServerErrorException(e);
        }
    }
}
