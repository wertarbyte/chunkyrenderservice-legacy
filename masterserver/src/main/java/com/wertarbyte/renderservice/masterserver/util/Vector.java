/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.util;

/**
 * A three-dimensional vector.
 */
public class Vector {
    private double x;
    private double y;
    private double z;

    /**
     * Creates a new vector with the given components.
     *
     * @param x x-component
     * @param y y-component
     * @param z z-component
     */
    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Creates a new rotation vector with the given pitch and yaw.
     *
     * @param pitch pitch in radians
     * @param yaw   yaw in radians
     */
    public Vector(double pitch, double yaw) {
        x = Math.sin(pitch) * Math.cos(yaw);
        y = Math.sin(pitch) * Math.sin(yaw);
        z = Math.cos(pitch);
        normalize();
    }

    /**
     * Creates a copy of the given vector.
     *
     * @param v vector to copy
     */
    public Vector(Vector v) {
        this(v.x, v.y, v.z);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Calculates the length of this vector.
     *
     * @return the length of this vector
     */
    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Adds the given vector to this vector.
     *
     * @param v vector to add
     * @return this vector
     */
    public Vector add(Vector v) {
        x += v.x;
        y += v.y;
        z += v.z;
        return this;
    }

    /**
     * Scales this vector by the given factor.
     *
     * @param s scale factor
     * @return this vector
     */
    public Vector scale(double s) {
        x *= s;
        y *= s;
        z *= s;
        return this;
    }

    /**
     * Calculates the cross product of this vector and the other vector (this x v).
     *
     * @param v vector to multiply
     * @return a new vector that is orthogonal to this and the given vector
     */
    public Vector cross(Vector v) {
        return new Vector(
                y * v.z - z * v.y,
                z * v.x - x * v.z,
                x * v.y - y * v.x);
    }

    /**
     * Normalizes this vector.
     *
     * @return this vector
     */
    public Vector normalize() {
        return scale(1 / length());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector vector = (Vector) o;

        if (Double.compare(vector.x, x) != 0) return false;
        if (Double.compare(vector.y, y) != 0) return false;
        if (Double.compare(vector.z, z) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
