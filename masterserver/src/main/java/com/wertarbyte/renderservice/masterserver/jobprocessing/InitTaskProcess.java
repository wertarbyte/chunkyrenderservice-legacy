/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.masterserver.jobprocessing;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.RenderListener;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfiguration;
import com.wertarbyte.renderservice.masterserver.MasterMain;
import com.wertarbyte.renderservice.masterserver.entities.RenderTask;
import com.wertarbyte.renderservice.scene.SceneDescription;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;
import org.kamranzafar.jtar.TarOutputStream;

import java.io.*;
import java.sql.SQLException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * A wrapper for the job initialization process.
 */
class InitTaskProcess extends com.wertarbyte.renderservice.libchunky.ChunkyProcessWrapper implements RenderListener {
    private static final Logger LOGGER = LogManager.getLogger(InitTaskProcess.class);
    private final RenderTask task;
    private File worldFile;
    private ChunkyConfiguration config;

    /**
     * Creates a new job initialization process.
     *
     * @param task      job
     * @param worldFile the .tar.gz world file for that job
     * @param config    chunky configuration for that job
     */
    public InitTaskProcess(RenderTask task, File worldFile, ChunkyConfiguration config) {
        this.task = task;
        this.worldFile = worldFile;
        this.config = config;

        addListener(this);
    }

    private RenderTask getTask() {
        return task;
    }

    @Override
    public void render() throws IOException {
        setSceneDirectory(config.getSceneDirectory());
        setTexturepack(config.getDefaultTexturePack());
        setThreadCount(config.getThreadCount());
        setJvmMinMemory(config.getJvmMinMemory());
        setJvmMaxMemory(config.getJvmMaxMemory());
        setTargetSpp(config.getTargetSpp());

        config.getSceneDirectory().mkdirs();

        try {
            setScene(getSceneDescription());
        } catch (IOException e) {
            throw new IOException("Could not create scene description", e);
        }

        LOGGER.info("Started initializing " + getTask());

        if (!getWorldPath().exists()) {
            //world path doesn't exist, so get and extract the world
            try {
                extractWorld();
            } catch (IOException e) {
                throw new IOException("Could not extract world", e);
            }
        }

        super.render();
    }

    private void addFileToTar(TarOutputStream tar, File file, String name) throws IOException {
        tar.putNextEntry(new TarEntry(file, name));
        try (BufferedInputStream origin = new BufferedInputStream(new FileInputStream(file))) {
            int count;
            byte data[] = new byte[2084];
            while ((count = origin.read(data)) != -1) {
                tar.write(data, 0, count);
            }
            tar.flush();
        }
    }

    private SceneDescription getSceneDescription() throws IOException {
        SceneDescription settings = MasterMain.getJobProcessor().getSceneDescription(getTask());

        //...add some other parameters from the job...
        settings.setName(config.getSceneName());

        //...don't forget the skymap...
        File skymap = MasterMain.getPresetDataProvider().getSkymap(getTask().getPreset());
        settings.getSky().setSkymap(skymap != null ? skymap.getAbsolutePath() : "");
        settings.setDumpFrequency(config.getDumpFrequency());
        //...and the world...
        settings.getWorld().setPath(getWorldPath().getAbsolutePath());

        return settings;
    }

    private File getWorldPath() {
        return new File(config.getSceneDirectory(), "world");
    }

    /**
     * Extracts this job's world files from the given file.
     *
     * @throws java.io.IOException if extracting the world fails
     */
    private void extractWorld() throws IOException {
        FileInputStream binaryTarGzStream = new FileInputStream(worldFile);
        try (GZIPInputStream gzip = new GZIPInputStream(new BufferedInputStream(binaryTarGzStream));
             TarInputStream tis = new TarInputStream(new BufferedInputStream(gzip))) {
            TarEntry entry;

            while ((entry = tis.getNextEntry()) != null) {
                int count;
                byte data[] = new byte[2084];

                if (entry.isDirectory()) {
                    new File(getWorldPath(), entry.getName()).mkdirs();
                    continue;
                } else {
                    int di = entry.getName().lastIndexOf('/');
                    if (di != -1) {
                        new File(getWorldPath(), entry.getName().substring(0, di)).mkdirs();
                    }
                }

                try (FileOutputStream fos = new FileOutputStream(new File(getWorldPath(), entry.getName()));
                     BufferedOutputStream dest = new BufferedOutputStream(fos)) {
                    while ((count = tis.read(data)) != -1) {
                        dest.write(data, 0, count);
                    }
                    dest.flush();
                }
            }
        }
    }

    @Override
    public void onFinished() {
        //Pack render files
        File sceneDir = MasterMain.getSceneFileProvider().getSceneDirectory(getTask());
        String nameBase = MasterMain.getSceneFileProvider().getSceneName(getTask());

        try (FileOutputStream fos = new FileOutputStream(MasterMain.getSceneFileProvider().getRenderDataFile(getTask()));
             GZIPOutputStream gzip = new GZIPOutputStream(new BufferedOutputStream(fos));
             TarOutputStream tos = new TarOutputStream(gzip)) {

            addFileToTar(tos, new File(sceneDir, nameBase + ".octree"), nameBase + ".octree");
            addFileToTar(tos, new File(sceneDir, nameBase + ".foliage"), nameBase + ".foliage");
            addFileToTar(tos, new File(sceneDir, nameBase + ".grass"), nameBase + ".grass");
        } catch (IOException e) {
            LOGGER.error("Could not create render data file for " + getTask(), e);
        }

        if (!new File(sceneDir, nameBase + ".dump").delete()) {
            LOGGER.warn("Could not delete dump file for " + getTask());
        }

        //Set job status to finished
        try {
            MasterMain.getDatabase().setTaskStatus(getTask(), RenderTask.Status.READY);
        } catch (SQLException e) {
            LOGGER.warn("Could not update status of " + getTask(), e);
        }
        LOGGER.info("Initialized task " + getTask());
    }

    @Override
    public void onRenderStatusChanged(int currentSpp, int targetSpp) {

    }

    @Override
    public void onStatusChanged(ChunkyTask task) {

    }
}
