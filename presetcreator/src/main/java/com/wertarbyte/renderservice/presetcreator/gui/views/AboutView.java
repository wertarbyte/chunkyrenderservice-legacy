/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.gui.views;

import com.wertarbyte.renderservice.presetcreator.logic.PresetCreator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The about dialog.
 */
public class AboutView extends JFrame {
    public AboutView() {
        setLayout(new BorderLayout(10, 10));

        JLabel titleLabel = new JLabel("PresetCreator " + PresetCreator.VERSION);
        titleLabel.setFont(new Font(null, Font.BOLD, 20));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        add(titleLabel, BorderLayout.PAGE_START);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
        JButton closeButton = new JButton("OK");
        closeButton.setPreferredSize(new Dimension(100, 25));
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
            }
        });
        buttonPanel.add(closeButton);
        getRootPane().setDefaultButton(closeButton);
        add(buttonPanel, BorderLayout.PAGE_END);

        JLabel textLabel = new JLabel("<html>" +
                "<p>PresetCreator is a program to easily create presets for the Wertarbyte RenderService.</p><br>" +
                "<p>Copyright (C) 2015 Wertarbyte (http://wertarbyte.com)</p><br>" +
                "<p>This program is free software: you can redistribute it and/or modify " +
                "it under the terms of the GNU General Public License as published by " +
                "the Free Software Foundation, either version 3 of the License, or " +
                "(at your option) any later version.</p><br>" +
                "<p>PresetCreator heavily uses Chunky by Jesper Öqvist, licensed under the GPLv3. See " +
                "http://chunky.llbit.se for more information.</p>"+
                "</html>");
        textLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(textLabel, BorderLayout.CENTER);

        setPreferredSize(new Dimension(600,350));
        pack();
        setLocationRelativeTo(null);
    }
}
