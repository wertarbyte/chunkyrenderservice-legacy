package com.wertarbyte.renderservice.presetcreator.gui.views;

import com.wertarbyte.renderservice.libchunky.status.RenderStatus;
import com.wertarbyte.renderservice.libchunky.status.RenderingStatus;
import com.wertarbyte.renderservice.presetcreator.gui.controllers.MainController;
import com.wertarbyte.renderservice.presetcreator.gui.models.PresetConfiguration;
import com.wertarbyte.renderservice.presetcreator.logic.PresetCreator;
import com.wertarbyte.renderservice.presetcreator.logic.PreviewListener;
import com.wertarbyte.renderservice.presetcreator.logic.PreviewRenderer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;

/**
 * The main view of the Preset Creator.
 */
public class MainView extends JFrame implements Observer, PreviewListener {
    private static final Logger LOGGER = LogManager.getLogger(MainView.class);
    private MainController controller;

    private ImagePanel previewPanel;
    private ConfigurationPanel configurationPanel;

    private JProgressBar progressBar;

    public MainView(PresetConfiguration model) {
        controller = new MainController(model, this);
        model.addObserver(this);

        setTitle("PresetCreator");
        setLayout(new BorderLayout());

        JPanel headPanel = new JPanel();
        JButton saveImageButton = new JButton("Save preview image");
        saveImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.saveImage();
            }
        });
        headPanel.add(saveImageButton);

        JButton savePresetButton = new JButton("Save preset");
        savePresetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.savePreset();
            }
        });
        headPanel.add(savePresetButton);

        JButton showAboutButton = new JButton("About");
        showAboutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.showAboutDialog();
            }
        });
        headPanel.add(showAboutButton);

        add(headPanel, BorderLayout.PAGE_START);

        previewPanel = new ImagePanel();
        previewPanel.setPreferredSize(new Dimension(640, 360));
        add(previewPanel, BorderLayout.CENTER);

        progressBar = new JProgressBar(0, 100);
        progressBar.setPreferredSize(new Dimension(-1, 20));
        progressBar.setEnabled(false);
        add(progressBar, BorderLayout.PAGE_END);

        configurationPanel = new ConfigurationPanel(controller.getPresetConfiguration());
        add(configurationPanel, BorderLayout.LINE_END);

        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                PresetCreator.getInstance().stopPreviewRenderer();
                Runtime.getRuntime().exit(0);
            }
        });
    }

    public void setPreviewImage(BufferedImage image) {
        previewPanel.setImage(image);
    }

    @Override
    public void update(Observable observable, Object o) {

    }

    @Override
    public void onRenderingStatusChanged(final PreviewRenderer renderer, final RenderingStatus status) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                progressBar.setIndeterminate(false);
                progressBar.setValue((int) status.getProgressPercentage());
                setPreviewImage(renderer.getImage());
            }
        });
    }

    @Override
    public void onStatusChanged(final PreviewRenderer renderer, final RenderStatus status) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (status.isFinished()) {
                    progressBar.setValue(0);
                    progressBar.setIndeterminate(false);
                    progressBar.setEnabled(false);

                    setPreviewImage(renderer.getImage());
                } else if (!status.isStarted()) {
                    progressBar.setValue(0);
                    progressBar.setIndeterminate(false);
                    progressBar.setEnabled(false);
                } else {
                    progressBar.setIndeterminate(true);
                    progressBar.setEnabled(true);
                }
            }
        });
    }

}
