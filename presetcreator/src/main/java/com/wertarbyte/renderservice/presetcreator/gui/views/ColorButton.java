package com.wertarbyte.renderservice.presetcreator.gui.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A color button.
 */
public class ColorButton extends JButton {
    private Color color = Color.RED;

    public ColorButton() {
        setModel(new DefaultButtonModel() {
            @Override
            protected void fireActionPerformed(ActionEvent e) {
                if (e.getActionCommand() == null || !e.getActionCommand().equals("color set")) {
                    Color newColor = JColorChooser.showDialog(ColorButton.this, "Select color", color);
                    if (newColor != null) {
                        setColor(newColor);
                        for (ActionListener a : getActionListeners()) {
                            a.actionPerformed(new ActionEvent(ColorButton.this, ActionEvent.ACTION_PERFORMED, "color set"));
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(color);
        Rectangle bounds = getBounds();
        g.fillRect(3, 3, getWidth() - 6, getHeight() - 6);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        repaint();
    }

    /**
     * Sets the color.
     *
     * @param hexColor hex color
     * @throws IllegalArgumentException if the given hex color string is invalid
     */
    public void setColor(String hexColor) {
        try {
            setColor(Color.decode(hexColor));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(hexColor + " is not a hex color", e);
        }
    }
}
