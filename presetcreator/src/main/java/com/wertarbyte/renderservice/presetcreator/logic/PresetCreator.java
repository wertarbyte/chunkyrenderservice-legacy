/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.logic;

import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.api.RenderTask;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfiguration;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfigurationBuilder;
import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.util.TempFileUtil;
import com.wertarbyte.renderservice.util.http.TransferProgressListener;
import com.wertarbyte.renderservice.util.FileUtil;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * The facade of the application.
 */
public class PresetCreator {
    /**
     * The human-readable version of the PresetCreator.
     */
    public static final String VERSION = "1.0.0";

    private static final Logger LOGGER = LogManager.getLogger(PresetCreator.class);
    private static PresetCreator instance;

    public static PresetCreator getInstance() {
        if (instance == null) {
            instance = new PresetCreator();
        }
        return instance;
    }

    private final File texturepackPath;
    private final RenderApiClient api;

    private File sceneDirectory;
    private File sceneFile;
    private SceneDescription sceneDescription;
    private PreviewRenderer renderer;


    private PresetCreator() {
        try {
            texturepackPath = TempFileUtil.extractTemporary(BundledResourceManager.getDefaultTextures());
            LOGGER.info("Temporary textures path: " + texturepackPath.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.error("Could not extract textures", e);
            throw new RuntimeException("Could not extract textures", e);
        }

        api = new RenderApiClient();
    }

    public RenderApiClient getApi() {
        return api;
    }

    public ChunkyConfigurationBuilder createDefaultChunkyConfiguration() {
        return ChunkyConfiguration.builder()
                .setJvmMinMemory(1024) //TODO make this configurable
                .setJvmMaxMemory(2048)
                .setDefaultTexturePack(texturepackPath)
                .setThreadCount(2); //2 is best, see http://llbit.se/?p=1827}
    }

    /**
     * Clears the scene directory and stops the renderer, if it was running.
     */
    public void clearScene() throws IOException {
        if (renderer != null) {
            renderer.stop();
        }

        if (sceneDirectory != null) {
            FileUtil.deleteDirectory(sceneDirectory);
        }

        try {
            sceneDirectory = TempFileUtil.createTemporaryDirectory();
        } catch (IOException e) {
            LOGGER.error("Could not create temp directory", e);
            throw e;
        }
    }

    /**
     * Downloads and extracts all scene files of the job with the given ID. Returns the preset name.
     *
     * @param jobId job ID
     * @return preset name of that job
     * @throws IOException if downloading any file fails
     */
    public String downloadSceneFiles(int jobId) throws IOException {
        RenderJob job;
        RenderTask task;
        try {
            job = PresetCreator.getInstance().getApi().getJobById(jobId);
            task = PresetCreator.getInstance().getApi().getTasksByJob(job.getId()).get(0);
        } catch (ApiClientException e) {
            LOGGER.error("Could not download job", e);
            throw new IOException(e);
        }

        try {
            File sceneTarGz = TempFileUtil.extractTemporary(getApi().getRenderDataById(task.getId(), new TransferProgressListener() {
                @Override
                public void transferred(long transferredBytes, long totalBytes) {

                }
            }));
            extractData(sceneTarGz, sceneDirectory);
            sceneTarGz.delete();
        } catch (IOException | ApiClientException e) {
            LOGGER.error("Could not download scene files", e);
            throw new IOException(e);
        }

        sceneFile = new File(sceneDirectory, "task_" + task.getId() + ".json");

        try {
            sceneDescription = PresetCreator.getInstance().getApi().getSceneDescriptionById(task.getId());
        } catch (ApiClientException e) {
            LOGGER.error("Could not get scene description");
            throw new IOException(e);
        }
        sceneDescription.setName("task_" + task.getId());

        return job.getPreset() == null || job.getPreset().trim().isEmpty() ? "default" : job.getPreset();
    }

    /**
     * Starts the preview renderer.
     *
     * @param listeners listeners to attach to the preview renderer
     * @throws IOException if starting the preview renderer fails
     */
    public void startPreviewRenderer(PreviewListener... listeners) throws IOException {
        try {
            renderer = new PreviewRenderer(sceneFile, sceneDescription);
            for (PreviewListener listener : listeners) {
                renderer.addListener(listener);
            }
            renderer.start();
        } catch (IOException e) {
            LOGGER.error("Could not start renderer", e);
            throw e;
        }
    }

    /**
     * Stops the preview renderer.
     */
    public void stopPreviewRenderer() {
        if (renderer != null) {
            renderer.stop();
            renderer = null;
        }
    }

    public void setPreset(RenderPreset preset) {
        renderer.setPreset(preset);
    }

    public void setSkymap(File skymap) {
        renderer.setSkymap(skymap);
    }

    /**
     * Gets the preview image.
     *
     * @return the preview image
     * @throws IllegalStateException if the renderer is not started
     */
    public BufferedImage getImage() {
        if (renderer == null) {
            throw new IllegalStateException("Renderer not started");
        }
        return renderer.getImage();
    }

    private void extractData(File tarGzFile, File targetDirectory) throws IOException {
        try (TarInputStream is = new TarInputStream(new GZIPInputStream(new BufferedInputStream(new FileInputStream(tarGzFile))))) {
            TarEntry entry;

            while ((entry = is.getNextEntry()) != null) {
                int count;
                byte data[] = new byte[2084];

                if (entry.isDirectory()) {
                    new File(targetDirectory, entry.getName()).mkdirs();
                    continue;
                } else {
                    int di = entry.getName().lastIndexOf('/');
                    if (di != -1) {
                        new File(targetDirectory, entry.getName().substring(0, di)).mkdirs();
                    }
                }

                try (FileOutputStream fos = new FileOutputStream(targetDirectory + "/" + entry.getName());
                     BufferedOutputStream dest = new BufferedOutputStream(fos)) {
                    while ((count = is.read(data)) != -1) {
                        dest.write(data, 0, count);
                    }

                    dest.flush();
                }
            }
        }
    }
}
