package com.wertarbyte.renderservice.presetcreator.gui.controllers;

import com.google.gson.GsonBuilder;
import com.wertarbyte.renderservice.presetcreator.gui.models.PresetConfiguration;
import com.wertarbyte.renderservice.presetcreator.gui.views.AboutView;
import com.wertarbyte.renderservice.presetcreator.gui.views.JobSelectorView;
import com.wertarbyte.renderservice.presetcreator.gui.views.MainView;
import com.wertarbyte.renderservice.presetcreator.logic.PresetCreator;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Observable;
import java.util.Observer;

/**
 * The main controller.
 */
public class MainController implements Observer {
    private static final Logger LOGGER = LogManager.getLogger(MainController.class);
    private MainView view;
    private PresetConfiguration presetConfiguration;

    public MainController(PresetConfiguration model, MainView view) {
        model.addObserver(this);

        this.view = view;
        this.presetConfiguration = model;

        new JobSelectorView(this, view).setVisible(true);
    }

    public void initialize(int jobId) throws IOException {
        PresetCreator.getInstance().clearScene();
        String preset = PresetCreator.getInstance().downloadSceneFiles(jobId);

        PresetCreator.getInstance().startPreviewRenderer(view);

        try {
            presetConfiguration.setPreset(PresetCreator.getInstance().getApi().getPreset(preset));
        } catch (ApiClientException e) {
            LOGGER.warn("Could not download preset", e);
        }

        try {
            File skymap = File.createTempFile("skymap", "rs");
            PresetCreator.getInstance().getApi().getSkymap(preset).saveTo(skymap);
            PresetCreator.getInstance().setSkymap(skymap);
        } catch (ApiClientException | IOException e) {
            LOGGER.warn("Could not download skymap", e);
        }
    }

    public void saveImage() {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
            try {
                ImageIO.write(PresetCreator.getInstance().getImage(), "png", chooser.getSelectedFile());
            } catch (IOException e) {
                LOGGER.error("Could not save image", e);
            }
        }
    }

    public void savePreset() {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
            try (Writer writer = new FileWriter(chooser.getSelectedFile())) {
                (new GsonBuilder().setPrettyPrinting().create()).toJson(presetConfiguration.getPreset(), writer);
            } catch (IOException e) {
                LOGGER.error("Could not save preset", e);
            }
        }
    }

    public PresetConfiguration getPresetConfiguration() {
        return presetConfiguration;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o == presetConfiguration) {
            PresetCreator.getInstance().setPreset(presetConfiguration.getPreset());
        }
    }

    public void showAboutDialog() {
        new AboutView().setVisible(true);
    }
}
