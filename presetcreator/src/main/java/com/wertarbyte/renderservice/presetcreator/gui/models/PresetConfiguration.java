package com.wertarbyte.renderservice.presetcreator.gui.models;

import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.api.presets.entities.PostprocessMethod;
import com.wertarbyte.renderservice.presetcreator.gui.util.DisableableObservable;

import java.util.Observable;
import java.util.Observer;

/**
 * Configuration for a {@link com.wertarbyte.renderservice.api.presets.RenderPreset}.
 */
public class PresetConfiguration extends DisableableObservable implements Observer {
    private String name = "";
    private final SunConfiguration sun = new SunConfiguration();
    private PostprocessMethod postprocess = PostprocessMethod.TONE_MAP;
    private double exposure = 1;
    private boolean emittersEnabled = true;
    private double emitterIntensity = 13;
    private boolean cloudsEnabled = false;
    private boolean biomeColorsEnabled = true;
    private final CameraConfiguration camera = new CameraConfiguration();
    private final WaterConfiguration water = new WaterConfiguration();
    private final FogConfiguration fog = new FogConfiguration();
    private final SkyConfiguration sky = new SkyConfiguration();

    public PresetConfiguration() {
        sun.addObserver(this);
        camera.addObserver(this);
        water.addObserver(this);
        fog.addObserver(this);
        sky.addObserver(this);
    }

    public RenderPreset getPreset() {
        return new RenderPreset(name, sun.getSun(), postprocess, exposure, emittersEnabled, emitterIntensity, sun.isEnabled(),
                cloudsEnabled, water.isStillWater(), water.isClearWater(), biomeColorsEnabled, fog.getDensity(), fog.getColor(),
                camera.getCamera(), sky.getYaw(), camera.isFocusSupported(), sky.getMode(), water.getOpacity(), water.getColor(),
                water.getVisibility(), water.getHeight());
    }

    public void setPreset(RenderPreset preset) {
        setIsNotifying(false);

        setName(preset.getName());
        setPostprocess(preset.getPostprocess());
        setExposure(preset.getExposure());
        setEmittersEnabled(preset.isEmittersEnabled());
        setEmitterIntensity(preset.getEmitterIntensity());
        setCloudsEnabled(preset.isCloudsEnabled());
        setBiomeColorsEnabled(preset.isBiomeColorsEnabled());


        sun.setEnabled(preset.isSunEnabled());
        sun.setAzimuth(preset.getSun().getAzimuth());
        sun.setAltitude(preset.getSun().getAltitude());
        sun.setColor(preset.getSun().getColor());
        sun.setIntensity(preset.getSun().getIntensity());

        camera.setDof(preset.getCamera().getDof());
        camera.setFocalOffset(preset.getCamera().getFocalOffset());
        camera.setFov(preset.getCamera().getFov());
        camera.setFocusSupported(preset.isFocusSupported());
        if (preset.getCamera().isInfDof()) {
            camera.setDof(Double.POSITIVE_INFINITY);
        }

        water.setColor(preset.getWaterColor());
        water.setStillWater(preset.isStillWater());
        water.setClearWater(preset.isClearWater());
        water.setVisibility(preset.getWaterVisibility());
        water.setOpacity(preset.getWaterOpacity());
        water.setHeight(preset.getWaterHeight());

        fog.setColor(preset.getFogColor());
        fog.setDensity(preset.getFogDensity());

        if (preset.getSkyMode() != null) {
            sky.setMode(preset.getSkyMode());
        }
        sky.setYaw(preset.getSkyYaw());

        setIsNotifying(true);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        setChanged();
        notifyObservers();
    }

    public SunConfiguration getSun() {
        return sun;
    }

    public PostprocessMethod getPostprocess() {
        return postprocess;
    }

    public void setPostprocess(PostprocessMethod postprocess) {
        if (this.postprocess != postprocess) {
            this.postprocess = postprocess;
            setChanged();
            notifyObservers();
        }
    }

    public double getExposure() {
        return exposure;
    }

    public void setExposure(double exposure) {
        if (this.exposure != exposure) {
            this.exposure = exposure;
            setChanged();
            notifyObservers();
        }
    }

    public boolean isEmittersEnabled() {
        return emittersEnabled;
    }

    public void setEmittersEnabled(boolean emittersEnabled) {
        if (this.emittersEnabled != emittersEnabled) {
            this.emittersEnabled = emittersEnabled;
            setChanged();
            notifyObservers();
        }
    }

    public double getEmitterIntensity() {
        return emitterIntensity;
    }

    public void setEmitterIntensity(double emitterIntensity) {
        if (this.emitterIntensity != emitterIntensity) {
            this.emitterIntensity = emitterIntensity;
            setChanged();
            notifyObservers();
        }
    }

    public boolean isCloudsEnabled() {
        return cloudsEnabled;
    }

    public void setCloudsEnabled(boolean cloudsEnabled) {
        if (this.cloudsEnabled != cloudsEnabled) {
            this.cloudsEnabled = cloudsEnabled;
            setChanged();
            notifyObservers();
        }
    }

    public boolean isBiomeColorsEnabled() {
        return biomeColorsEnabled;
    }

    public void setBiomeColorsEnabled(boolean biomeColorsEnabled) {
        if (this.biomeColorsEnabled != biomeColorsEnabled) {
            this.biomeColorsEnabled = biomeColorsEnabled;
            setChanged();
            notifyObservers();
        }
    }

    public CameraConfiguration getCamera() {
        return camera;
    }

    public WaterConfiguration getWater() {
        return water;
    }

    public FogConfiguration getFog() {
        return fog;
    }

    public SkyConfiguration getSky() {
        return sky;
    }

    @Override
    public void update(Observable o, Object arg) {
        setChanged();
        notifyObservers();
    }
}
