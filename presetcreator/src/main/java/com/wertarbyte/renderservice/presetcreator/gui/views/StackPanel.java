package com.wertarbyte.renderservice.presetcreator.gui.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * A vertical stack panel.
 */
public class StackPanel extends JPanel {
    private GridBagConstraints listGridBagConstraints;
    private JPanel list;

    public StackPanel(boolean scrollable) {
        list = new JPanel(new GridBagLayout());
        listGridBagConstraints = new GridBagConstraints();

        //add filler so that everything aligns to top
        listGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        listGridBagConstraints.weightx = 1;
        listGridBagConstraints.weighty = 1;
        list.add(new JPanel(), listGridBagConstraints);

        if (scrollable) {
            super.addImpl(new JScrollPane(list), BorderLayout.CENTER, 0);
            list.setBorder(new EmptyBorder(5, 5, 5, 5));
        } else {
            super.addImpl(list, BorderLayout.CENTER, 0);
        }
    }

    @Override
    protected void addImpl(Component comp, Object constraints, int index) {
        listGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        listGridBagConstraints.weightx = 1;
        listGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;

        if (index == -1) {
            index = list.getComponentCount() - 1;
        }
        list.add(comp, listGridBagConstraints, index);

        validate();
        repaint();
    }
}
