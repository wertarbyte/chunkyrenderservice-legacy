package com.wertarbyte.renderservice.presetcreator.gui.models;

import com.wertarbyte.renderservice.api.presets.entities.SkyMode;

import java.util.Observable;

/**
 * Sky configuration for a {@link com.wertarbyte.renderservice.api.presets.RenderPreset}.
 */
public class SkyConfiguration extends Observable {
    private double yaw;
    private SkyMode mode = SkyMode.SKYMAP_PANORAMIC;

    public double getYaw() {
        return yaw;
    }

    public void setYaw(double yaw) {
        if (this.yaw != yaw) {
            this.yaw = yaw;
            setChanged();
            notifyObservers();
        }
    }

    public SkyMode getMode() {
        return mode;
    }

    public void setMode(SkyMode mode) {
        if (this.mode != mode) {
            this.mode = mode;
            setChanged();
            notifyObservers();
        }
    }
}
