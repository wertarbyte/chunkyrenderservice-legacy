package com.wertarbyte.renderservice.presetcreator.gui.models;

import java.util.Observable;

/**
 * Water configuration for a {@link com.wertarbyte.renderservice.api.presets.RenderPreset}.
 */
public class WaterConfiguration extends Observable {
    private boolean stillWater = false;
    private boolean clearWater = false;
    private double opacity = 0.42;
    private String color = null; //TODO add default value
    private double visibility = 9.0;
    private int height = 0;

    public boolean isStillWater() {
        return stillWater;
    }

    public void setStillWater(boolean stillWater) {
        if (this.stillWater != stillWater) {
            this.stillWater = stillWater;
            setChanged();
            notifyObservers();
        }
    }

    public boolean isClearWater() {
        return clearWater;
    }

    public void setClearWater(boolean clearWater) {
        if (this.clearWater != clearWater) {
            this.clearWater = clearWater;
            setChanged();
            notifyObservers();
        }
    }

    public double getOpacity() {
        return opacity;
    }

    public void setOpacity(double opacity) {
        if (this.opacity != opacity) {
            this.opacity = opacity;
            setChanged();
            notifyObservers();
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        if ((this.color == null && color != null) || (this.color != null && !this.color.equals(color))) {
            this.color = color;
            setChanged();
            notifyObservers();
        }
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        if (this.visibility != visibility) {
            this.visibility = visibility;
            setChanged();
            notifyObservers();
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (this.height != height) {
            this.height = height;
            setChanged();
            notifyObservers();
        }
    }
}
