/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.gui.models;

import com.wertarbyte.renderservice.api.presets.entities.Camera;

import java.util.Observable;

/**
 * Camera configuration for a {@link com.wertarbyte.renderservice.api.presets.RenderPreset}.
 */
public class CameraConfiguration extends Observable {
    private double fov = 70;
    private double dof = Double.POSITIVE_INFINITY;
    private double focalOffset = 2;
    private boolean focusSupported = false;

    public double getFov() {
        return fov;
    }

    public void setFov(double fov) {
        if (this.fov != fov) {
            this.fov = fov;
            setChanged();
            notifyObservers();
        }
    }

    public double getDof() {
        return dof;
    }

    public void setDof(double dof) {
        if (this.dof != dof) {
            this.dof = dof;
            setChanged();
            notifyObservers();
        }
    }

    public double getFocalOffset() {
        return focalOffset;
    }

    public void setFocalOffset(double focalOffset) {
        if (this.focalOffset != focalOffset) {
            this.focalOffset = focalOffset;
            setChanged();
            notifyObservers();
        }
    }

    public boolean isFocusSupported() {
        return focusSupported;
    }

    public void setFocusSupported(boolean focusSupported) {
        if (this.focusSupported != focusSupported) {
            this.focusSupported = focusSupported;
            setChanged();
            notifyObservers();
        }
    }

    public Camera getCamera() {
        return new Camera(fov, Double.isInfinite(dof) ? 0 : dof, Double.isInfinite(dof), fov);
    }
}
