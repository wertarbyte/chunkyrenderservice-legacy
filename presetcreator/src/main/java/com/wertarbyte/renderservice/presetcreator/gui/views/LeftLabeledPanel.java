package com.wertarbyte.renderservice.presetcreator.gui.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * A panel with a label on the left side.
 */
public class LeftLabeledPanel extends JPanel {
    private JLabel label;

    public LeftLabeledPanel(String label, JComponent child) {
        setLayout(new BorderLayout());
        this.label = new JLabel(label);
        this.label.setBorder(new EmptyBorder(0, 0, 0, 5));
        add(this.label, BorderLayout.LINE_START);

        add(child, BorderLayout.CENTER);
    }

    public String getLabel() {
        return label.getText();
    }

    public void setLabel(String label) {
        this.label.setText(label);
    }
}
