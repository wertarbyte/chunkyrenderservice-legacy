/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.logic;

import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.wertarbyte.renderservice.api.CommonApiClient;
import com.wertarbyte.renderservice.api.RenderTask;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.api.presets.SkymapContainer;
import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.util.http.CountingInputStream;
import com.wertarbyte.renderservice.util.http.TransferProgressListener;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

public class RenderApiClient extends CommonApiClient {
    public InputStream getRenderDataById(int jobId, final TransferProgressListener listener) throws ApiClientException {
        HttpResponse response = client.get("/tasks/" + jobId + "/data.tar.gz");

        HttpEntity respEntity = response.getEntity();
        if (respEntity != null) {
            try {
                return new CountingInputStream(respEntity.getContent(), listener, respEntity.getContentLength());
            } catch (IOException e) {
                throw new ApiClientException("Connection error", e);
            }
        } else {
            throw new ApiClientException("Empty response", response.getStatusLine().getStatusCode());
        }
    }

    public SkymapContainer getSkymap(String presetName) throws ApiClientException {
        HttpResponse response = client.get("/presets/" + presetName + "/skymap");
        if (response.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = response.getEntity();
            if (response.getEntity().getContentType().getValue().equals("image/png")) {
                try {
                    return new SkymapContainer(SkymapContainer.Format.Png, entity.getContent());
                } catch (IOException e) {
                    throw new ApiClientException("Error while getting the content.", e);
                }
            } else if (entity.getContentType().getValue().equals("image/jpeg")) {
                try {
                    return new SkymapContainer(SkymapContainer.Format.Jpeg, entity.getContent());
                } catch (IOException e) {
                    throw new ApiClientException("Error while getting the content.", e);
                }
            } else {
                throw new ApiClientException("Unknown skymap format: " + response.getEntity().getContentType().getValue(), 200);
            }
        }
        return null;
    }

    public RenderPreset getPreset(String presetName) throws ApiClientException {
        return client.getFromJson("/presets/" + presetName, RenderPreset.class);
    }

    @Override
    protected String getUserAgent() {
        return "RenderService Renderer v2";
    }

    public SceneDescription getSceneDescriptionById(int id) throws ApiClientException {
        try {
            try (InputStreamReader reader = new InputStreamReader(client.get("/tasks/" + id + "/scene.json").getEntity().getContent())) {
                return new SceneDescription(new JsonParser().parse(reader).getAsJsonObject());
            }
        } catch (IOException e) {
            throw new ApiClientException("Could not read json", e);
        }
    }

    public List<RenderTask> getTasksByJob(int jobId) throws ApiClientException {
        Type listType = new TypeToken<List<RenderTask>>() {
        }.getType();
        return client.getFromJson("/jobs/" + jobId + "/tasks", listType);
    }
}
