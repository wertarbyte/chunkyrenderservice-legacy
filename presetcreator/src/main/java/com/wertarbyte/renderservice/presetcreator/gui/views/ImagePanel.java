package com.wertarbyte.renderservice.presetcreator.gui.views;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * A panel that displays a {@link BufferedImage}.
 */
public class ImagePanel extends JPanel {
    private BufferedImage image;

    /**
     * Gets the displayed image.
     *
     * @return the displayed image
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * Sets the displayed image.
     *
     * @param image the displayed image
     */
    public void setImage(BufferedImage image) {
        this.image = image;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (image != null) {
            g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters
        }
    }
}
