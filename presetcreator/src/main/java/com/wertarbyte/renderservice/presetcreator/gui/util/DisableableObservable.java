package com.wertarbyte.renderservice.presetcreator.gui.util;

import java.util.Observable;

/**
 * An {@link Observable} that can be disabled so that it doesn't notify observers.
 */
public class DisableableObservable extends Observable {
    private boolean isNotifying = true;

    /**
     * Checks if this observable notifies observers.
     *
     * @return true if this observer notifies observers, false if not
     */
    public boolean isNotifying() {
        return isNotifying;
    }

    /**
     * Enables or disables this observable. If it is enabled, {@link #notifyObservers()} is called automatically.
     *
     * @param enabled whether or not observers should be notified when {@link #notifyObservers()} is called
     */
    public void setIsNotifying(boolean enabled) {
        this.isNotifying = enabled;

        if (this.isNotifying) {
            notifyObservers();
        }
    }

    @Override
    public void notifyObservers() {
        if (isNotifying()) {
            super.notifyObservers();
        }
    }

    @Override
    public void notifyObservers(Object arg) {
        if (isNotifying()) {
            super.notifyObservers(arg);
        }
    }
}
