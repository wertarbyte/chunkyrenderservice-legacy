/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.gui.views;

import com.wertarbyte.renderservice.presetcreator.gui.controllers.MainController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * A window to select a job to start with.
 */
public class JobSelectorView extends JDialog {
    private static final Logger LOGGER = LogManager.getLogger(JobSelectorView.class);

    public JobSelectorView(final MainController mainController, Window owner) {
        super(owner, "Select start job", Dialog.ModalityType.MODELESS);

        setLayout(new BorderLayout());
        JPanel configPanel = new JPanel();
        add(configPanel, BorderLayout.CENTER);

        configPanel.setLayout(new FlowLayout());
        configPanel.add(new JLabel("Job: "));

        final JSpinner jobId = new JSpinner(new SpinnerNumberModel(42, 0, Integer.MAX_VALUE, 1));
        configPanel.add(jobId);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        add(buttonPanel, BorderLayout.PAGE_END);

        final JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        jobId.setEnabled(false);
                        okButton.setEnabled(false);
                        okButton.setText("Loading...");
                        try {
                            mainController.initialize((Integer) jobId.getValue());
                            dispose();
                        } catch (IOException e) {
                            jobId.setEnabled(true);
                            okButton.setEnabled(true);
                            okButton.setText("OK");
                            JOptionPane.showMessageDialog(JobSelectorView.this,
                                    "Could not download job data.",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }).start();
            }
        });
        buttonPanel.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Runtime.getRuntime().exit(0);
            }
        });
        buttonPanel.add(cancelButton);

        pack();
        setLocationRelativeTo(null);
    }
}
