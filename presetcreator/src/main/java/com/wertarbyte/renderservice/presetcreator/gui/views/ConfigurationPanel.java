package com.wertarbyte.renderservice.presetcreator.gui.views;

import com.wertarbyte.renderservice.api.presets.entities.PostprocessMethod;
import com.wertarbyte.renderservice.api.presets.entities.SkyMode;
import com.wertarbyte.renderservice.presetcreator.gui.models.PresetConfiguration;
import com.wertarbyte.renderservice.presetcreator.gui.util.AsyncActionListener;
import com.wertarbyte.renderservice.presetcreator.gui.util.AsyncChangeListener;
import com.wertarbyte.renderservice.presetcreator.logic.PresetCreator;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Configuration panel for presets.
 */
public class ConfigurationPanel extends StackPanel {
    private final PresetConfiguration configuration;

    public ConfigurationPanel(final PresetConfiguration configuration) {
        super(true);
        this.configuration = configuration;

        add(buildEmittersPanel());
        add(buildSunPanel());
        add(buildWaterPanel());
        add(buildFogPanel());
        add(buildCameraPanel());
        add(buildSkyPanel());
        add(buildOtherPanel());
    }

    private JPanel buildEmittersPanel() {
        JPanel emittersPanel = new JPanel();
        emittersPanel.setLayout(new BoxLayout(emittersPanel, BoxLayout.PAGE_AXIS));
        emittersPanel.setBorder(BorderFactory.createTitledBorder("Emitters"));
        add(emittersPanel);

        final JCheckBox emittersCheckbox = new JCheckBox("Enabled", configuration.isEmittersEnabled());
        emittersCheckbox.setPreferredSize(new Dimension(100, 25));
        emittersCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.setEmittersEnabled(emittersCheckbox.isSelected());
            }
        });
        emittersPanel.add(emittersCheckbox);

        final JSpinner emittersIntensitySpinner = new JSpinner(new SpinnerNumberModel(configuration.getEmitterIntensity(), 0, Double.MAX_VALUE, 0.5));
        emittersIntensitySpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.setEmitterIntensity((Double) emittersIntensitySpinner.getValue());
            }
        });
        emittersIntensitySpinner.setPreferredSize(new Dimension(100, 25));
        emittersPanel.add(new LeftLabeledPanel("Intensity:", emittersIntensitySpinner));

        configuration.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                emittersCheckbox.setSelected(configuration.isEmittersEnabled());
                emittersIntensitySpinner.setValue(configuration.getEmitterIntensity());
            }
        });

        return emittersPanel;
    }

    private JPanel buildSunPanel() {
        JPanel sunPanel = new JPanel();
        sunPanel.setLayout(new BoxLayout(sunPanel, BoxLayout.PAGE_AXIS));
        sunPanel.setBorder(BorderFactory.createTitledBorder("Sun"));
        add(sunPanel);

        final JCheckBox sunCheckbox = new JCheckBox("Enabled", configuration.getSun().isEnabled());
        sunCheckbox.setPreferredSize(new Dimension(100, 25));
        sunCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.getSun().setEnabled(sunCheckbox.isSelected());
            }
        });
        sunPanel.add(sunCheckbox);

        final JSpinner sunIntensitySpinner = new JSpinner(new SpinnerNumberModel(configuration.getSun().getIntensity(), 0, Double.MAX_VALUE, 0.5));
        sunIntensitySpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getSun().setIntensity((Double) sunIntensitySpinner.getValue());
            }
        });
        sunIntensitySpinner.setPreferredSize(new Dimension(100, 25));
        sunPanel.add(new LeftLabeledPanel("Intensity:", sunIntensitySpinner));

        final ColorButton sunColorButton = new ColorButton();
        sunColorButton.setColor(configuration.getSun().getColor());
        sunColorButton.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                Color color = sunColorButton.getColor();
                configuration.getSun().setColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
            }
        });
        sunPanel.add(new LeftLabeledPanel("Color:", sunColorButton));

        final JSpinner sunAltitudeSpinner = new JSpinner(new SpinnerNumberModel(configuration.getSun().getAltitude(), 0, 360, 1));
        sunAltitudeSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getSun().setAltitude((Double) sunAltitudeSpinner.getValue());
            }
        });
        sunAltitudeSpinner.setPreferredSize(new Dimension(100, 25));
        sunPanel.add(new LeftLabeledPanel("Altitude:", sunAltitudeSpinner));

        final JSpinner sunAzimuthSpinner = new JSpinner(new SpinnerNumberModel(configuration.getSun().getAzimuth(), 0, 360, 1));
        sunAzimuthSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getSun().setAzimuth((Double) sunAzimuthSpinner.getValue());
            }
        });
        sunAzimuthSpinner.setPreferredSize(new Dimension(100, 25));
        sunPanel.add(new LeftLabeledPanel("Azimuth:", sunAzimuthSpinner));

        configuration.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                sunCheckbox.setSelected(configuration.getSun().isEnabled());
                sunIntensitySpinner.setValue(configuration.getSun().getIntensity());
                sunColorButton.setColor(configuration.getSun().getColor());
                sunAltitudeSpinner.setValue(configuration.getSun().getAltitude());
                sunAzimuthSpinner.setValue(configuration.getSun().getAzimuth());
            }
        });

        return sunPanel;
    }

    private JPanel buildWaterPanel() {
        JPanel waterPanel = new JPanel();
        waterPanel.setLayout(new BoxLayout(waterPanel, BoxLayout.PAGE_AXIS));
        waterPanel.setBorder(BorderFactory.createTitledBorder("Water"));
        add(waterPanel);

        final JCheckBox stillWaterCheckbox = new JCheckBox("Still water", configuration.getWater().isStillWater());
        stillWaterCheckbox.setPreferredSize(new Dimension(100, 25));
        stillWaterCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.getWater().setStillWater(stillWaterCheckbox.isSelected());
            }
        });
        waterPanel.add(stillWaterCheckbox);

        final JCheckBox clearWaterCheckbox = new JCheckBox("Clear water", configuration.getWater().isClearWater());
        clearWaterCheckbox.setPreferredSize(new Dimension(100, 25));
        clearWaterCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.getWater().setClearWater(clearWaterCheckbox.isSelected());
            }
        });
        waterPanel.add(clearWaterCheckbox);

        final JSpinner waterOpacitySpinner = new JSpinner(new SpinnerNumberModel(configuration.getWater().getOpacity(), 0, 1, 0.1));
        waterOpacitySpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getWater().setOpacity((Double) waterOpacitySpinner.getValue());
            }
        });
        waterOpacitySpinner.setPreferredSize(new Dimension(100, 25));
        waterPanel.add(new LeftLabeledPanel("Opacity:", waterOpacitySpinner));

        final ColorButton waterColorButton = new ColorButton();
        if (configuration.getWater().getColor() != null) { //TODO add default color and remove this check
            waterColorButton.setColor(configuration.getWater().getColor());
        }
        waterColorButton.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                Color color = waterColorButton.getColor();
                configuration.getWater().setColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
            }
        });
        waterPanel.add(new LeftLabeledPanel("Color:", waterColorButton));

        final JSpinner waterVisibilitySpinner = new JSpinner(new SpinnerNumberModel(configuration.getWater().getVisibility(), 0, Double.MAX_VALUE, 1));
        waterVisibilitySpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getWater().setVisibility((Double) waterVisibilitySpinner.getValue());
            }
        });
        waterVisibilitySpinner.setPreferredSize(new Dimension(100, 25));
        waterPanel.add(new LeftLabeledPanel("Visibility:", waterVisibilitySpinner));

        final JSpinner waterHeightSpinner = new JSpinner(new SpinnerNumberModel(configuration.getWater().getHeight(), 0, 255, 1));
        waterHeightSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getWater().setHeight((Integer) waterHeightSpinner.getValue());
            }
        });
        waterHeightSpinner.setPreferredSize(new Dimension(100, 25));
        waterPanel.add(new LeftLabeledPanel("Height (for water worlds):", waterHeightSpinner));

        configuration.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                stillWaterCheckbox.setSelected(configuration.getWater().isStillWater());
                clearWaterCheckbox.setSelected(configuration.getWater().isClearWater());
                waterOpacitySpinner.setValue(configuration.getWater().getOpacity());
                if (configuration.getWater().getColor() != null) {
                    waterColorButton.setColor(configuration.getWater().getColor());
                } else {
                    waterColorButton.setColor("#ffffff"); //TODO use default water color
                }
                waterVisibilitySpinner.setValue(configuration.getWater().getVisibility());
                waterHeightSpinner.setValue(configuration.getWater().getHeight());
            }
        });

        return waterPanel;
    }

    private JPanel buildCameraPanel() {
        JPanel cameraPanel = new JPanel();
        cameraPanel.setLayout(new BoxLayout(cameraPanel, BoxLayout.PAGE_AXIS));
        cameraPanel.setBorder(BorderFactory.createTitledBorder("Camera"));
        add(cameraPanel);

        final JCheckBox focusSupportedCheckbox = new JCheckBox("Focus supported", configuration.getCamera().isFocusSupported());
        focusSupportedCheckbox.setPreferredSize(new Dimension(100, 25));
        focusSupportedCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.getCamera().setFocusSupported(focusSupportedCheckbox.isSelected());
            }
        });
        cameraPanel.add(focusSupportedCheckbox);

        final JSpinner dofSpinner = new JSpinner(new SpinnerNumberModel(Math.min(configuration.getCamera().getDof(), Double.MAX_VALUE), 0, Double.MAX_VALUE, 0.1));
        dofSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getCamera().setDof((Double) dofSpinner.getValue());
            }
        });
        dofSpinner.setPreferredSize(new Dimension(100, 25));
        if (Double.isInfinite(configuration.getCamera().getDof())) {
            dofSpinner.setEnabled(false);
        }
        cameraPanel.add(new LeftLabeledPanel("Depth of field:", dofSpinner));

        final JCheckBox infiniteDofCheckbox = new JCheckBox("Infinite DOF", Double.isInfinite(configuration.getCamera().getDof()));
        infiniteDofCheckbox.setPreferredSize(new Dimension(100, 25));
        infiniteDofCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (infiniteDofCheckbox.isSelected()) {
                            configuration.getCamera().setDof(Double.POSITIVE_INFINITY);
                        } else {
                            configuration.getCamera().setDof((Double) dofSpinner.getValue());
                        }
                    }
                });
            }
        });
        cameraPanel.add(infiniteDofCheckbox);

        final JSpinner focalOffsetSpinner = new JSpinner(new SpinnerNumberModel(configuration.getCamera().getFocalOffset(), 0, Double.MAX_VALUE, 1));
        focalOffsetSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getCamera().setFocalOffset((Double) focalOffsetSpinner.getValue());
            }
        });
        focalOffsetSpinner.setPreferredSize(new Dimension(100, 25));
        cameraPanel.add(new LeftLabeledPanel("Focal offset:", focalOffsetSpinner));

        final JSpinner fovSpinner = new JSpinner(new SpinnerNumberModel(configuration.getWater().getVisibility(), 0, Double.MAX_VALUE, 1));
        fovSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getCamera().setFov((Double) fovSpinner.getValue());
            }
        });
        fovSpinner.setPreferredSize(new Dimension(100, 25));
        cameraPanel.add(new LeftLabeledPanel("Field of view:", fovSpinner));

        configuration.getCamera().addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                focusSupportedCheckbox.setSelected(configuration.getCamera().isFocusSupported());

                if (Double.isInfinite(configuration.getCamera().getDof())) {
                    infiniteDofCheckbox.setSelected(true);
                    dofSpinner.setEnabled(false);
                } else {
                    infiniteDofCheckbox.setSelected(false);
                    dofSpinner.setEnabled(true);
                }

                focalOffsetSpinner.setValue(configuration.getCamera().getFocalOffset());
                fovSpinner.setValue(configuration.getCamera().getFov());
            }
        });

        return cameraPanel;
    }

    private JPanel buildFogPanel() {
        JPanel fogPanel = new JPanel();
        fogPanel.setLayout(new BoxLayout(fogPanel, BoxLayout.PAGE_AXIS));
        fogPanel.setBorder(BorderFactory.createTitledBorder("Fog"));

        final JSpinner fogDensitySpinner = new JSpinner(new SpinnerNumberModel(configuration.getFog().getDensity(), 0, 1, 0.05));
        fogDensitySpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getFog().setDensity((Double) fogDensitySpinner.getValue());
            }
        });
        fogDensitySpinner.setPreferredSize(new Dimension(100, 25));
        fogPanel.add(new LeftLabeledPanel("Density:", fogDensitySpinner));

        final ColorButton fogColorButton = new ColorButton();
        fogColorButton.setColor(configuration.getFog().getColor());
        fogColorButton.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                Color color = fogColorButton.getColor();
                configuration.getFog().setColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
            }
        });
        fogPanel.add(new LeftLabeledPanel("Color:", fogColorButton));

        configuration.getFog().addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                fogDensitySpinner.setValue(configuration.getFog().getDensity());

                if (configuration.getFog().getColor() != null) {
                    fogColorButton.setColor(configuration.getFog().getColor());
                } else {
                    fogColorButton.setColor("#ffffff");
                }
            }
        });

        return fogPanel;
    }

    private JPanel buildSkyPanel() {
        JPanel skyPanel = new JPanel();
        skyPanel.setLayout(new BoxLayout(skyPanel, BoxLayout.PAGE_AXIS));
        skyPanel.setBorder(BorderFactory.createTitledBorder("Sky"));

        final JSpinner skyYawSpinner = new JSpinner(new SpinnerNumberModel(configuration.getSky().getYaw(), 0, 360, 1));
        skyYawSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.getSky().setYaw((Double) skyYawSpinner.getValue());
            }
        });
        skyYawSpinner.setPreferredSize(new Dimension(100, 25));
        skyPanel.add(new LeftLabeledPanel("Yaw:", skyYawSpinner));


        JButton skymapButton = new JButton("Load skymap...");
        skymapButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser chooser = new JFileChooser();
                if (chooser.showSaveDialog(ConfigurationPanel.this) == JFileChooser.APPROVE_OPTION) {
                    PresetCreator.getInstance().setSkymap(chooser.getSelectedFile());
                }
            }
        });
        skyPanel.add(skymapButton);

        final JComboBox<String> skyModeBox = new JComboBox<>(new String[]{
                "Panoramic skymap",
                "Spherical skymap",
                "Simulated"
        });
        switch (configuration.getSky().getMode()) {
            case SKYMAP_PANORAMIC:
                skyModeBox.setSelectedIndex(0);
                break;
            case SKYMAP_SPHERICAL:
                skyModeBox.setSelectedIndex(1);
                break;
            case SIMULATED:
                skyModeBox.setSelectedIndex(2);
                break;
        }
        skyModeBox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                switch (skyModeBox.getSelectedIndex()) {
                    case 0:
                        configuration.getSky().setMode(SkyMode.SKYMAP_PANORAMIC);
                        break;
                    case 1:
                        configuration.getSky().setMode(SkyMode.SKYMAP_SPHERICAL);
                        break;
                    case 2:
                        configuration.getSky().setMode(SkyMode.SIMULATED);
                        break;
                }
            }
        });
        skyPanel.add(new LeftLabeledPanel("Sky mode:", skyModeBox));

        configuration.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                skyYawSpinner.setValue(configuration.getSky().getYaw());

                switch (configuration.getSky().getMode()) {
                    case SKYMAP_PANORAMIC:
                        skyModeBox.setSelectedIndex(0);
                        break;
                    case SKYMAP_SPHERICAL:
                        skyModeBox.setSelectedIndex(1);
                        break;
                    case SIMULATED:
                        skyModeBox.setSelectedIndex(2);
                        break;
                }
            }
        });

        return skyPanel;
    }

    private JPanel buildOtherPanel() {
        JPanel otherPanel = new JPanel();
        otherPanel.setLayout(new BoxLayout(otherPanel, BoxLayout.PAGE_AXIS));
        otherPanel.setBorder(BorderFactory.createTitledBorder("Other"));
        add(otherPanel);

        final JTextField nameField = new JTextField(configuration.getName());
        nameField.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.setName(nameField.getText());
            }
        });
        nameField.setPreferredSize(new Dimension(100, 25));
        otherPanel.add(new LeftLabeledPanel("Name:", nameField));

        final JSpinner exposureSpinner = new JSpinner(new SpinnerNumberModel(configuration.getExposure(), 0, Double.MAX_VALUE, 0.05));
        exposureSpinner.addChangeListener(new AsyncChangeListener() {
            @Override
            public void stateChangedAsync(ChangeEvent changeEvent) {
                configuration.setExposure((Double) exposureSpinner.getValue());
            }
        });
        exposureSpinner.setPreferredSize(new Dimension(100, 25));
        otherPanel.add(new LeftLabeledPanel("Exposure:", exposureSpinner));

        final JComboBox<String> postprocessMethodBox = new JComboBox<>(new String[]{
                "None",
                "Tone Map",
                "Gamma Correction"
        });
        switch (configuration.getPostprocess()) {
            case NONE:
                postprocessMethodBox.setSelectedIndex(0);
                break;
            case GAMMA_CORRECTION:
                postprocessMethodBox.setSelectedIndex(1);
                break;
            case TONE_MAP:
                postprocessMethodBox.setSelectedIndex(2);
                break;
        }
        postprocessMethodBox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                switch (postprocessMethodBox.getSelectedIndex()) {
                    case 0:
                        configuration.setPostprocess(PostprocessMethod.NONE);
                        break;
                    case 1:
                        configuration.setPostprocess(PostprocessMethod.GAMMA_CORRECTION);
                        break;
                    case 2:
                        configuration.setPostprocess(PostprocessMethod.TONE_MAP);
                        break;
                }
            }
        });
        postprocessMethodBox.setPreferredSize(new Dimension(100, 25));
        otherPanel.add(new LeftLabeledPanel("Postprocessing:", postprocessMethodBox));

        final JCheckBox cloudsCheckbox = new JCheckBox("Clouds enabled", configuration.isCloudsEnabled());
        cloudsCheckbox.setPreferredSize(new Dimension(100, 25));
        cloudsCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.setCloudsEnabled(cloudsCheckbox.isSelected());
            }
        });
        otherPanel.add(cloudsCheckbox);

        final JCheckBox biomeColorsCheckbox = new JCheckBox("Biome colors enabled", configuration.isBiomeColorsEnabled());
        biomeColorsCheckbox.setPreferredSize(new Dimension(100, 25));
        biomeColorsCheckbox.addActionListener(new AsyncActionListener() {
            @Override
            public void actionPerformedAsync(ActionEvent actionEvent) {
                configuration.setBiomeColorsEnabled(biomeColorsCheckbox.isSelected());
            }
        });
        otherPanel.add(biomeColorsCheckbox);

        configuration.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                exposureSpinner.setValue(configuration.getExposure());

                switch (configuration.getPostprocess()) {
                    case NONE:
                        postprocessMethodBox.setSelectedIndex(0);
                        break;
                    case GAMMA_CORRECTION:
                        postprocessMethodBox.setSelectedIndex(1);
                        break;
                    case TONE_MAP:
                        postprocessMethodBox.setSelectedIndex(2);
                        break;
                }

                cloudsCheckbox.setSelected(configuration.isCloudsEnabled());
                biomeColorsCheckbox.setSelected(configuration.isBiomeColorsEnabled());
            }
        });

        return otherPanel;
    }
}
