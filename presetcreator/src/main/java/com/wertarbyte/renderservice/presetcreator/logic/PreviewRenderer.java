/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.logic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfiguration;
import com.wertarbyte.renderservice.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.ChunkyInstanceWrapper;
import com.wertarbyte.renderservice.libchunky.RenderListener;
import com.wertarbyte.renderservice.libchunky.status.RenderStatus;
import com.wertarbyte.renderservice.libchunky.status.RenderingStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A renderer for previews.
 */
public class PreviewRenderer implements RenderListener {
    private static final Logger LOGGER = LogManager.getLogger(PreviewRenderer.class);
    private RenderPreset preset;
    private final SceneDescription sceneDescription;
    private final List<PreviewListener> listeners;
    private final ChunkyInstanceWrapper chunky;
    private final ChunkyConfiguration config;

    public PreviewRenderer(File sceneFile, SceneDescription sceneDescription) throws IOException {
        this.sceneDescription = sceneDescription;
        this.listeners = new ArrayList<>();

        config = PresetCreator.getInstance().createDefaultChunkyConfiguration()
                .setDumpFrequency(5)
                .setTargetSpp(5)
                .setSceneDirectory(sceneFile.getParentFile())
                .setSceneName(sceneFile.getName().substring(0, sceneFile.getName().length() - 5))
                .build();

        chunky = new ChunkyInstanceWrapper();
        chunky.setSceneDirectory(config.getSceneDirectory());
        chunky.setScene(sceneDescription);
        chunky.setTexturepack(config.getDefaultTexturePack());
        chunky.setThreadCount(config.getThreadCount());
        chunky.setTargetSpp(config.getTargetSpp());
        chunky.addListener(this);

        if (new File(config.getSceneDirectory(), config.getSceneName() + ".dump").delete()) {
            LOGGER.info("Removed existing render dump");
        }
    }

    public void setPreset(RenderPreset preset) {
        this.preset = preset;
        chunky.setScene(getSceneDescription());
    }

    public void setSkymap(File skymap) {
        if (skymap != null) {
            sceneDescription.getSky().setSkymap(skymap.getAbsolutePath());
        } else {
            sceneDescription.getSky().setSkymap("");
        }

        chunky.setScene(getSceneDescription());
    }

    private SceneDescription getSceneDescription() {
        SceneDescription sceneDescription = this.sceneDescription;
        sceneDescription.setWidth(640);
        sceneDescription.setHeight(360);
        sceneDescription.setName(config.getSceneName());
        if (preset != null) {
            sceneDescription.applyPreset(preset);
        }
        return sceneDescription;
    }

    public BufferedImage getImage() {
        return chunky.getImage();
    }

    public void addListener(PreviewListener listener) {
        listeners.add(listener);
    }

    @Override
    public void onFinished() {
        for (PreviewListener listener : new ArrayList<>(listeners)) {
            listener.onStatusChanged(this, new RenderStatus(true, true));
        }
    }

    @Override
    public void onRenderStatusChanged(int currentSpp, int targetSpp) {
        for (PreviewListener listener : new ArrayList<>(listeners)) {
            listener.onRenderingStatusChanged(this, new RenderingStatus("Rendering", currentSpp, targetSpp));
        }
    }

    @Override
    public void onStatusChanged(ChunkyTask task) {
        for (PreviewListener listener : new ArrayList<>(listeners)) {
            listener.onStatusChanged(this, new RenderStatus(true, false));
        }
    }

    public void start() throws IOException {
        chunky.start();
    }

    public void stop() {
        chunky.stop();
    }
}
