package com.wertarbyte.renderservice.presetcreator.gui.models;

import java.util.Observable;

/**
 * Fog configuration for a {@link com.wertarbyte.renderservice.api.presets.RenderPreset}.
 */
public class FogConfiguration extends Observable {
    private double density = 0;
    private String color = "#ffffff";

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        if (this.density != density) {
            this.density = density;
            setChanged();
            notifyObservers();
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        if ((this.color == null && color != null) || (this.color != null && !this.color.equals(color))) {
            this.color = color;
            setChanged();
            notifyObservers();
        }
    }
}
