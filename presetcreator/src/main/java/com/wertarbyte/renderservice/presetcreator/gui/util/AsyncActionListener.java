/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.gui.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * An asynchronous action listener.
 */
public abstract class AsyncActionListener implements ActionListener {
    @Override
    public final void actionPerformed(final ActionEvent actionEvent) {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                actionPerformedAsync(actionEvent);
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public abstract void actionPerformedAsync(ActionEvent actionEvent);
}
