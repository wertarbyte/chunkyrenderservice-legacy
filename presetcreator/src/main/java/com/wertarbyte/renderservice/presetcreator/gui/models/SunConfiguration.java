/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.presetcreator.gui.models;

import com.wertarbyte.renderservice.api.presets.entities.Sun;

import java.util.Observable;

/**
 * Sun configuration for a {@link com.wertarbyte.renderservice.api.presets.RenderPreset}.
 */
public class SunConfiguration extends Observable {
    private boolean enabled = true;
    private double altitude = 1;
    private double azimuth = 1;
    private double intensity = 1.25;
    private String color = "#ffffff";

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean isEnabled) {
        if (this.enabled != isEnabled) {
            this.enabled = isEnabled;
            setChanged();
            notifyObservers();
        }
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        if (this.altitude != altitude) {
            this.altitude = altitude;
            setChanged();
            notifyObservers();
        }
    }

    public double getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(double azimuth) {
        if (this.azimuth != azimuth) {
            this.azimuth = azimuth;
            setChanged();
            notifyObservers();
        }
    }

    public double getIntensity() {
        return intensity;
    }

    public void setIntensity(double intensity) {
        if (this.intensity != intensity) {
            this.intensity = intensity;
            setChanged();
            notifyObservers();
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        if (!this.color.equals(color)) {
            this.color = color;
            setChanged();
            notifyObservers();
        }
    }

    public Sun getSun() {
        return new Sun(altitude, azimuth, intensity, color);
    }
}
