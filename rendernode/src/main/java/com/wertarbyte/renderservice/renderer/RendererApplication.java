/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer;

import com.wertarbyte.renderservice.api.RenderTask;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfigurationBuilder;
import com.wertarbyte.renderservice.renderer.rendering.RenderServerApiClient;

import java.io.File;
import java.util.UUID;

/**
 * A renderer application.
 */
public interface RendererApplication {
    /**
     * Returns a scene directory for the given job. This directory may be temporary and must be unique. Since the same
     * task may be rendered multiple times at the same time, it may not only depend on the task id.
     *
     * @param task task to get a directory for
     * @return directory for the task
     */
    File getSceneDirectory(RenderTask task);

    String getSceneName(RenderTask job);

    ChunkyConfigurationBuilder createDefaultChunkyConfiguration();

    RenderServerApiClient getApi();

    long getMaxTargetSamples();

    UUID getId();

    UUID getAccount();

    void setAccount(UUID account);
}
