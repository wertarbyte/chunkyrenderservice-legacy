/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.application.gui;

import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClient;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClientListenerAdapter;
import com.wertarbyte.renderservice.renderer.application.GuiRenderer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.io.IOException;

public class QuickConfigurationPanel extends JPanel {
    private static final Logger LOGGER = LogManager.getLogger(QuickConfigurationPanel.class);

    public QuickConfigurationPanel(final GuiRenderer guiRenderer, final MultiRemoteRendererClient renderer) {
        super(new FlowLayout(FlowLayout.LEFT));

        add(new JLabel("Process count:"));
        final JSpinner processCountSpinner = new JSpinner(new SpinnerNumberModel(
                new Integer(Math.max(1, renderer.getMaxParallelInstances())), // value
                new Integer(1), // min
                new Integer(8), // max
                new Integer(1) // step
        ));
        processCountSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                Integer count = (Integer) processCountSpinner.getValue();
                if (count != null && count != renderer.getMaxParallelInstances()) {
                    try {
                        renderer.setMaxParallelInstances(count);
                        LOGGER.debug("Set max parallel process count to " + processCountSpinner.getValue());
                    } catch (IOException e) {
                        LOGGER.debug("Could not set max parallel process count to " + processCountSpinner.getValue(), e);
                        processCountSpinner.setValue(renderer.getMaxParallelInstances());
                    }
                }
            }
        });
        add(processCountSpinner);
        renderer.addListener(new MultiRemoteRendererClientListenerAdapter() {
            @Override
            public void onMaxParallelInstancesChanged(MultiRemoteRendererClient multiClient, int newValue) {
                processCountSpinner.setValue(newValue);
            }
        });

        /*
        add(new JLabel("Max SPP per task:"));
        final JSpinner sppSpinner = new JSpinner(new SpinnerNumberModel(
                new Integer(Math.min(Math.max(50, guiRenderer.getMaxTargetSpp()), 1000)), // value
                new Integer(50), // min
                new Integer(1000), // max
                new Integer(1) // step
        ));
        sppSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                Integer count = (Integer) sppSpinner.getValue();
                if (count != null) {
                    guiRenderer.setNormalizedMaxTargetSpp(count);
                    LOGGER.debug("Set max target SPP to " + sppSpinner.getValue());
                }
            }
        });
        add(sppSpinner);
        */
    }
}
