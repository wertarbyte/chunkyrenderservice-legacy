/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.application;

import com.wertarbyte.consoleapplication.ConsoleApplication;
import com.wertarbyte.consoleapplication.commands.Command;
import com.wertarbyte.consoleapplication.commands.annotation.CommandHandler;
import com.wertarbyte.renderservice.api.RenderTask;
import com.wertarbyte.renderservice.libchunky.ChunkyProcessWrapper;
import com.wertarbyte.renderservice.libchunky.ChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfiguration;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfigurationBuilder;
import com.wertarbyte.renderservice.libchunky.remote.ChunkyWrapperFactory;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClient;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClientListenerAdapter;
import com.wertarbyte.renderservice.libchunky.remote.renderer.RemoteRendererClient;
import com.wertarbyte.renderservice.renderer.Renderer;
import com.wertarbyte.renderservice.renderer.RendererApplication;
import com.wertarbyte.renderservice.renderer.rendering.RenderServerApiClient;
import com.wertarbyte.renderservice.util.TempFileUtil;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import com.wertarbyte.util.config.Settings;
import com.wertarbyte.util.config.YamlSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

public class HeadlessRenderer extends ConsoleApplication implements RendererApplication {
    private static final Logger LOGGER = LogManager.getLogger(HeadlessRenderer.class);
    private static final String SETTINGS_FILENAME = "serverSettings.yml";
    private final RenderServerApiClient api = new RenderServerApiClient();
    private File jobDirectory;
    private File texturepackPath;
    private final UUID id = UUID.randomUUID();
    private UUID account;

    private MultiRemoteRendererClient renderer;

    public HeadlessRenderer() {
        super(new YamlSettings());
        Renderer.setInstance(this);
    }

    @Override
    public void start() {
        LOGGER.info("ChunkyRenderService - Copyright (C) 2014-2015 Wertarbyte GbR");
        LOGGER.info("Starting...");

        try {
            if (getApi().getVersions().getRenderer() != Renderer.VERSION) {
                LOGGER.warn("This version is outdated. You need to update to the latest version.");
                System.exit(0);
            }
        } catch (ApiClientException e) {
            LOGGER.error("Could not check version", e);
            System.exit(1);
        }

        loadSettings();

        try {
            texturepackPath = TempFileUtil.extractTemporary(BundledResourceManager.getDefaultTextures());
            LOGGER.info("Temporary textures path: " + texturepackPath.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.error("Could not extract textures", e);
            System.exit(1);
        }

        if (getSettings().contains("paths.jobs")) {
            jobDirectory = new File(getSettings().getString("paths.jobs"));
        } else {
            try {
                jobDirectory = TempFileUtil.createTemporaryDirectory();
                LOGGER.info("Temporary job path: " + jobDirectory.getAbsolutePath());
            } catch (IOException e) {
                LOGGER.error("Could not create temporary job directory", e);
                System.exit(1);
            }
        }

        if (getSettings().contains("account")) {
            try {
                account = UUID.fromString(getSettings().getString("account"));
            } catch (IllegalArgumentException e) {
                LOGGER.warn("You account UUID is invalid, you won't get any points for rendering", e);
            }
        } else {
            LOGGER.warn("You did not configure an account, you won't get any points for rendering");
        }

        super.start();

        try {
            renderer = new MultiRemoteRendererClient(new ChunkyWrapperFactory() {
                @Override
                public ChunkyWrapper getChunkyInstance() {
                    ChunkyConfiguration config = Renderer.createDefaultChunkyConfiguration().build();

                    ChunkyProcessWrapper chunky = new ChunkyProcessWrapper();
                    chunky.setThreadCount(config.getThreadCount());
                    chunky.setJvmMinMemory(config.getJvmMinMemory());
                    chunky.setJvmMaxMemory(config.getJvmMaxMemory());
                    chunky.setTexturepack(texturepackPath); //TODO remove this once we get the texturepack from master
                    return chunky;
                }
            }, getSettings().getString("pool.host", Renderer.RENDERER_POOL_HOST),
                    getSettings().getInteger("pool.port", Renderer.RENDERER_POOL_PORT));
        } catch (IOException e) {
            LOGGER.error("Could not connect remote renderer client", e);
            System.exit(1);
        }

        renderer.addListener(new MultiRemoteRendererClientListenerAdapter() {
            @Override
            public void onConnected(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
                try {
                    client.putData("clientId", id.toString());
                } catch (IOException e) {
                    LOGGER.warn("Could not send client ID to master server", e);
                }


                if (account != null) {
                    try {
                        client.putData("account", account.toString());
                    } catch (IOException e) {
                        LOGGER.warn("Could not send account ID to master server", e);
                    }
                }
            }
        });

        try {
            renderer.start(getSettings().getInteger("rendering.maxParallelInstances",
                    (int) Math.ceil(Runtime.getRuntime().availableProcessors() / 2.0)));
        } catch (IOException e) {
            LOGGER.error("Could not start remote renderer client", e);
            System.exit(1);
        }

        LOGGER.info("Started");
    }

    @CommandHandler(value = "stop", description = "Stop the server")
    @Override
    public void stop() {
        LOGGER.warn("Will now stop...");
        try {
            getSettings().save(new File(SETTINGS_FILENAME));
        } catch (IOException e) {
            LOGGER.warn("Error while saving the settings", e);
        }
        super.stop();
        try {
            renderer.stop();
        } catch (IOException e) {
            LOGGER.warn("Could not gracefully stop renderer", e);
        }

        LOGGER.info("Server should stop now...");
    }

    private void loadSettings() {
        LOGGER.info("Loading settings...");
        try {
            getSettings().load(new File(SETTINGS_FILENAME));
            LOGGER.info("Settings loaded.");
        } catch (FileNotFoundException e) {
            LOGGER.warn("Settings file not found. Default settings will be saved, edit them and restart!", e);
            try {
                getSettings().load(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("config.yml")));
                try {
                    getSettings().save(new File(SETTINGS_FILENAME));
                } catch (IOException e1) {
                    LOGGER.error("Could not save default settings", e);
                }
                System.exit(0);
            } catch (Settings.SettingsCorruptedException e1) {
                LOGGER.error("Could not load default settings", e);
            }
        } catch (Settings.SettingsCorruptedException e) {
            LOGGER.error("The settings file is invalid. Delete it to restore default settings.", e);
            System.exit(1);
        }
    }

    @CommandHandler(value = "list", description = "Show running tasks")
    public void showRunningTasks() {
        LOGGER.info(renderer.getRenderingInstancesCount() + " tasks running");
        for (RemoteRendererClient p : renderer.getRenderingInstances()) {
            LOGGER.info(String.format("Task %s: %s", p.getAdditionalData().get("taskId"), p.getChunky().getStatus()));
        }
    }

    @CommandHandler(value = "sps", description = "Show current samples per second")
    public void showSpp() {
        double sps = 0;
        for (RemoteRendererClient p : renderer.getRenderingInstances()) {
            sps += p.getChunky().getSamplesPerSecond();
        }
        LOGGER.info(String.format("Rendering with a total of %.2f samples per second", sps));
    }

    @CommandHandler(value = "whoami", description = "Show the unique ID of this renderer")
    public void showId() {
        LOGGER.info("Renderer ID: " + id);
    }

    @Override
    protected void onCommandUnknown(Command command) {
        LOGGER.info("Unknown command. Use \"help\" to get a list of all commands.");
    }

    @Override
    @CommandHandler(value = "help", description = "Show all available commands")
    public void printHelp() {
        LOGGER.info("Available commands:");
        for (CommandHandler handler : getCommandHandlers()) {
            LOGGER.info(handler.value() + "  -  " + handler.description());
        }
    }

    @Override
    public ChunkyConfigurationBuilder createDefaultChunkyConfiguration() {
        return ChunkyConfiguration.builder()
                .setJvmMinMemory(getSettings().getInteger("jvm.xms", 1024))
                .setJvmMaxMemory(getSettings().getInteger("jvm.xmx", 2048))
                .setDefaultTexturePack(texturepackPath)
                .setTargetSpp(getSettings().getInteger("rendering.sppPerRun", 100))
                .setThreadCount(getSettings().getInteger("rendering.threads", 2)) //2 is best, see http://llbit.se/?p=1827
                .setDumpFrequency(getSettings().getInteger("rendering.sppPerRun", 100));
    }

    @Override
    public File getSceneDirectory(RenderTask task) {
        return new File(jobDirectory, "task_" + task.getId() + "_" + UUID.randomUUID().toString().replaceAll("\\-", ""));
    }

    @Override
    public String getSceneName(RenderTask task) {
        return "task_" + task.getId();
    }

    @Override
    public RenderServerApiClient getApi() {
        return api;
    }

    @Override
    public long getMaxTargetSamples() {
        return getSettings().getInteger("rendering.sppPerRun", 100) * 360 * 640; //normalized for a 640x360 picture
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public UUID getAccount() {
        return account;
    }

    @Override
    public void setAccount(UUID account) {
        this.account = account;
    }
}
