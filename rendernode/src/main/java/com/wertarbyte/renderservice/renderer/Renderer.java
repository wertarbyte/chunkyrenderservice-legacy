/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer;

import com.wertarbyte.renderservice.api.CommonApiClient;
import com.wertarbyte.renderservice.api.RenderTask;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfigurationBuilder;
import com.wertarbyte.renderservice.renderer.rendering.RenderServerApiClient;

import java.io.File;
import java.util.UUID;

/**
 * Represents the currently running {@link RendererApplication}.
 */
public final class Renderer {
    /**
     * Renderer version. Incremented on breaking changes.
     */
    public static final int VERSION = 5;
    public static final String RENDERER_POOL_HOST = "s6.wertarbyte.com";
    public static final int RENDERER_POOL_PORT = 19876;
    private static RendererApplication instance;

    private Renderer() {
    }

    public static void setInstance(RendererApplication instance) {
        if (Renderer.instance == null) {
            Renderer.instance = instance;
        } else {
            throw new IllegalStateException("The renderer instance must not be redefined");
        }
    }

    public static File getSceneDirectory(RenderTask job) {
        return instance.getSceneDirectory(job);
    }

    public static String getSceneName(RenderTask job) {
        return instance.getSceneName(job);
    }

    public static ChunkyConfigurationBuilder createDefaultChunkyConfiguration() {
        return instance.createDefaultChunkyConfiguration();
    }

    public static RenderServerApiClient getApi() {
        return instance.getApi();
    }

    public static <T extends CommonApiClient> T getApi(Class<T> clazz) {
        CommonApiClient api = getApi();
        if (clazz.isAssignableFrom(api.getClass())) {
            return clazz.cast(api);
        }
        throw new IllegalStateException("API client is not of type " + clazz.getName());
    }

    public static long getMaxTargetSamples() {
        return instance.getMaxTargetSamples();
    }

    public static UUID getId() {
        return instance.getId();
    }

    public static UUID getAccount() {
        return instance.getAccount();
    }

    public static void setAccount(UUID account) {
        instance.setAccount(account);
    }
}
