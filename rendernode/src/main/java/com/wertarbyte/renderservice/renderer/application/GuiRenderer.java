/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.application;

import com.wertarbyte.renderservice.api.RenderTask;
import com.wertarbyte.renderservice.libchunky.ChunkyProcessWrapper;
import com.wertarbyte.renderservice.libchunky.ChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfiguration;
import com.wertarbyte.renderservice.libchunky.config.ChunkyConfigurationBuilder;
import com.wertarbyte.renderservice.libchunky.remote.ChunkyWrapperFactory;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClient;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClientListenerAdapter;
import com.wertarbyte.renderservice.libchunky.remote.renderer.RemoteRendererClient;
import com.wertarbyte.renderservice.renderer.Renderer;
import com.wertarbyte.renderservice.renderer.RendererApplication;
import com.wertarbyte.renderservice.renderer.application.gui.MainView;
import com.wertarbyte.renderservice.renderer.rendering.RenderServerApiClient;
import com.wertarbyte.renderservice.util.TempFileUtil;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * A renderer with a GUI.
 */
public class GuiRenderer implements RendererApplication {
    private static final Logger LOGGER = LogManager.getLogger(GuiRenderer.class);
    private RenderServerApiClient api = new RenderServerApiClient();
    private MultiRemoteRendererClient renderer;
    private File tempJobDirectory;
    private File texturepackPath;
    private final UUID id = UUID.randomUUID();
    private UUID account;

    private long maxTargetSamples = 200 * 640 * 360;

    public GuiRenderer() {
        Renderer.setInstance(this);

        try {
            if (getApi().getVersions().getRenderer() != Renderer.VERSION) {
                JOptionPane.showMessageDialog(null, "A new RenderService version is available. You need to update to the latest version in order to use this program.", "Update available", JOptionPane.WARNING_MESSAGE);
                System.exit(0);
            }
        } catch (ApiClientException e) {
            LOGGER.error("Could not check version", e);
            System.exit(1);
        }

        try {
            texturepackPath = TempFileUtil.extractTemporary(BundledResourceManager.getDefaultTextures());
            LOGGER.info("Temporary textures path: " + texturepackPath.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.error("Could not extract textures", e);
            System.exit(1);
        }

        try {
            tempJobDirectory = TempFileUtil.createTemporaryDirectory();
            LOGGER.info("Temporary job path: " + tempJobDirectory.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.error("Could not create job directory", e);
            System.exit(1);
        }

        try {
            renderer = new MultiRemoteRendererClient(new ChunkyWrapperFactory() {
                @Override
                public ChunkyWrapper getChunkyInstance() {
                    ChunkyConfiguration config = Renderer.createDefaultChunkyConfiguration().build();

                    ChunkyProcessWrapper chunky = new ChunkyProcessWrapper();
                    chunky.setThreadCount(config.getThreadCount());
                    chunky.setJvmMinMemory(config.getJvmMinMemory());
                    chunky.setJvmMaxMemory(config.getJvmMaxMemory());
                    chunky.setTexturepack(texturepackPath); //TODO remove this once we get the texturepack from master
                    return chunky;
                }
            }, Renderer.RENDERER_POOL_HOST, Renderer.RENDERER_POOL_PORT);
        } catch (IOException e) {
            LOGGER.error("Could not connect remote renderer client", e);
            System.exit(1);
        }

        renderer.addListener(new MultiRemoteRendererClientListenerAdapter() {
            @Override
            public void onConnected(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
                try {
                    client.putData("clientId", id.toString());
                } catch (IOException e) {
                    LOGGER.warn("Could not send clientId to master server", e);
                }

                if (account != null) {
                    try {
                        client.putData("account", account.toString());
                    } catch (IOException e) {
                        LOGGER.warn("Could not send account ID to master server", e);
                    }
                }
            }
        });

        MainView mainView = new MainView(this, renderer);
        renderer.addListener(mainView);
        try {
            renderer.start((int) Math.ceil(Runtime.getRuntime().availableProcessors() / 2.0));
        } catch (IOException e) {
            LOGGER.error("Could not start remote renderer client", e);
            System.exit(1);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    renderer.stop();
                } catch (IOException e) {
                    LOGGER.warn("Could not gracefully stop renderer", e);
                }
            }
        }));

    }

    @Override
    public File getSceneDirectory(RenderTask task) {
        return new File(tempJobDirectory, "task_" + task.getId() + "_" + UUID.randomUUID().toString().replaceAll("\\-", ""));
    }

    @Override
    public String getSceneName(RenderTask task) {
        return "task_" + task.getId();
    }

    @Override
    public ChunkyConfigurationBuilder createDefaultChunkyConfiguration() {
        return ChunkyConfiguration.builder()
                .setJvmMinMemory(1024) //TODO make this configurable
                .setJvmMaxMemory(2048)
                .setDefaultTexturePack(texturepackPath)
                .setThreadCount(2); //2 is best, see http://llbit.se/?p=1827
    }

    @Override
    public RenderServerApiClient getApi() {
        return api;
    }

    @Override
    public long getMaxTargetSamples() {
        return maxTargetSamples;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public UUID getAccount() {
        return account;
    }

    @Override
    public void setAccount(UUID account) {
        this.account = account;
    }

    /**
     * Sets the maximum target samples. The given samples per pixel will be normalized for a 640x360 picture.
     *
     * @param normalizedMaxTargetSpp maximum target spp for a 640x360 picture
     */
    public void setNormalizedMaxTargetSpp(int normalizedMaxTargetSpp) {
        maxTargetSamples = normalizedMaxTargetSpp * 640 * 360;
    }

    public int getMaxTargetSpp() {
        return (int) (maxTargetSamples / (640 * 360));
    }
}
