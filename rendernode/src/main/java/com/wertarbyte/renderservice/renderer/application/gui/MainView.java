/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.application.gui;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClient;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClientListener;
import com.wertarbyte.renderservice.libchunky.remote.renderer.RemoteRendererClient;
import com.wertarbyte.renderservice.renderer.application.GuiRenderer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * The main window view.
 */
public class MainView extends JFrame implements MultiRemoteRendererClientListener {
    private static final Logger LOGGER = LogManager.getLogger(MainView.class);
    private final MultiRemoteRendererClient renderer;
    private JPanel jobList;
    private JLabel waitingLabel;
    private GridBagConstraints listGridBagConstraints;

    public MainView(GuiRenderer guiRenderer, final MultiRemoteRendererClient renderer) {
        this.renderer = renderer;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e) {
            LOGGER.warn("Could not set system look and feel", e);
        }

        setTitle("Wertarbyte RenderService");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(500, 300));
        setResizable(false);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);

        JMenu infoMenu = new JMenu("?");
        JMenuItem aboutItem = new JMenuItem("About");
        aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFrame frame = new JFrame("About");
                frame.setContentPane(new AboutDialog().$$$getRootComponent$$$());
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });
        infoMenu.add(aboutItem);
        menu.add(infoMenu);

        try {
            getContentPane().add(new ImagePanel(getClass().getClassLoader().getResourceAsStream("top.png")), BorderLayout.PAGE_START);
        } catch (IOException e) {
            LOGGER.warn("Could not load logo", e);
        }

        waitingLabel = new JLabel("Waiting for tasks...");
        waitingLabel.setBorder(new EmptyBorder(2, 2, 2, 2));
        add(waitingLabel, BorderLayout.PAGE_END);

        jobList = new JPanel(new GridBagLayout());
        jobList.setBorder(new EmptyBorder(10, 10, 0, 10));
        listGridBagConstraints = new GridBagConstraints();
        add(new JScrollPane(jobList), BorderLayout.CENTER);

        //add filler so that everything aligns to top
        listGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        listGridBagConstraints.weightx = 1;
        listGridBagConstraints.weighty = 1;
        jobList.add(new JPanel(), listGridBagConstraints);

        add(new QuickConfigurationPanel(guiRenderer, renderer), BorderLayout.PAGE_END);

        setupTrayIcon();

        try {
            setIconImage(new ImageIcon(ImageIO.read(getClass().getClassLoader().getResourceAsStream("icon.png"))).getImage());
        } catch (IOException e) {
            LOGGER.warn("Could not load icon");
        }

        pack();
        setVisible(true);
    }

    private void setupTrayIcon() {
        if (SystemTray.isSupported()) {
            final RendererTrayIcon trayIcon;
            try {
                trayIcon = new RendererTrayIcon(this, ImageIO.read(getClass().getClassLoader().getResourceAsStream("icon.png")));
                renderer.addListener(trayIcon);
                try {
                    SystemTray.getSystemTray().add(trayIcon);
                    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SystemTray.getSystemTray().remove(trayIcon);
                        }
                    }));
                } catch (AWTException e) {
                    LOGGER.error("Could not add tray icon");
                }
            } catch (IOException e) {
                LOGGER.error("Could not load tray icon");
            }

            addWindowStateListener(new WindowStateListener() {
                public void windowStateChanged(WindowEvent e) {
                    if (e.getNewState() == ICONIFIED) {
                        setVisible(false); //minimize to tray
                    }
                }
            });
        } else {
            LOGGER.info("Tray icon not supported");
        }
    }

    @Override
    public void onConnected(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
        //TODO maybe show number of connected renderers?
    }

    @Override
    public void onRenderingStarting(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
        waitingLabel.setVisible(false);

        TaskPanel panel = new TaskPanel(
                Integer.parseInt(client.getAdditionalData().get("jobId")),
                Integer.parseInt(client.getAdditionalData().get("taskId")));
        client.addListener(panel);

        listGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        listGridBagConstraints.weightx = 1;
        listGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jobList.add(panel, listGridBagConstraints, 0);

        jobList.validate();
        jobList.repaint();
    }

    @Override
    public void onRenderingStopped(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
        if (multiClient.getRenderingInstancesCount() == 0) {
            waitingLabel.setVisible(true);
        }
    }

    @Override
    public void onFinished(MultiRemoteRendererClient multiClient, RemoteRendererClient client, BufferedImage image) {
        if (multiClient.getRenderingInstancesCount() == 0) {
            waitingLabel.setVisible(true);
        }
    }

    @Override
    public void onStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, ChunkyTask task) {

    }

    @Override
    public void onRenderStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, int currentSpp, int targetSpp) {

    }

    @Override
    public void onMaxParallelInstancesChanged(MultiRemoteRendererClient multiClient, int newValue) {

    }
}
