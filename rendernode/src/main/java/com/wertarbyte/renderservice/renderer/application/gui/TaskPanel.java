/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.application.gui;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.remote.renderer.RemoteRendererClient;
import com.wertarbyte.renderservice.libchunky.remote.renderer.RemoteRendererClientListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * A panel for a job.
 */
public class TaskPanel extends JPanel implements RemoteRendererClientListener {
    private static final Logger LOGGER = LogManager.getLogger(TaskPanel.class);
    private final int taskId;
    private final int jobId;

    private JLabel jobNameLabel;
    private JProgressBar progressBar;

    public TaskPanel(final int jobId, int taskId) {
        this.taskId = taskId;
        this.jobId = jobId;

        setLayout(new BorderLayout(10, 0));
        setPreferredSize(new Dimension(-1, 40));
        setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
        setBorder(new EmptyBorder(0, 0, 10, 0));

        jobNameLabel = new JLabel();
        jobNameLabel.setText(getPrefix() + "Loading...");
        add(jobNameLabel, BorderLayout.PAGE_START);

        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        progressBar.setMaximum(100);
        progressBar.setIndeterminate(true);
        add(progressBar, BorderLayout.CENTER);

        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                try {
                    Desktop.getDesktop().browse(new URI("http://rs.wertarbyte.com/gallery/" + jobId));
                } catch (IOException | URISyntaxException e) {
                    LOGGER.error("Invalid job URI", e);
                }
            }
        });
    }

    private String getPrefix() {
        return String.format("Task %d (Job %d) - ", taskId, jobId);
    }

    private void removeThis() {
        final Container parent = getParent();
        parent.remove(TaskPanel.this);
        parent.revalidate();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                parent.repaint();
            }
        });
    }

    @Override
    public void onConnected(RemoteRendererClient client) {
        //Nothing to do here
    }

    @Override
    public void onRenderingStarting(RemoteRendererClient client) {
        progressBar.setIndeterminate(true);
        jobNameLabel.setText(getPrefix() + "Crunching data...");
    }

    @Override
    public void onRenderingStopped(RemoteRendererClient client) {
        client.removeListener(this);
        removeThis();
    }

    @Override
    public void onFinished(RemoteRendererClient client, BufferedImage image) {
        client.removeListener(this);
        removeThis();
    }

    @Override
    public void onStatusChanged(RemoteRendererClient client, ChunkyTask task) {
        progressBar.setIndeterminate(true);
        jobNameLabel.setText(getPrefix() + "Crunching data...");
    }

    @Override
    public void onRenderStatusChanged(RemoteRendererClient client, int currentSpp, int targetSpp) {
        progressBar.setIndeterminate(false);
        double percentage = currentSpp * 100.0 / targetSpp;
        progressBar.setValue((int) percentage);
        jobNameLabel.setText(String.format(getPrefix() + "Rendering (%.2f%%, %.0f SPS)...", percentage, client.getChunky().getSamplesPerSecond()));
    }
}
