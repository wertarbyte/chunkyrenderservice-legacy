/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.renderer.application.gui;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClient;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClientListener;
import com.wertarbyte.renderservice.libchunky.remote.renderer.RemoteRendererClient;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * The tray icon.
 */
public class RendererTrayIcon extends TrayIcon implements MultiRemoteRendererClientListener {
    public RendererTrayIcon(final MainView mainView, Image icon) throws IOException {
        super(icon.getScaledInstance(new TrayIcon(icon).getSize().width, -1, Image.SCALE_SMOOTH),
                "RenderService - 0 tasks running");

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mainView.setVisible(true);
                mainView.setState(Frame.NORMAL);
            }
        });
    }

    private void updateToolTip(MultiRemoteRendererClient renderer) {
        this.setToolTip(String.format("RenderService - %d tasks running", renderer.getRenderingInstancesCount()));
    }

    @Override
    public void onConnected(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
        //TODO maybe change icon if we are connected?
    }

    @Override
    public void onRenderingStarting(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
        updateToolTip(multiClient);
    }

    @Override
    public void onRenderingStopped(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
        updateToolTip(multiClient);
    }

    @Override
    public void onFinished(MultiRemoteRendererClient multiClient, RemoteRendererClient client, BufferedImage image) {
        updateToolTip(multiClient);
    }

    @Override
    public void onStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, ChunkyTask task) {
        updateToolTip(multiClient);
    }

    @Override
    public void onRenderStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, int currentSpp, int targetSpp) {
        updateToolTip(multiClient);
    }

    @Override
    public void onMaxParallelInstancesChanged(MultiRemoteRendererClient multiClient, int newValue) {

    }
}
