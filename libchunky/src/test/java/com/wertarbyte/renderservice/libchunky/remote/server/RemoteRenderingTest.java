package com.wertarbyte.renderservice.libchunky.remote.server;

import com.wertarbyte.renderservice.libchunky.ChunkyProcessWrapper;
import com.wertarbyte.renderservice.libchunky.ChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.remote.ChunkyWrapperFactory;
import com.wertarbyte.renderservice.libchunky.remote.renderer.MultiRemoteRendererClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RemoteRenderingTest {
    private RemoteRendererPool pool;

    @Before
    public void setUp() throws Exception {
        pool = new RemoteRendererPool(1337); //TODO use a junit rule to determine a port
        pool.start();
    }

    @After
    public void tearDown() throws Exception {
        pool.stop();
    }

    @Test
    public void testClientConnection() throws Exception {
        MultiRemoteRendererClient client = new MultiRemoteRendererClient(new ChunkyWrapperFactory() {
            @Override
            public ChunkyWrapper getChunkyInstance() {
                return new ChunkyProcessWrapper();
            }
        }, "localhost", 1337); //TODO put host and port in variable

        client.start(1);
        Thread.sleep(1000);
        assertEquals(1, pool.getAvailableRenderersCount());
        client.setMaxParallelInstances(42);
        Thread.sleep(1000);
        assertEquals(0, client.getRenderingInstancesCount());
        assertEquals(42, pool.getAvailableRenderersCount());
        client.setMaxParallelInstances(0);
        Thread.sleep(1000);
        assertEquals(0, pool.getAvailableRenderersCount());
    }
}