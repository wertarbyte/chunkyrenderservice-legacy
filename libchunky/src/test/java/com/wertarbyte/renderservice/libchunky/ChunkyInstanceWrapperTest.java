/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.util.FileUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class ChunkyInstanceWrapperTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private ChunkyInstanceWrapper chunky;
    private File sceneDirectory;
    private SceneDescription scene;

    @Before
    public void setup() throws Exception {
        sceneDirectory = temporaryFolder.newFolder();

        try (InputStreamReader stream = new InputStreamReader(getClass().getClassLoader()
                .getResourceAsStream("testscene/test_scene.json"))) {
            scene = new SceneDescription((new Gson()).fromJson(stream, JsonObject.class));
        }

        //extract scene files
        FileUtil.extractResource("testscene/test_scene.foliage", new File(sceneDirectory, "test_scene.foliage"));
        FileUtil.extractResource("testscene/test_scene.grass", new File(sceneDirectory, "test_scene.grass"));
        FileUtil.extractResource("testscene/test_scene.octree", new File(sceneDirectory, "test_scene.octree"));
        FileUtil.extractResource("testscene/test_scene.json", new File(sceneDirectory, "test_scene.json"));

        chunky = new ChunkyInstanceWrapper();
        chunky.setTargetSpp(1);
        chunky.setThreadCount(2);
        chunky.setSceneDirectory(sceneDirectory);
        chunky.setScene(scene);
    }

    @After
    public void tearDown() {
        chunky.stop();
    }

    @Test
    public void testImageCreation() throws Exception {
        chunky.render();

        assertThat(chunky.getImage(), is(notNullValue()));
        assertThat(chunky.getImage().getWidth(), is(scene.getWidth()));
        assertThat(chunky.getImage().getHeight(), is(scene.getHeight()));
    }

    @Test
    public void testDumpCreation() throws Exception {
        chunky.render();

        assertThat(chunky.getDump(), is(notNullValue()));
        assertThat(chunky.getDump().getWidth(), is(scene.getWidth()));
        assertThat(chunky.getDump().getHeight(), is(scene.getHeight()));
        assertThat(chunky.getDump().getSpp(), is(1));
    }

    @Test
    public void testListeners() throws Exception {
        final AtomicBoolean finished = new AtomicBoolean(false);
        final AtomicBoolean finishedExactlyOnce = new AtomicBoolean(false);
        final AtomicBoolean statusChangedWhenFinished = new AtomicBoolean(false);

        chunky.addListener(new RenderListener() {
            @Override
            public void onFinished() {
                if (finished.get()) {
                    finishedExactlyOnce.set(false);
                } else {
                    finishedExactlyOnce.set(true);
                }
                finished.set(true);
            }

            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                if (currentSpp == targetSpp) {
                    statusChangedWhenFinished.set(true);
                }
            }

            @Override
            public void onStatusChanged(ChunkyTask task) {

            }
        });
        chunky.render();

        assertThat(finished.get(), is(true));
        assertThat(finishedExactlyOnce.get(), is(true));
        assertThat(statusChangedWhenFinished.get(), is(true));
    }

    @Test
    public void testChunkyCrash() throws Exception {
        new File(sceneDirectory, "test_scene.json").delete(); //delete octree to make chunky crash while loading

        boolean exceptionThrown = false;
        try {
            chunky.render();
        } catch (IOException e) {
            exceptionThrown = true;
        }

        assertThat("IOException should be thrown if chunky crashes", exceptionThrown, is(true));
    }

    @Test
    public void testContinuousRendering() throws Exception {
        final AtomicInteger finishedCount = new AtomicInteger(0);

        chunky.addListener(new RenderListener() {
            @Override
            public void onFinished() {
                int finished = finishedCount.incrementAndGet();
                if (finished == 1) {
                    chunky.setTargetSpp(2); //continue rendering after finishing the first time
                }
            }

            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {

            }

            @Override
            public void onStatusChanged(ChunkyTask task) {

            }
        });

        chunky.start();

        while (finishedCount.get() < 2 && chunky.isStarted()) {
            Thread.yield();
        }

        assertThat(finishedCount.get(), is(2));
    }

    @Test
    public void testContinuousRenderingDumpCreation() throws Exception {
        final AtomicBoolean finished = new AtomicBoolean(false);

        chunky.addListener(new RenderListener() {
            @Override
            public void onFinished() {
                finished.set(true);
            }

            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {

            }

            @Override
            public void onStatusChanged(ChunkyTask task) {

            }
        });
        chunky.start();

        while (!finished.get()) {
            Thread.yield();
        }

        assertThat(chunky.getDump().getSpp(), is(1));
        assertThat(chunky.getDump().getWidth(), is(scene.getWidth()));
        assertThat(chunky.getDump().getHeight(), is(scene.getHeight()));
    }

    @Test
    public void testContinuousRenderingImageCreation() throws Exception {
        final AtomicBoolean finished = new AtomicBoolean(false);

        chunky.addListener(new RenderListener() {
            @Override
            public void onFinished() {
                finished.set(true);
            }

            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {

            }

            @Override
            public void onStatusChanged(ChunkyTask task) {

            }
        });
        chunky.start();

        while (!finished.get()) {
            Thread.yield();
        }

        assertThat(chunky.getImage().getWidth(), is(scene.getWidth()));
        assertThat(chunky.getImage().getHeight(), is(scene.getHeight()));
    }

    @Test
    public void testMissingTexturepack() throws Exception {
        chunky.setTexturepack(new File("this does not exist"));

        exception.expect(IOException.class);
        chunky.render();
    }
}