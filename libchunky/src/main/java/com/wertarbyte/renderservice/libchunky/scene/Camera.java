/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonObject;

public class Camera {
    private double dof = Double.POSITIVE_INFINITY;
    private double focalOffset = 18;
    private double fov = 70;
    private Orientation orientation = new Orientation();
    private Position position = new Position(0, 0, 0);
    private ProjectionMode projectionMode = ProjectionMode.PINHOLE;

    public double getDof() {
        return dof;
    }

    public void setDof(double dof) {
        this.dof = dof;
    }

    public double getFocalOffset() {
        return focalOffset;
    }

    public void setFocalOffset(double focalOffset) {
        this.focalOffset = focalOffset;
    }

    public double getFov() {
        return fov;
    }

    public void setFov(double fov) {
        this.fov = fov;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public ProjectionMode getProjectionMode() {
        return projectionMode;
    }

    public void setProjectionMode(ProjectionMode projectionMode) {
        this.projectionMode = projectionMode;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        if (getDof() == Double.POSITIVE_INFINITY) {
            json.addProperty("dof", "Infinity");
        } else {
            json.addProperty("dof", getDof());
        }
        json.addProperty("fov", getFov());
        json.addProperty("focalOffset", getFocalOffset());
        json.addProperty("projectionMode", getProjectionMode().name());
        json.add("position", getPosition().toJson());
        json.add("orientation", getOrientation().toJson());

        return json;
    }

    public static Camera fromJson(JsonObject json) {
        Camera camera = new Camera();
        if (json.get("dof").getAsString().equals("Infinity")) {
            camera.setDof(Double.POSITIVE_INFINITY);
        } else {
            camera.setDof(json.get("dof").getAsDouble());
        }
        camera.setFov(json.get("fov").getAsDouble());
        camera.setFocalOffset(json.get("focalOffset").getAsDouble());
        camera.setProjectionMode(ProjectionMode.valueOf(json.get("projectionMode").getAsString()));
        camera.setPosition(Position.fromJson(json.getAsJsonObject("position")));
        camera.setOrientation(Orientation.fromJson(json.getAsJsonObject("orientation")));
        return camera;
    }
}
