/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * A message to set the thread count.
 */
public class SetThreadCountMessage extends Message {
    private final int threads;

    public SetThreadCountMessage(int threads) {
        this.threads = threads;
    }

    SetThreadCountMessage(DataInputStream inputStream) throws IOException {
        this.threads = inputStream.readInt();
    }

    @Override
    protected byte getId() {
        return Message.SET_THREAD_COUNT;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt(threads);
    }

    public int getThreads() {
        return threads;
    }
}
