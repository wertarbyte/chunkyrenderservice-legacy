/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import se.llbit.chunky.main.Chunky;

import java.util.ArrayList;
import java.util.List;

/**
 * A wrapper for {@link Chunky} that fixes problems with arguments that contain spaces.
 */
public class ChunkyArgumentFixer {
    public static void main(String[] args) {
        List<String> arguments = new ArrayList<>();
        for (String arg : args) {
            if (arguments.isEmpty() || arguments.get(arguments.size() - 1).startsWith("-") || arg.startsWith("-")) {
                arguments.add(arg);
            } else {
                arguments.set(arguments.size() - 1, arguments.get(arguments.size() - 1) + " " + arg);
            }
        }
        Chunky.main(arguments.toArray(new String[arguments.size()]));
    }
}
