/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.renderer;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;

import java.awt.image.BufferedImage;

/**
 * A listener for {@link RemoteRendererClient}s.
 */
public interface MultiRemoteRendererClientListener {
    /**
     * Gets called when a renderer connected to a server.
     *
     * @param multiClient multi remote renderer client
     * @param client      remote renderer client that just connected to a server
     */
    void onConnected(MultiRemoteRendererClient multiClient, RemoteRendererClient client);

    /**
     * Gets called just before the client starts rendering.
     *
     * @param multiClient multi remote renderer client
     * @param client      remote renderer client
     */
    void onRenderingStarting(MultiRemoteRendererClient multiClient, RemoteRendererClient client);

    /**
     * Gets called after rendering has stopped (no matter if it failed or succeeded).
     *
     * @param multiClient multi remote renderer client
     * @param client      remote renderer client
     */
    void onRenderingStopped(MultiRemoteRendererClient multiClient, RemoteRendererClient client);

    /**
     * Gets called after rendering completed successfully.
     *
     * @param multiClient multi remote renderer client
     * @param client      remote renderer client
     * @param image       the rendered image
     */
    void onFinished(MultiRemoteRendererClient multiClient, RemoteRendererClient client, BufferedImage image);

    /**
     * Gets called whenever the status changes.
     *
     * @param multiClient multi remote renderer client
     * @param client      remote renderer client
     * @param task        currently performed task
     */
    void onStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, ChunkyTask task);

    /**
     * Gets called whenever the rendering status changes.
     *
     * @param multiClient multi remote renderer client
     * @param client      remote renderer client
     * @param currentSpp  current spp
     * @param targetSpp   target spp
     */
    void onRenderStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, int currentSpp, int targetSpp);

    /**
     * Gets called whenever the maximum number of parallel instances is changed.
     *
     * @param multiClient multi remote renderer client
     * @param newValue    new maximum number of parallel instances
     */
    void onMaxParallelInstancesChanged(MultiRemoteRendererClient multiClient, int newValue);
}
