/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.renderer;

import com.wertarbyte.renderservice.libchunky.ChunkyProcessWrapper;
import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.ChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.remote.ChunkyWrapperFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Client for {@link com.wertarbyte.renderservice.libchunky.remote.server.RemoteRenderer} that uses multiple chunky
 * instances.
 */
public class MultiRemoteRendererClient implements RemoteRendererClientListener {
    protected final ChunkyWrapperFactory chunkyFactory;
    protected final String host;
    protected final int port;

    private final List<RemoteRendererClient> threads = new ArrayList<>();
    private final List<MultiRemoteRendererClientListener> listeners = new CopyOnWriteArrayList<>();
    private int maxParallelInstances;
    private ExecutorService dumpUploader = Executors.newFixedThreadPool(1);

    public MultiRemoteRendererClient(ChunkyWrapperFactory chunkyFactory, String host, int port) throws IOException {
        this.chunkyFactory = chunkyFactory;
        this.host = host;
        this.port = port;
    }

    public void start(int maxParallelInstances) throws IOException {
        setMaxParallelInstances(maxParallelInstances);
    }

    public void stop() throws IOException {
        setMaxParallelInstances(0);
        dumpUploader.shutdown();
    }

    public int getMaxParallelInstances() {
        return threads.size();
    }

    public synchronized void setMaxParallelInstances(int maxParallelInstances) throws IOException {
        if (this.maxParallelInstances > maxParallelInstances) {
            for (int i = 0; i < this.maxParallelInstances - maxParallelInstances; i++) {
                threads.get(0).removeListener(this);
                threads.remove(0).stop();
            }
        } else {
            for (int i = 0; i < maxParallelInstances - this.maxParallelInstances; i++) {
                RemoteRendererClient client = new QueuedRemoteRendererClient(chunkyFactory, host, port);
                client.addListener(this);
                Thread thread = new Thread(client);
                threads.add(client);
                thread.setDaemon(true);
                thread.start();
            }
        }

        this.maxParallelInstances = maxParallelInstances;

        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onMaxParallelInstancesChanged(this, maxParallelInstances);
        }
    }

    public static void main(String[] args) {
        try {
            MultiRemoteRendererClient client = new MultiRemoteRendererClient(new ChunkyWrapperFactory() {
                @Override
                public ChunkyWrapper getChunkyInstance() {
                    return new ChunkyProcessWrapper();
                }
            }, "127.0.0.1", 19876);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (!Thread.interrupted()) {
        }
    }

    /**
     * Gets the number of currently rendering instances.
     *
     * @return number of currently rendering instances
     */
    public int getRenderingInstancesCount() {
        int count = 0;
        for (RemoteRendererClient client : threads) {
            if (client.isRendering()) {
                count++;
            }
        }
        return count;
    }

    public List<RemoteRendererClient> getRenderingInstances() {
        List<RemoteRendererClient> instances = new ArrayList<>();
        for (RemoteRendererClient client : threads) {
            if (client.isRendering()) {
                instances.add(client);
            }
        }
        return instances;
    }

    public void addListener(MultiRemoteRendererClientListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MultiRemoteRendererClientListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void onConnected(RemoteRendererClient client) {
        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onConnected(this, client);
        }
    }

    @Override
    public void onRenderingStarting(RemoteRendererClient client) {
        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onRenderingStarting(this, client);
        }
    }

    @Override
    public void onRenderingStopped(RemoteRendererClient client) {
        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onRenderingStopped(this, client);
        }
    }

    @Override
    public void onFinished(RemoteRendererClient client, BufferedImage image) {
        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onFinished(this, client, image);
        }
    }

    @Override
    public void onStatusChanged(RemoteRendererClient client, ChunkyTask task) {
        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onStatusChanged(this, client, task);
        }
    }

    @Override
    public void onRenderStatusChanged(RemoteRendererClient client, int currentSpp, int targetSpp) {
        for (MultiRemoteRendererClientListener listener : listeners) {
            listener.onRenderStatusChanged(this, client, currentSpp, targetSpp);
        }
    }

    private class QueuedRemoteRendererClient extends RemoteRendererClient {
        public QueuedRemoteRendererClient(ChunkyWrapperFactory chunkyFactory, String host, int port) {
            super(chunkyFactory, host, port);
        }

        @Override
        protected void sendDump() throws IOException {
            try {
                dumpUploader.submit(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        QueuedRemoteRendererClient.super.sendDump();
                        return null;
                    }
                }).get();
            } catch (InterruptedException | ExecutionException e) {
                throw new IOException("Could not send dump", e);
            }
        }
    }
}
