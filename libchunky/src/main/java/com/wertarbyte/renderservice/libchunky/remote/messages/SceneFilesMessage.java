/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Message to send the scene files.
 */
public class SceneFilesMessage extends Message {
    private final File foliage;
    private final File grass;
    private final File octree;

    @Override
    protected byte getId() {
        return Message.SEND_SCENE_FILES;
    }

    SceneFilesMessage(DataInputStream inputStream) throws IOException {
        foliage = File.createTempFile("sceneFile", "foliage");
        grass = File.createTempFile("sceneFile", "grass");
        octree = File.createTempFile("sceneFile", "octree");

        readFile(inputStream, foliage);
        readFile(inputStream, grass);
        readFile(inputStream, octree);
    }

    public SceneFilesMessage(File foliage, File grass, File octree) {
        this.foliage = foliage;
        this.grass = grass;
        this.octree = octree;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        sendFile(outputStream, foliage);
        sendFile(outputStream, grass);
        sendFile(outputStream, octree);
    }

    public File getFoliage() {
        return foliage;
    }

    public File getGrass() {
        return grass;
    }

    public File getOctree() {
        return octree;
    }
}
