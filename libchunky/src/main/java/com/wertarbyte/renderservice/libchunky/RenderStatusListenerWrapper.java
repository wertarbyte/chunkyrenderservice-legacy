/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import se.llbit.chunky.renderer.RenderState;
import se.llbit.chunky.renderer.RenderStatusListener;

/**
 * Wrapper for {@link RenderStatusListener}.
 */
class RenderStatusListenerWrapper implements RenderStatusListener {
    private final RenderListener listener;

    RenderStatusListenerWrapper(RenderListener listener) {
        this.listener = listener;
    }

    public void chunksLoaded() {

    }

    public void setRenderTime(long l) {

    }

    public void setSamplesPerSecond(int i) {

    }

    public void setSPP(int i) {

    }

    public void sceneSaved() {

    }

    public void sceneLoaded() {

    }

    public void renderStateChanged(RenderState renderState) {

    }

    public void renderJobFinished(long time, int sps) {
        listener.onFinished();
    }

    public void renderResetRequested() {

    }

    public void setProgress(String task, int done, int start, int target) {
        if (task.equalsIgnoreCase("rendering")) {
            listener.onRenderStatusChanged(done, target);
        } else {
            listener.onStatusChanged(ChunkyTask.fromString(task));
        }
    }

    public void setProgress(String task, int done, int start, int target, String eta) {
        if (task.equalsIgnoreCase("rendering")) {
            listener.onRenderStatusChanged(done, target);
        } else {
            listener.onStatusChanged(ChunkyTask.fromString(task));
        }
    }

    public void taskAborted(String task) {

    }

    public void taskFailed(String task) {

    }
}
