/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Message to send the render status.
 */
public class RenderStatusMessage extends Message {
    private ChunkyTask task;

    RenderStatusMessage(DataInputStream inputStream) throws IOException {
        task = ChunkyTask.valueOf(inputStream.readUTF());
    }

    public RenderStatusMessage(ChunkyTask task) {
        this.task = task;
    }

    @Override
    protected byte getId() {
        return Message.RENDER_STATUS;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        outputStream.writeUTF(task.name());
    }

    public ChunkyTask getTask() {
        return task;
    }
}
