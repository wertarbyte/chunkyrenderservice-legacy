/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Orientation {
    private double pitch;
    private double roll;
    private double yaw;

    public double getPitch() {
        return this.pitch;
    }

    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    public double getRoll() {
        return this.roll;
    }

    public void setRoll(double roll) {
        this.roll = roll;
    }

    public double getYaw() {
        return this.yaw;
    }

    public void setYaw(double yaw) {
        this.yaw = yaw;
    }

    public JsonElement toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("roll", getRoll());
        json.addProperty("pitch", getPitch());
        json.addProperty("yaw", getYaw());

        return json;
    }

    public static Orientation fromJson(JsonObject json) {
        Orientation orientation = new Orientation();
        orientation.setRoll(json.get("roll").getAsDouble());
        orientation.setPitch(json.get("pitch").getAsDouble());
        orientation.setYaw(json.get("yaw").getAsDouble());
        return orientation;
    }
}
