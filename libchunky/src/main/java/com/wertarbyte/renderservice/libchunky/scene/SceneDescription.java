/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SceneDescription {
    public static final int SDF_VERSION = 5;

    private String name = "default";
    private int width = 640;
    private int height = 480;
    private double exposure = 1.0;
    private PostprocessMethod postProcess = PostprocessMethod.GammaCorrection;
    private int rayDepth = 5;
    private boolean pathTrace = true;
    private int dumpFrequency = 500;
    private boolean saveSnapshots = true;
    private boolean emittersEnabled = true;
    private double emitterIntensity = 13;
    private boolean sunEnabled = true;
    private boolean stillWater = false;
    private double waterOpacity = 0.42;
    private double waterVisibility = 9.0;
    private Color waterColor = null;
    private Color fogColor = new Color(255, 255, 255);
    private boolean fastFog = true;
    private boolean biomeColorsEnabled = true;
    private boolean atmosphereEnabled = false;
    private boolean transparentSky = false;
    private double fogDensity = 0.0;
    private int waterHeight = 0;
    private Sky sky = new Sky();
    private Camera camera = new Camera();
    private Sun sun = new Sun();
    private World world = new World();
    private List<ChunkPosition> chunkList = Collections.emptyList();

    public SceneDescription() {
    }

    public SceneDescription(JsonObject json) {
        setName(json.get("name").getAsString());
        setWidth(json.get("width").getAsInt());
        setHeight(json.get("height").getAsInt());
        setExposure(json.get("exposure").getAsDouble());
        setPostProcess(getPostProcess(json.get("postprocess").getAsInt()));
        setRayDepth(json.get("rayDepth").getAsInt());
        setPathTrace(json.get("pathTrace").getAsBoolean());
        setDumpFrequency(json.get("dumpFrequency").getAsInt());
        saveSnapshots = json.get("saveSnapshots").getAsBoolean();
        setEmittersEnabled(json.get("emittersEnabled").getAsBoolean());
        setEmitterIntensity(json.get("emitterIntensity").getAsDouble());
        setSunEnabled(json.get("sunEnabled").getAsBoolean());
        setStillWater(json.get("stillWater").getAsBoolean());
        setWaterOpacity(json.get("waterOpacity").getAsDouble());
        setWaterVisibility(json.get("waterVisibility").getAsDouble());
        if (json.get("useCustomWaterColor").getAsBoolean()) {
            setWaterColor(Color.fromJson(json.getAsJsonObject("waterColor")));
        }
        if (json.has("fogColor")) {
            setFogColor(Color.fromJson(json.getAsJsonObject("fogColor")));
        }
        if (json.has("fastFog")) {
            setFastFog(json.get("fastFog").getAsBoolean());
        }
        setBiomeColorsEnabled(json.get("biomeColorsEnabled").getAsBoolean());
        setTransparentSky(json.get("transparentSky").getAsBoolean());
        if (json.has("fogDensity")) {
            setFogDensity(json.get("fogDensity").getAsDouble());
        }
        setWaterHeight(json.get("waterHeight").getAsInt());
        world = World.fromJson(json.getAsJsonObject("world"));
        camera = Camera.fromJson(json.getAsJsonObject("camera"));
        sun = Sun.fromJson(json.getAsJsonObject("sun"));
        sky = Sky.fromJson(json.getAsJsonObject("sky"));

        List<ChunkPosition> chunks = new ArrayList<>();
        for (JsonElement jsonElement : json.getAsJsonArray("chunkList")) {
            JsonArray chunk = jsonElement.getAsJsonArray();
            chunks.add(new ChunkPosition(chunk.get(0).getAsInt(), chunk.get(1).getAsInt()));

        }
        setChunkList(chunks);
    }

    public int getDumpFrequency() {
        return dumpFrequency;
    }

    public void setDumpFrequency(int dumpFrequency) {
        this.dumpFrequency = dumpFrequency;
    }

    public boolean isSaveSnapshots() {
        return saveSnapshots;
    }

    public boolean isEmittersEnabled() {
        return emittersEnabled;
    }

    public void setEmittersEnabled(boolean emittersEnabled) {
        this.emittersEnabled = emittersEnabled;
    }

    public double getEmitterIntensity() {
        return emitterIntensity;
    }

    public void setEmitterIntensity(double emitterIntensity) {
        this.emitterIntensity = emitterIntensity;
    }

    public boolean isSunEnabled() {
        return sunEnabled;
    }

    public void setSunEnabled(boolean sunEnabled) {
        this.sunEnabled = sunEnabled;
    }

    public boolean isStillWater() {
        return stillWater;
    }

    public void setStillWater(boolean stillWater) {
        this.stillWater = stillWater;
    }

    public double getWaterOpacity() {
        return waterOpacity;
    }

    public void setWaterOpacity(double waterOpacity) {
        this.waterOpacity = waterOpacity;
    }

    public double getWaterVisibility() {
        return waterVisibility;
    }

    public void setWaterVisibility(double waterVisibility) {
        this.waterVisibility = waterVisibility;
    }

    public Color getWaterColor() {
        return waterColor;
    }

    public void setWaterColor(Color waterColor) {
        this.waterColor = waterColor;
    }

    public Color getFogColor() {
        return fogColor;
    }

    public void setFogColor(Color fogColor) {
        this.fogColor = fogColor;
    }

    public boolean isFastFog() {
        return fastFog;
    }

    public void setFastFog(boolean fastFog) {
        this.fastFog = fastFog;
    }

    public boolean isBiomeColorsEnabled() {
        return biomeColorsEnabled;
    }

    public void setBiomeColorsEnabled(boolean biomeColorsEnabled) {
        this.biomeColorsEnabled = biomeColorsEnabled;
    }

    public boolean isTransparentSky() {
        return transparentSky;
    }

    public void setTransparentSky(boolean transparentSky) {
        this.transparentSky = transparentSky;
    }

    public double getFogDensity() {
        return fogDensity;
    }

    public void setFogDensity(double fogDensity) {
        this.fogDensity = fogDensity;
    }

    public int getWaterHeight() {
        return waterHeight;
    }

    public void setWaterHeight(int waterHeight) {
        this.waterHeight = waterHeight;
    }

    public Sky getSky() {
        return sky;
    }

    public Camera getCamera() {
        return camera;
    }

    public Sun getSun() {
        return sun;
    }

    public World getWorld() {
        return world;
    }

    public List<ChunkPosition> getChunkList() {
        return chunkList;
    }

    public void setChunkList(List<ChunkPosition> chunkList) {
        this.chunkList = chunkList;
    }

    private static int getPostProcessConfig(PostprocessMethod postprocess) {
        if (postprocess != null)
            switch (postprocess) {
                case None:
                    return 0;
                case GammaCorrection:
                    return 1;
                case ToneMap:
                    return 2;
            }
        return 0;
    }

    private static PostprocessMethod getPostProcess(int postprocess) {
        switch (postprocess) {
            case 0:
                return PostprocessMethod.None;
            case 1:
                return PostprocessMethod.GammaCorrection;
            case 2:
                return PostprocessMethod.ToneMap;
        }
        return null;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.addProperty("sdfVersion", SDF_VERSION);
        json.addProperty("name", getName());
        json.addProperty("width", getWidth());
        json.addProperty("height", getHeight());
        json.addProperty("exposure", getExposure());
        json.addProperty("postprocess", getPostProcessConfig(getPostProcess()));
        json.addProperty("rayDepth", getRayDepth());
        json.addProperty("pathTrace", isPathTrace());
        json.addProperty("dumpFrequency", getDumpFrequency());
        json.addProperty("saveSnapshots", isSaveSnapshots());
        json.addProperty("emittersEnabled", isEmittersEnabled());
        json.addProperty("emitterIntensity", getEmitterIntensity());
        json.addProperty("sunEnabled", isSunEnabled());
        json.addProperty("stillWater", isStillWater());
        json.addProperty("waterOpacity", getWaterOpacity());
        json.addProperty("waterVisibility", getWaterVisibility());
        json.addProperty("useCustomWaterColor", getWaterColor() != null);
        if (getWaterColor() != null) {
            json.add("waterColor", getWaterColor().toJson());
        }
        json.add("fogColor", getFogColor().toJson());
        json.addProperty("fastFog", isFastFog());
        json.addProperty("biomeColorsEnabled", isBiomeColorsEnabled());
        json.addProperty("transparentSky", isTransparentSky());
        json.addProperty("fogDensity", getFogDensity());
        json.addProperty("waterHeight", getWaterHeight());
        json.add("world", getWorld().toJson());
        json.add("camera", getCamera().toJson());
        json.add("sun", getSun().toJson());
        json.add("sky", getSky().toJson());

        JsonArray chunkList = new JsonArray();
        for (ChunkPosition chunkPos : getChunkList()) {
            JsonArray chunk = new JsonArray();
            chunk.add(new JsonPrimitive(chunkPos.getX()));
            chunk.add(new JsonPrimitive(chunkPos.getZ()));
            chunkList.add(chunk);
        }
        json.add("chunkList", chunkList);

        return json;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getExposure() {
        return exposure;
    }

    public void setExposure(double exposure) {
        this.exposure = exposure;
    }

    public PostprocessMethod getPostProcess() {
        return postProcess;
    }

    public void setPostProcess(PostprocessMethod postProcess) {
        this.postProcess = postProcess;
    }

    public int getRayDepth() {
        return rayDepth;
    }

    public void setRayDepth(int rayDepth) {
        this.rayDepth = rayDepth;
    }

    public boolean isPathTrace() {
        return pathTrace;
    }

    public void setPathTrace(boolean pathTrace) {
        this.pathTrace = pathTrace;
    }
}
