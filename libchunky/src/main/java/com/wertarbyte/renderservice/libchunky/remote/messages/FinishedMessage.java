/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Message to send the render dump after rendering.
 */
public class FinishedMessage extends Message {
    private final ChunkyRenderDump dump;

    FinishedMessage(DataInputStream inputStream) throws IOException {
        int width = inputStream.readInt();
        int height = inputStream.readInt();
        double[] samples = new double[width * height * 3];
        for (int i = 0; i < samples.length; i++) {
            samples[i] = inputStream.readDouble();
        }
        dump = new ChunkyRenderDump(width, height, samples, inputStream.readInt(), inputStream.readLong());
    }

    public FinishedMessage(ChunkyRenderDump dump) {
        this.dump = dump;
    }

    @Override
    protected byte getId() {
        return Message.FINISHED;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt(dump.getWidth());
        outputStream.writeInt(dump.getHeight());
        double[] samples = dump.getSamples();
        for (double sample : samples) {
            outputStream.writeDouble(sample);
        }
        outputStream.writeInt(dump.getSpp());
        outputStream.writeLong(dump.getRenderTime());
    }

    public ChunkyRenderDump getDump() {
        return dump;
    }
}
