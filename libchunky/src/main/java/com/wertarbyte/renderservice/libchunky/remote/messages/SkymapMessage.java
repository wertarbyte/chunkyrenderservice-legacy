/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;

/**
 * A message to send skymaps.
 */
public class SkymapMessage extends Message {
    private final File skymap;

    SkymapMessage(DataInputStream inputStream) throws IOException {
        skymap = File.createTempFile("texturepackmessage", "skymap" + inputStream.readUTF());
        readFile(inputStream, skymap);
    }

    public SkymapMessage(File skymap) {
        this.skymap = skymap;
    }

    @Override
    protected byte getId() {
        return Message.SEND_SKYMAP;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        outputStream.writeUTF(skymap.getName());
        sendFile(outputStream, skymap);
    }

    public File getSkymap() {
        return skymap;
    }

    public File moveTo(File directory) throws IOException {
        Path target = Paths.get(directory.getAbsolutePath(), skymap.getName());
        Files.move(skymap.toPath(), target, StandardCopyOption.REPLACE_EXISTING);
        return target.toFile();
    }
}
