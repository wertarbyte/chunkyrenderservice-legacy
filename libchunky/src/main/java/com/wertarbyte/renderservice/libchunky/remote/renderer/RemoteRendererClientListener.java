package com.wertarbyte.renderservice.libchunky.remote.renderer;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;

import java.awt.image.BufferedImage;

/**
 * A listener for {@link RemoteRendererClient}s.
 */
public interface RemoteRendererClientListener {
    /**
     * Gets called after the client connected to a server.
     *
     * @param client remote renderer client
     */
    void onConnected(RemoteRendererClient client);

    /**
     * Gets called just before the client starts rendering.
     *
     * @param client remote renderer client
     */
    void onRenderingStarting(RemoteRendererClient client);

    /**
     * Gets called after rendering has stopped (no matter if it failed or succeeded).
     *
     * @param client remote renderer client
     */
    void onRenderingStopped(RemoteRendererClient client);

    /**
     * Gets called after rendering completed successfully.
     *
     * @param client remote renderer client
     * @param image  the rendered image
     */
    void onFinished(RemoteRendererClient client, BufferedImage image);

    /**
     * Gets called whenever the status changes.
     *
     * @param client remote renderer client
     * @param task   currently performed task
     */
    void onStatusChanged(RemoteRendererClient client, ChunkyTask task);

    /**
     * Gets called whenever the rendering status changes.
     *
     * @param client     remote renderer client
     * @param currentSpp current spp
     * @param targetSpp  target spp
     */
    void onRenderStatusChanged(RemoteRendererClient client, int currentSpp, int targetSpp);
}
