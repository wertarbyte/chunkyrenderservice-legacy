/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.renderer;

import com.wertarbyte.renderservice.libchunky.ChunkyProcessWrapper;
import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.ChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.RenderListener;
import com.wertarbyte.renderservice.libchunky.remote.ChunkyWrapperFactory;
import com.wertarbyte.renderservice.libchunky.remote.messages.*;
import com.wertarbyte.renderservice.libchunky.remote.util.CompressedBlockInputStream;
import com.wertarbyte.renderservice.libchunky.remote.util.CompressedBlockOutputStream;
import com.wertarbyte.renderservice.libchunky.util.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Client for the {@link com.wertarbyte.renderservice.libchunky.remote.server.RemoteRenderer} that performs the rendering.
 */
public class RemoteRendererClient implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(RemoteRendererClient.class);
    protected final ChunkyWrapperFactory chunkyFactory;
    protected final String host;
    protected final int port;
    private final List<RemoteRendererClientListener> listeners = new CopyOnWriteArrayList<>();
    private Socket connection;
    private boolean closing;
    private Map<String, String> additionalData = new HashMap<>();
    private boolean rendering;
    private ChunkyWrapper chunky;

    private DataOutputStream out;
    private DataInputStream in;

    public RemoteRendererClient(ChunkyWrapperFactory chunkyFactory, String host, int port) {
        this.chunkyFactory = chunkyFactory;
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                connection = new Socket(host, port);
            } catch (IOException e) {
                LOGGER.error("Could not connect", e);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    LOGGER.warn("Interrupted while waiting for reconnect", e);
                    break;
                }
                continue;
            }

            try {
                out = new DataOutputStream(new CompressedBlockOutputStream(new BufferedOutputStream(connection.getOutputStream()), 8192));
                in = new DataInputStream(new CompressedBlockInputStream(new BufferedInputStream(connection.getInputStream())));
                for (RemoteRendererClientListener listener : listeners) {
                    listener.onConnected(this);
                }

                while (!Thread.interrupted() && !connection.isClosed()) {
                    final AtomicBoolean finished = new AtomicBoolean(false);
                    rendering = false;

                    chunky = chunkyFactory.getChunkyInstance();
                    final File sceneDirectory = Files.createTempDirectory("remotechunky").toFile();
                    chunky.setSceneDirectory(sceneDirectory);

                    out.flush();

                    chunky.addListener(new RenderListener() {
                        @Override
                        public void onFinished() {
                            try {
                                sendDump();
                            } catch (IOException e) {
                                LOGGER.error("Could not send finished status", e);
                                chunky.stop();
                                return;
                            }
                            finished.set(true);

                            try {
                                BufferedImage image = chunky.getImage();
                                for (RemoteRendererClientListener listener : listeners) {
                                    listener.onFinished(RemoteRendererClient.this, image);
                                }
                            } catch (IOException e) {
                                LOGGER.error("Could not get image", e);
                            }
                        }

                        @Override
                        public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                            try {
                                Message.write(out, new RenderingStatusMessage(
                                        currentSpp,
                                        targetSpp,
                                        chunky.getSamplesPerSecond()));
                            } catch (IOException e) {
                                LOGGER.error("Could not send status", e);
                                chunky.stop();
                            }

                            for (RemoteRendererClientListener listener : listeners) {
                                listener.onRenderStatusChanged(RemoteRendererClient.this, currentSpp, targetSpp);
                            }
                        }

                        @Override
                        public void onStatusChanged(ChunkyTask task) {
                            try {
                                Message.write(out, new RenderStatusMessage(task));
                            } catch (IOException e) {
                                LOGGER.error("Could not send status", e);
                                chunky.stop();
                            }

                            for (RemoteRendererClientListener listener : listeners) {
                                listener.onStatusChanged(RemoteRendererClient.this, task);
                            }
                        }
                    });

                    Message message;

                    while (!Thread.interrupted() && (message = Message.read(in)) != null) {
                        if (message instanceof SetTargetSppMessage) {
                            chunky.setTargetSpp(((SetTargetSppMessage) message).getSpp());
                        } else if (message instanceof SetThreadCountMessage) {
                            chunky.setThreadCount(((SetThreadCountMessage) message).getThreads());
                        } else if (message instanceof SceneMessage) {
                            chunky.setScene(((SceneMessage) message).getScene());
                        } else if (message instanceof SceneFilesMessage) {
                            Files.copy(((SceneFilesMessage) message).getFoliage().toPath(),
                                    Paths.get(sceneDirectory.getAbsolutePath(), chunky.getScene().getName() + ".foliage"),
                                    StandardCopyOption.REPLACE_EXISTING);
                            Files.copy(((SceneFilesMessage) message).getGrass().toPath(),
                                    Paths.get(sceneDirectory.getAbsolutePath(), chunky.getScene().getName() + ".grass"),
                                    StandardCopyOption.REPLACE_EXISTING);
                            Files.copy(((SceneFilesMessage) message).getOctree().toPath(),
                                    Paths.get(sceneDirectory.getAbsolutePath(), chunky.getScene().getName() + ".octree"),
                                    StandardCopyOption.REPLACE_EXISTING);
                        } else if (message instanceof TexturepackMessage) {
                            Files.copy(((TexturepackMessage) message).getTexturepack().toPath(),
                                    Paths.get(sceneDirectory.getAbsolutePath(), "texturepack.zip"),
                                    StandardCopyOption.REPLACE_EXISTING);
                            chunky.setTexturepack(new File(sceneDirectory.getAbsolutePath(), "texturepack.zip"));
                        } else if (message instanceof SkymapMessage) {
                            if (chunky.getScene() != null) {
                                chunky.getScene().getSky().setSkymap(((SkymapMessage) message).moveTo(sceneDirectory).getAbsolutePath());
                            } else {
                                LOGGER.warn("No scene set, skymap ignored");
                            }
                        } else if (message instanceof AdditionalDataMessage) {
                            additionalData.putAll(((AdditionalDataMessage) message).getData());
                        } else if (message instanceof StartMessage) {
                            for (RemoteRendererClientListener listener : listeners) {
                                listener.onRenderingStarting(this);
                            }
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Message.write(out, new SetThreadCountMessage(chunky.getThreadCount())); //tell our default thread count to master

                                        rendering = true;
                                        chunky.render();
                                    } catch (IOException e) {
                                        LOGGER.error("Rendering failed", e);
                                        try {
                                            Message.write(out, new FailureMessage());
                                        } catch (IOException e1) {
                                            LOGGER.error("Could not send failure message to the server", e1);
                                        }
                                    } finally {
                                        rendering = false;

                                        for (RemoteRendererClientListener listener : listeners) {
                                            listener.onRenderingStopped(RemoteRendererClient.this);
                                        }
                                    }
                                }
                            }).start();
                        } else if (message instanceof StopMessage) {
                            chunky.stop();
                        } else if (message instanceof WeAreDoneMessage) {
                            LOGGER.info("Cleaning up...");
                            int files = FileUtil.deleteDirectory(sceneDirectory);
                            LOGGER.info("Cleanup done, " + files + " files removed");
                            break;
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.info("Connection closed");
                LOGGER.debug("Connection closed", e);
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.warn("Could not close input stream", e);
                }
                try {
                    out.close();
                } catch (IOException e) {
                    LOGGER.warn("Could not close output stream", e);
                }
            }

            if (closing || Thread.interrupted()) {
                break;
            } else {
                //disconnected, retry after 5 seconds
                LOGGER.info("Reconnecting in 5 seconds...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }

    protected void sendDump() throws IOException {
        Message.write(out, new FinishedMessage(chunky.getDump()));
    }

    public void stop() throws IOException {
        closing = true;
        connection.close();
    }

    public static void main(String[] args) {
        new RemoteRendererClient(new ChunkyWrapperFactory() {
            @Override
            public ChunkyWrapper getChunkyInstance() {
                return new ChunkyProcessWrapper();
            }
        }, "127.0.0.1", 19876).run();
    }

    /**
     * Checks if the underlying chunky instance is rendering at the moment.
     *
     * @return true if the underlying chunky instance is rendering
     */
    public boolean isRendering() {
        return rendering;
    }

    /**
     * Gets the current chunky instance of this renderer.
     *
     * @return chunky instance
     */
    public ChunkyWrapper getChunky() {
        return chunky;
    }

    /**
     * Gets additional data sent by the server. The provided data depends on the implementation and
     * configuration of the server.
     *
     * @return additional data, might be empty
     */
    public Map<String, String> getAdditionalData() {
        return additionalData;
    }

    public void addListener(RemoteRendererClientListener listener) {
        listeners.add(listener);
    }

    public void removeListener(RemoteRendererClientListener listener) {
        listeners.remove(listener);
    }

    public void putData(String key, String value) throws IOException {
        Message.write(out, new AdditionalDataMessage().put(key, value));
        additionalData.put(key, value);
    }
}
