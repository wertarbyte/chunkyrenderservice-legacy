package com.wertarbyte.renderservice.libchunky.remote.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * A message to send any string data to the client.
 */
public class AdditionalDataMessage extends Message {
    private final Map<String, String> data;

    AdditionalDataMessage(DataInputStream inputStream) throws IOException {
        int count = inputStream.readInt();
        data = new HashMap<>(count);

        for (int i = 0; i < count; i++) {
            data.put(inputStream.readUTF(), inputStream.readUTF());
        }
    }

    public AdditionalDataMessage() {
        data = new HashMap<>();
    }

    public Map<String, String> getData() {
        return Collections.unmodifiableMap(data);
    }

    public AdditionalDataMessage put(String key, String value) {
        data.put(key, value);
        return this;
    }

    public Message putAll(Map<String, String> data) {
        this.data.putAll(data);
        return this;
    }

    @Override
    protected byte getId() {
        return Message.ADDITIONAL_DATA;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt(data.size());

        for (Map.Entry<String, String> entry : data.entrySet()) {
            outputStream.writeUTF(entry.getKey());
            outputStream.writeUTF(entry.getValue());
        }
    }
}
