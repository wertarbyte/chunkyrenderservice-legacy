package com.wertarbyte.renderservice.libchunky.remote.server;

/**
 * A listener for a {@link RemoteRendererPool}s.
 */
public interface RemoteRendererPoolListener {
    /**
     * Gets invoked when a renderer connects.
     *
     * @param pool     pool instance
     * @param renderer renderer
     */
    void onRendererConnected(RemoteRendererPool pool, RemoteRenderer renderer);

    /**
     * Gets invoked when a renderer disconnects.
     *
     * @param pool     pool instance
     * @param renderer renderer
     */
    void onRendererDisconnected(RemoteRendererPool pool, RemoteRenderer renderer);

    /**
     * Gets invoked when a renderer is ready (initialized after connecting) or when a renderer is available
     * again after rendering.
     *
     * @param pool     pool instance
     * @param renderer renderer
     */
    void onRendererAvailable(RemoteRendererPool pool, RemoteRenderer renderer);

    /**
     * Gets invoked just before a renderer instance is checked out to be used.
     *
     * @param pool     pool instance
     * @param renderer renderer
     */
    void onRendererUnavailable(RemoteRendererPool pool, RemoteRenderer renderer);
}
