/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonObject;

public class World {
    private Number dimension = 0;
    private String path = "";

    public Number getDimension() {
        return this.dimension;
    }

    public void setDimension(Number dimension) {
        this.dimension = dimension;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.addProperty("path", getPath());
        json.addProperty("dimension", getDimension());
        return json;
    }

    public static World fromJson(JsonObject json) {
        World world = new World();
        world.setPath(json.get("path").getAsString());
        world.setDimension(json.get("dimension").getAsInt());
        return world;
    }
}
