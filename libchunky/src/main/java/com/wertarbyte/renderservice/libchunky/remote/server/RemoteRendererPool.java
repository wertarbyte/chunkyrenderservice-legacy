/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.server;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wertarbyte.renderservice.libchunky.ChunkyTask;
import com.wertarbyte.renderservice.libchunky.RenderListener;
import com.wertarbyte.renderservice.libchunky.remote.UnmodifiableRemoteRenderer;
import com.wertarbyte.renderservice.libchunky.scene.PostprocessMethod;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.util.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A pool with connections to remote renderers.
 */
public class RemoteRendererPool implements RendererPool {
    private static final Logger LOGGER = LogManager.getLogger(RemoteRendererPool.class);

    private final int port;
    private BlockingQueue<RemoteRenderer> renderers;
    private List<RemoteRenderer> workingRenderers;

    private Thread thread;
    private ServerSocket serverSocket;

    private List<RemoteRendererPoolListener> listeners = new CopyOnWriteArrayList<>();

    public RemoteRendererPool() {
        this(19876);
    }

    public RemoteRendererPool(int port) {
        this.port = port;
        renderers = new LinkedBlockingQueue<>();
        workingRenderers = new CopyOnWriteArrayList<>();
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        serverSocket = new ServerSocket(port);
                        while (!Thread.interrupted()) {
                            RemoteRenderer renderer = new RemoteRenderer(RemoteRendererPool.this, serverSocket.accept());
                            Thread remoteObserver = new Thread(renderer);
                            remoteObserver.setDaemon(true);
                            remoteObserver.start(); //start connection observer

                            for (RemoteRendererPoolListener listener : listeners) {
                                listener.onRendererConnected(RemoteRendererPool.this, renderer);
                            }
                        }
                    } catch (IOException e) {
                        if (serverSocket.isClosed()) {
                            LOGGER.info("Socket closed", e);
                        } else {
                            LOGGER.error("Socket error", e);
                        }
                    } finally {
                        //close all connections
                        if (serverSocket != null) {
                            try {
                                serverSocket.close();
                            } catch (IOException e) {
                                LOGGER.warn("Could not close server socket", e);
                            }
                        }
                    }
                }
            });
            thread.start();
        }
    }

    public void stop() throws IOException {
        if (thread != null) {
            thread.interrupt();
            thread = null;
            serverSocket.close();
        }
    }

    @Override
    public RemoteRenderer getChunkyInstance() {
        try {
            RemoteRenderer renderer = renderers.take();
            workingRenderers.add(renderer);
            renderer.setPool(this);

            for (RemoteRendererPoolListener listener : listeners) {
                listener.onRendererUnavailable(this, renderer);
            }

            return renderer;
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while waiting for a chunky instance", e);
        }
    }

    void done(RemoteRenderer chunky) {
        add(chunky);
    }

    @Override
    public int getActiveRenderersCount() {
        return workingRenderers.size();
    }

    @Override
    public List<? extends UnmodifiableRemoteRenderer> getActiveRenderers() {
        return Collections.unmodifiableList(workingRenderers);
    }

    @Override
    public Collection<? extends UnmodifiableRemoteRenderer> getUnusedRenderers() {
        return Collections.unmodifiableCollection(renderers);
    }

    @Override
    public int getAvailableRenderersCount() {
        return renderers.size();
    }

    @Override
    public int getSize() {
        return getAvailableRenderersCount() + getActiveRenderersCount();
    }

    @Override
    public void stopRenderer(UnmodifiableRemoteRenderer renderer) {
        if (renderer instanceof RemoteRenderer) {
            ((RemoteRenderer) renderer).stop();
            done((RemoteRenderer) renderer);
        } else {
            LOGGER.warn("Could not stop renderer");
        }
    }

    public static void main(String[] args) throws Exception {
        RemoteRendererPool pool = new RemoteRendererPool();
        pool.start();

        for (int i = 0; i < 3; i++) {
            File sceneDirectory = Files.createTempDirectory("remoterendererpool").toFile();
            FileUtil.extractResource("testscene/test_scene.foliage", new File(sceneDirectory, "test_scene.foliage"));
            FileUtil.extractResource("testscene/test_scene.grass", new File(sceneDirectory, "test_scene.grass"));
            FileUtil.extractResource("testscene/test_scene.octree", new File(sceneDirectory, "test_scene.octree"));
            FileUtil.extractResource("testscene/test_scene.json", new File(sceneDirectory, "test_scene.json"));

            RemoteRenderer chunky = pool.getChunkyInstance();
            chunky.setTargetSpp(1);
            chunky.setThreadCount(2);
            chunky.setSceneDirectory(sceneDirectory);
            chunky.addListener(new RenderListener() {
                @Override
                public void onFinished() {

                }

                @Override
                public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                    System.out.println("Rendering... " + currentSpp + "/" + targetSpp);
                }

                @Override
                public void onStatusChanged(ChunkyTask task) {

                }
            });

            try (InputStreamReader stream = new InputStreamReader(pool.getClass().getClassLoader()
                    .getResourceAsStream("testscene/test_scene.json"))) {
                chunky.setScene(new SceneDescription((new Gson()).fromJson(stream, JsonObject.class)));
            }

            System.out.println("Start rendering...");
            try {
                chunky.render();
                System.out.println("Finished!");
                ImageIO.write(chunky.getDump().getPicture(1.0, PostprocessMethod.GammaCorrection), "PNG", new File("/home/maik/remotetest" + i + ".png"));
                pool.done(chunky);
            } catch (IOException e) {
                LOGGER.error("Rendering failed", e);
            }
        }

        pool.stop();
    }

    public void addListener(RemoteRendererPoolListener listener) {
        listeners.add(listener);
    }

    public void removeListener(RemoteRendererPoolListener listener) {
        listeners.remove(listener);
    }

    void add(RemoteRenderer renderer) {
        workingRenderers.remove(renderer);
        renderer.reset();
        renderers.add(renderer);

        for (RemoteRendererPoolListener listener : listeners) {
            listener.onRendererAvailable(this, renderer);
        }
    }

    void onDisconnect(RemoteRenderer renderer) {
        renderers.remove(renderer);
        workingRenderers.remove(renderer);

        for (RemoteRendererPoolListener listener : listeners) {
            listener.onRendererDisconnected(this, renderer);
        }
    }

}