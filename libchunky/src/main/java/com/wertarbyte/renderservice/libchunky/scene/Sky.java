/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonObject;

public class Sky {
    private double skyYaw = 0;
    private boolean skyMirrored = true;
    private double skyLightModifier = 1.0;
    private String skymap = "";
    private SkyMode mode = SkyMode.SKYMAP_PANORAMIC;
    private double horizonOffset = 0.1;
    private boolean cloudsEnabled = false;
    private double cloudSize = 64;
    private Position cloudOffset = new Position(0, 128, 0);

    public double getSkyYaw() {
        return skyYaw;
    }

    public void setSkyYaw(double skyYaw) {
        this.skyYaw = skyYaw;
    }

    public boolean isSkyMirrored() {
        return skyMirrored;
    }

    public void setSkyMirrored(boolean skyMirrored) {
        this.skyMirrored = skyMirrored;
    }

    public double getSkyLightModifier() {
        return skyLightModifier;
    }

    public void setSkyLightModifier(double skyLightModifier) {
        this.skyLightModifier = skyLightModifier;
    }

    public String getSkymap() {
        return skymap;
    }

    public void setSkymap(String skymap) {
        this.skymap = skymap;
    }

    public SkyMode getMode() {
        return mode;
    }

    public void setMode(SkyMode mode) {
        this.mode = mode;
    }

    public double getHorizonOffset() {
        return horizonOffset;
    }

    public void setHorizonOffset(double horizonOffset) {
        this.horizonOffset = horizonOffset;
    }

    public boolean isCloudsEnabled() {
        return cloudsEnabled;
    }

    public void setCloudsEnabled(boolean cloudsEnabled) {
        this.cloudsEnabled = cloudsEnabled;
    }

    public double getCloudSize() {
        return cloudSize;
    }

    public void setCloudSize(double cloudSize) {
        this.cloudSize = cloudSize;
    }

    public Position getCloudOffset() {
        return cloudOffset;
    }

    public void setCloudOffset(Position cloudOffset) {
        this.cloudOffset = cloudOffset;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("skyYaw", getSkyYaw());
        json.addProperty("skyMirrored", isSkyMirrored());
        json.addProperty("skyLight", getSkyLightModifier());
        json.addProperty("mode", getMode().name());
        json.addProperty("horizonOffset", getHorizonOffset());
        json.addProperty("cloudsEnabled", isCloudsEnabled());
        json.addProperty("cloudSize", getCloudSize());
        json.add("cloudOffset", getCloudOffset().toJson());
        //sky.addProperty("gradient", gradientJson(gradient)); //TODO
        json.addProperty("skymap", getSkymap());

        return json;
    }

    public static Sky fromJson(JsonObject json) {
        Sky sky = new Sky();
        sky.setSkyYaw(json.get("skyYaw").getAsDouble());
        sky.setSkyMirrored(json.get("skyMirrored").getAsBoolean());
        sky.setSkyLightModifier(json.get("skyLight").getAsDouble());
        sky.setMode(SkyMode.valueOf(json.get("mode").getAsString()));
        sky.setHorizonOffset(json.get("horizonOffset").getAsDouble());
        sky.setCloudsEnabled(json.get("cloudsEnabled").getAsBoolean());
        sky.setCloudSize(json.get("cloudSize").getAsDouble());
        sky.setCloudOffset(Position.fromJson(json.getAsJsonObject("cloudOffset")));
        if (json.has("skymap")) {
            sky.setSkymap(json.get("skymap").getAsString());
        }
        return sky;
    }
}
