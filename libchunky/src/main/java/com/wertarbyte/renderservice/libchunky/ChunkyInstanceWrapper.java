/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.scene.Sky;
import com.wertarbyte.renderservice.libchunky.scene.Sun;
import com.wertarbyte.renderservice.libchunky.status.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import se.llbit.chunky.main.ChunkyOptions;
import se.llbit.chunky.renderer.PlaceholderRenderCanvas;
import se.llbit.chunky.renderer.RenderContext;
import se.llbit.chunky.renderer.RenderManager;
import se.llbit.chunky.renderer.scene.Scene;
import se.llbit.chunky.renderer.scene.SceneLoadingError;
import se.llbit.chunky.resources.TexturePackLoader;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A wrapper for chunky.
 */
public class ChunkyInstanceWrapper implements ChunkyWrapper {
    private static final Logger LOGGER = LogManager.getLogger(ChunkyInstanceWrapper.class);

    private RenderManager renderManager;
    private final List<RenderListener> listeners = new CopyOnWriteArrayList<>();

    private File sceneDirectory;
    private File texturePack;

    private RenderStatus status = new RenderStatus(false, false);
    private RenderStatus previousStatus = status;

    private int targetSpp = 10;
    private int threadCount = 1;
    private SceneDescription scene;

    @Override
    public void render() throws IOException {
        render(false);

        while (renderManager.isAlive()) {
            Thread.yield();
        }
    }

    public void render(final boolean continuous) throws IOException {
        ChunkyOptions options = new ChunkyOptions();
        options.sceneDir = sceneDirectory;
        options.sceneName = scene.getName();
        options.renderThreads = threadCount;
        options.target = targetSpp;

        if (texturePack != null) {
            options.texturePack = texturePack.getAbsolutePath();
            try {
                TexturePackLoader.loadTexturePack(new File(options.texturePack), false);
            } catch (TexturePackLoader.TextureLoadingError e) {
                throw new IOException("Could not load texture pack", e);
            }
        }
        renderManager = new RenderManager(
                new PlaceholderRenderCanvas(),
                new RenderContext(options), new RenderStatusListenerWrapper(new RenderListener() {
            @Override
            public void onFinished() {
                previousStatus = status;
                status = new RenderStatus(continuous, true);

                for (RenderListener listener : listeners) {
                    listener.onFinished();
                }
            }

            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                previousStatus = status;
                status = new RenderingStatus(currentSpp, targetSpp);

                for (RenderListener listener : listeners) {
                    listener.onRenderStatusChanged(currentSpp, targetSpp);
                }
            }

            @Override
            public void onStatusChanged(ChunkyTask task) {
                previousStatus = status;
                switch (task) {
                    case LOADING:
                        status = new LoadingStatus("", LoadingStatus.Element.Unknown, -1);
                        break;
                    case WRITING_PNG:
                        status = new SavingStatus("", SavingStatus.Element.Unknown, -1);
                        break;
                    case SAVING:
                        status = new SavingStatus("", SavingStatus.Element.Unknown, -1);
                        break;
                    case RENDERING:
                        status = new RenderingStatus("", renderManager.getCurrentSPP(), targetSpp);
                        break;
                    case UNKNOWN:
                        status = new UnknownChunkyStatus("");
                        break;
                }

                for (RenderListener listener : listeners) {
                    listener.onStatusChanged(task);
                }
            }
        }), !continuous);

        try {
            renderManager.loadScene(options.sceneName);
        } catch (FileNotFoundException e) {
            renderManager.interrupt();
            throw new IOException("Scene \"" + options.sceneName + "\" not found!", e);
        } catch (IOException | SceneLoadingError e) {
            renderManager.interrupt();
            throw new IOException("Could not load the scene", e);
        } catch (InterruptedException e) {
            renderManager.interrupt();
            throw new IOException("Interrupted while loading scene", e);
        }

        setScene(scene);
        renderManager.setBufferFinalization(true); //allow preview while rendering
        renderManager.scene().startHeadlessRender();
        renderManager.setTargetSPP(targetSpp);
        renderManager.start();
    }

    @Override
    public void setSceneDirectory(File sceneDirectory) {
        this.sceneDirectory = sceneDirectory;
    }

    @Override
    public SceneDescription getScene() {
        return this.scene;
    }

    public void setScene(SceneDescription sd) {
        scene = sd;

        if (renderManager != null) {
            Scene scene = renderManager.scene();

            scene.setExposure(sd.getExposure());
            scene.setPostprocess(Converter.convert(sd.getPostProcess()));
            scene.setRayDepth(sd.getRayDepth());
            scene.setEmittersEnabled(sd.isEmittersEnabled());
            scene.setEmitterIntensity(sd.getEmitterIntensity());
            scene.setStillWater(sd.isStillWater());
            scene.setWaterOpacity(sd.getWaterOpacity());
            scene.setWaterVisibility(sd.getWaterVisibility());

            if (sd.getWaterColor() != null) {
                scene.setWaterColor(Converter.convert(sd.getWaterColor()));
                scene.setUseCustomWaterColor(true);
            } else {
                scene.setUseCustomWaterColor(false);
            }

            scene.setFogColor(Converter.convert(sd.getFogColor()));
            scene.setFastFog(sd.isFastFog());
            scene.setBiomeColorsEnabled(sd.isBiomeColorsEnabled());
            scene.setTransparentSky(sd.isTransparentSky());
            scene.setFogDensity(sd.getFogDensity());
            scene.setWaterHeight(sd.getWaterHeight());

            Sky sky = sd.getSky();
            scene.sky().setRotation(sky.getSkyYaw());
            scene.sky().setMirrored(sky.isSkyMirrored());
            scene.sky().setSkyLight(sky.getSkyLightModifier());
            if (sky.getSkymap() != null) {
                scene.sky().loadSkymap(sky.getSkymap());
            }
            scene.sky().setSkyMode(Converter.convert(sky.getMode()));
            scene.sky().setHorizonOffset(sky.getHorizonOffset());
            scene.sky().setCloudsEnabled(sky.isCloudsEnabled());
            scene.sky().setCloudSize(sky.getCloudSize());
            scene.sky().setCloudXOffset(sky.getCloudOffset().getX());
            scene.sky().setCloudYOffset(sky.getCloudOffset().getY());
            scene.sky().setCloudZOffset(sky.getCloudOffset().getZ());

            com.wertarbyte.renderservice.libchunky.scene.Camera camera = sd.getCamera();
            scene.camera().setDof(camera.getDof());
            scene.camera().setSubjectDistance(camera.getFocalOffset());
            scene.camera().setFoV(camera.getFov());
            scene.camera().setView(camera.getOrientation().getYaw(), camera.getOrientation().getPitch(), camera.getOrientation().getRoll());
            scene.camera().setPosition(Converter.convert(camera.getPosition()));
            scene.camera().setProjectionMode(Converter.convert(camera.getProjectionMode()));

            Sun sun = sd.getSun();
            scene.sun().setAltitude(sun.getAltitude());
            scene.sun().setAzimuth(sun.getAzimuth());
            scene.sun().setColor(Converter.convert(sun.getColor()));
            scene.sun().setIntensity(sun.getIntensity());

            renderManager.scene().resetRender();
        } else {
            File f = new File(sceneDirectory, scene.getName() + ".json");
            try (FileWriter writer = new FileWriter(f)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                writer.write(gson.toJson(scene.toJson()));
                writer.flush();
            } catch (IOException e) {
                LOGGER.error("Could not create scene description file", e);
            }
        }
    }

    @Override
    public void setTexturepack(File texturepack) {
        this.texturePack = texturepack;
    }

    /**
     * Starts continuous rendering. Use {@link #stop()} to stop it.
     *
     * @throws IOException if starting continuous rendering fails
     */
    public void start() throws IOException {
        render(true);
    }

    @Override
    public void stop() {
        if (renderManager != null) {
            renderManager.interrupt();
        }
    }

    @Override
    public BufferedImage getImage() {
        BufferedImage img = new BufferedImage(renderManager.bufferedScene().width,
                renderManager.bufferedScene().height,
                BufferedImage.TYPE_INT_RGB);
        renderManager.drawBufferedImage(img.getGraphics(), img.getWidth(), img.getHeight());
        return img;
    }

    @Override
    public ChunkyRenderDump getDump() throws IOException {
        return new ChunkyRenderDump(renderManager.bufferedScene());
    }

    @Override
    public double getSamplesPerSecond() {
        if (renderManager != null) {
            return renderManager.getCurrentSPP();
        } else {
            return 0;
        }
    }

    @Override
    public void addListener(RenderListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(RenderListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void setTargetSpp(int targetSpp) {
        this.targetSpp = targetSpp;
        if (renderManager != null) {
            renderManager.setTargetSPP(targetSpp);
        }
    }

    @Override
    public int getThreadCount() {
        return threadCount;
    }

    @Override
    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
        if (renderManager != null) {
            renderManager.setNumThreads(threadCount);
        }
    }

    @Override
    public RenderStatus getStatus() {
        return status;
    }

    @Override
    public RenderStatus getPreviousStatus() {
        return previousStatus;
    }

    public boolean isStarted() {
        return renderManager != null && renderManager.isAlive();
    }
}
