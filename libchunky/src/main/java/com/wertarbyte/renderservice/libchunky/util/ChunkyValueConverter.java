/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.util;

public class ChunkyValueConverter {
    /**
     * Calculates pitch for chunky from Minecraft pitch.
     *
     * @param pitch Pitch in Minecraft (in degrees)
     * @return Pitch for chunky (in radiants)
     */
    public static double getPitchFromMinecraft(double pitch) {
        return (pitch - 90) % 360 * Math.PI / 180;
    }

    /**
     * Calculates yaw for chunky from Minecraft yaw.
     *
     * @param yaw Yaw in Minecraft (in degrees)
     * @return Yaw for chunky (in radiants)
     */
    public static double getYawFromMinecraft(double yaw) {
        return (-(yaw + 180) - 90) % 360 * Math.PI / 180;
    }
}
