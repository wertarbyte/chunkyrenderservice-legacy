/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.server;

import com.wertarbyte.renderservice.libchunky.ChunkyRenderDump;
import com.wertarbyte.renderservice.libchunky.ChunkyWrapper;
import com.wertarbyte.renderservice.libchunky.RenderListener;
import com.wertarbyte.renderservice.libchunky.remote.UnmodifiableRemoteRenderer;
import com.wertarbyte.renderservice.libchunky.remote.messages.*;
import com.wertarbyte.renderservice.libchunky.remote.util.CompressedBlockInputStream;
import com.wertarbyte.renderservice.libchunky.remote.util.CompressedBlockOutputStream;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.status.RenderStatus;
import com.wertarbyte.renderservice.libchunky.status.RenderingStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A renderer that works on a different machine using a TCP connection.
 */
public class RemoteRenderer implements ChunkyWrapper, Runnable, UnmodifiableRemoteRenderer {
    private static final Logger LOGGER = LogManager.getLogger(RemoteRenderer.class);

    private final RemoteRendererPool parentPool;
    private final Socket connection;
    private DataOutputStream out;
    private DataInputStream in;

    private final List<RenderListener> listeners = new CopyOnWriteArrayList<>();
    private int threadCount = 1;
    private File sceneDirectory;
    private SceneDescription scene;
    private ChunkyRenderDump dump;
    private File texturepack;
    private Map<String, String> additionalData = new HashMap<>();

    private RenderStatus status = new RenderStatus(false, false);
    private RenderStatus previousStatus = status;
    private double samplesPerSecond;
    private boolean finished;
    private RemoteRendererPool pool;

    RemoteRenderer(RemoteRendererPool parentPool, Socket connection) throws IOException {
        this.parentPool = parentPool;
        this.connection = connection;
        out = new DataOutputStream(new CompressedBlockOutputStream(new BufferedOutputStream(connection.getOutputStream()), 8192));
        out.flush();
    }

    @Override
    public void setSceneDirectory(File sceneDirectory) {
        this.sceneDirectory = sceneDirectory;
    }

    @Override
    public SceneDescription getScene() {
        return scene;
    }

    @Override
    public void setScene(SceneDescription scene) throws IOException {
        this.scene = scene;
    }

    @Override
    public void setTexturepack(File texturepack) {
        this.texturepack = texturepack;
    }

    @Override
    public String getAdditionalData(String key) {
        return additionalData.get(key);
    }

    public void putData(String key, String value) throws IOException {
        Message.write(out, new AdditionalDataMessage().put(key, value));
        additionalData.put(key, value);
    }

    public void putData(Map<String, String> data) throws IOException {
        Message.write(out, new AdditionalDataMessage().putAll(data));
        additionalData.putAll(data);
    }

    @Override
    public void render() throws IOException {
        Message.write(out, new SceneMessage(scene));

        Message.write(out, new SceneFilesMessage(
                new File(sceneDirectory, scene.getName() + ".foliage"),
                new File(sceneDirectory, scene.getName() + ".grass"),
                new File(sceneDirectory, scene.getName() + ".octree")
        ));

        if (texturepack != null) {
            Message.write(out, new TexturepackMessage(texturepack));
        }

        if (scene.getSky().getSkymap() != null && !"".equals(scene.getSky().getSkymap())) {
            Message.write(out, new SkymapMessage(new File(scene.getSky().getSkymap())));
        }

        samplesPerSecond = 0;
        finished = false;

        Message.write(out, new StartMessage());

        while (!finished) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }

        Message.write(out, new WeAreDoneMessage());

        if (dump == null) {
            throw new IOException("Rendering failed");
        }

        for (RenderListener listener : listeners) {
            listener.onFinished();
        }
    }

    /**
     * Puts this remote renderer back into the pool. Call this when you are finished with using this remote renderer.
     */
    public void done() {
        if (pool != null) {
            pool.done(this);
        }
    }

    @Override
    public void stop() {
        try {
            Message.write(out, new StopMessage(), new WeAreDoneMessage());
        } catch (IOException e) {
            LOGGER.error("Could not tell remote renderer to stop", e);
        }

        done();
    }

    @Override
    public BufferedImage getImage() throws IOException {
        if (dump != null) {
            return dump.getPicture(scene.getExposure(), scene.getPostProcess());
        }
        return null;
    }

    @Override
    public ChunkyRenderDump getDump() throws IOException {
        return dump;
    }

    @Override
    public double getSamplesPerSecond() {
        return samplesPerSecond;
    }

    @Override
    public void addListener(RenderListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(RenderListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void setTargetSpp(int targetSpp) {
        try {
            Message.write(out, new SetTargetSppMessage(targetSpp));
        } catch (IOException e) {
            LOGGER.error("Could not set SPP", e);
        }
    }

    @Override
    public int getThreadCount() {
        return threadCount;
    }

    @Override
    public void setThreadCount(int threadCount) {
        try {
            Message.write(out, new SetThreadCountMessage(threadCount));
            this.threadCount = threadCount;
        } catch (IOException e) {
            LOGGER.error("Could not set SPP", e);
        }
    }

    @Override
    public RenderStatus getStatus() {
        return status;
    }

    @Override
    public RenderStatus getPreviousStatus() {
        return previousStatus;
    }

    @Override
    public void run() {
        try {
            in = new DataInputStream(new CompressedBlockInputStream(new BufferedInputStream(connection.getInputStream())));
        } catch (IOException e) {
            LOGGER.error("Could not connect", e);
            return;
        }
        parentPool.add(this);

        final AtomicBoolean alive = new AtomicBoolean(true);

        Thread heartbeatThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.interrupted()) {
                    try {
                        Message.write(out, new HeartbeatMessage());
                    } catch (IOException e) {
                        LOGGER.error("Could not send heartbeat");
                        alive.set(false);
                        try {
                            in.close();
                        } catch (IOException e1) {
                            LOGGER.warn("Could not close input stream", e1);
                        }
                        break;
                    }
                    try {
                        Thread.sleep(30_000);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }
        });
        heartbeatThread.setDaemon(true);
        heartbeatThread.start();

        while (!Thread.interrupted() && alive.get()) {
            try {
                Message message = Message.read(in);

                if (message instanceof RenderingStatusMessage) {
                    int spp = ((RenderingStatusMessage) message).getCurrentSpp();
                    int target = ((RenderingStatusMessage) message).getTargetSpp();
                    samplesPerSecond = ((RenderingStatusMessage) message).getSamplesPerSecond();

                    this.previousStatus = this.status;
                    this.status = new RenderingStatus(spp, target);

                    for (RenderListener listener : listeners) {
                        listener.onRenderStatusChanged(spp, target);
                    }
                } else if (message instanceof RenderStatusMessage) {
                    for (RenderListener listener : listeners) {
                        listener.onStatusChanged(((RenderStatusMessage) message).getTask());
                    }
                } else if (message instanceof FinishedMessage) {
                    samplesPerSecond = 0;
                    dump = ((FinishedMessage) message).getDump();
                    finished = true;
                } else if (message instanceof AdditionalDataMessage) {
                    additionalData.putAll(((AdditionalDataMessage) message).getData());
                } else if (message instanceof SetThreadCountMessage) {
                    threadCount = ((SetThreadCountMessage) message).getThreads();
                } else if (message instanceof FailureMessage) {
                    samplesPerSecond = 0;
                    dump = null;
                    finished = true;
                }
            } catch (IOException e) {
                LOGGER.error("Remote renderer disconnected", e);
                parentPool.onDisconnect(this);
                finished = true;
                break;
            }
        }
    }

    void reset() {
        listeners.clear();
        threadCount = 1;
        sceneDirectory = null;
        scene = null;
        dump = null;
        texturepack = null;

        status = new RenderStatus(false, false);
        previousStatus = status;
        samplesPerSecond = 0;
        finished = false;

        pool = null;
    }

    void setPool(RemoteRendererPool pool) {
        this.pool = pool;
    }

    RemoteRendererPool getPool() {
        return pool;
    }
}
