/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.status;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RenderingStatus extends ChunkyStatus {
    private static final Pattern RENDER_PROCESS = Pattern.compile("Rendering: (\\d+[\\.,]\\d+)% \\(([0-9,\\.]+) of ([0-9,\\.]+)\\)( \\[ETA=([0-9:]+)\\])?");

    private int currentSPP;
    private int targetSPP;
    private int eta = -1;

    public RenderingStatus(String line, int current, int target) {
        super(line);

        this.currentSPP = current;
        this.targetSPP = target;
    }

    public RenderingStatus(int current, int target) {
        this("", current, target);
    }

    public RenderingStatus(String line, int current, int target, int eta) {
        this(line, current, target);
        this.eta = eta;
    }

    public int getCurrentSpp() {
        return currentSPP;
    }

    public int getTargetSPP() {
        return targetSPP;
    }

    @Override
    public double getProgressPercentage() {
        return (double) getCurrentSpp() / getTargetSPP() * 100;
    }

    /**
     * Gets the estimated render time left, in seconds.
     *
     * @return int render time left, in secodns
     */
    public int getEta() {
        return this.eta;
    }

    public boolean hasEta() {
        return this.eta >= 0;
    }

    @Override
    public String toString() {
        if (hasEta())
            return String.format("Rendering: %d SPP (%.2f %%), ETA: %d min.", getCurrentSpp(), getProgressPercentage(), getEta() / 60);
        else
            return String.format("Rendering: %f%%", getProgressPercentage());
    }

    public static ChunkyStatus parse(String line) {
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();
        format.setParseBigDecimal(true);

        Matcher m = RENDER_PROCESS.matcher(line);
        if (m.find()) {
            try {
                int currentSps = format.parse(m.group(2)).intValue();
                int targetSps = format.parse(m.group(3)).intValue();

                String eta = m.group(5);
                if (eta != null && !eta.isEmpty()) {
                    String[] parts = eta.split(":");
                    int etaSec = format.parse(parts[0]).intValue() * 60 * 60 + format.parse(parts[1]).intValue() * 60 + format.parse(parts[2]).intValue();
                    return new RenderingStatus(line, currentSps, targetSps, etaSec);
                }

                return new RenderingStatus(line, currentSps, targetSps);
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }
}
