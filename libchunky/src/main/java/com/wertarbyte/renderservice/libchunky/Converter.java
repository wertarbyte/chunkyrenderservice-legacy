/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import com.wertarbyte.renderservice.libchunky.scene.*;
import se.llbit.chunky.renderer.Postprocess;
import se.llbit.chunky.renderer.scene.Sky;
import se.llbit.math.Vector3d;

/**
 * Internal utility class to convert chunky classes to our classes.
 */
class Converter {
    public static Postprocess convert(PostprocessMethod postProcess) {
        switch (postProcess) {
            case GammaCorrection:
                return Postprocess.GAMMA;
            case ToneMap:
                return Postprocess.TONEMAP1;
            case None:
                return Postprocess.NONE;
            default:
                return Postprocess.NONE;
        }
    }

    public static Vector3d convert(Color color) {
        return new Vector3d(color.getRed(), color.getGreen(), color.getBlue());
    }

    public static Sky.SkyMode convert(SkyMode mode) {
        switch (mode) {
            case SIMULATED:
                return Sky.SkyMode.SIMULATED;
            case SKYBOX:
                return Sky.SkyMode.SKYBOX;
            case SKYMAP_PANORAMIC:
                return Sky.SkyMode.SKYMAP_PANORAMIC;
            case SKYMAP_SPHERICAL:
                return Sky.SkyMode.SKYMAP_SPHERICAL;
            default:
                return Sky.SkyMode.SKYMAP_PANORAMIC;
        }
    }

    public static Vector3d convert(Position position) {
        return new Vector3d(position.getX(), position.getY(), position.getZ());
    }

    public static se.llbit.chunky.renderer.projection.ProjectionMode convert(ProjectionMode projectionMode) {
        switch (projectionMode) {
            case PANORAMIC:
                return se.llbit.chunky.renderer.projection.ProjectionMode.PANORAMIC;
            case PINHOLE:
                return se.llbit.chunky.renderer.projection.ProjectionMode.PINHOLE;
            default:
                return se.llbit.chunky.renderer.projection.ProjectionMode.PINHOLE;
        }
    }
}
