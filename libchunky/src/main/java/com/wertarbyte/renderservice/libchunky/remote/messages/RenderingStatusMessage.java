/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Message to send the rendering status.
 */
public class RenderingStatusMessage extends Message {
    private final int currentSpp;
    private final int targetSpp;
    private final double samplesPerSecond;

    RenderingStatusMessage(DataInputStream inputStream) throws IOException {
        currentSpp = inputStream.readInt();
        targetSpp = inputStream.readInt();
        samplesPerSecond = inputStream.readDouble();
    }

    public RenderingStatusMessage(int currentSpp, int targetSpp, double samplesPerSecond) {
        this.currentSpp = currentSpp;
        this.targetSpp = targetSpp;
        this.samplesPerSecond = samplesPerSecond;
    }

    @Override
    protected byte getId() {
        return Message.RENDERING_STATUS;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt(currentSpp);
        outputStream.writeInt(targetSpp);
        outputStream.writeDouble(samplesPerSecond);
    }

    public int getCurrentSpp() {
        return currentSpp;
    }

    public int getTargetSpp() {
        return targetSpp;
    }

    public double getSamplesPerSecond() {
        return samplesPerSecond;
    }
}
