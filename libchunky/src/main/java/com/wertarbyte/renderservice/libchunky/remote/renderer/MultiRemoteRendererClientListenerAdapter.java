/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.renderer;

import com.wertarbyte.renderservice.libchunky.ChunkyTask;

import java.awt.image.BufferedImage;

/**
 * An adapter for {@link MultiRemoteRendererClientListener}.
 */
public abstract class MultiRemoteRendererClientListenerAdapter implements MultiRemoteRendererClientListener {
    @Override
    public void onConnected(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
    }

    @Override
    public void onRenderingStarting(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
    }

    @Override
    public void onRenderingStopped(MultiRemoteRendererClient multiClient, RemoteRendererClient client) {
    }

    @Override
    public void onFinished(MultiRemoteRendererClient multiClient, RemoteRendererClient client, BufferedImage image) {
    }

    @Override
    public void onStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, ChunkyTask task) {
    }

    @Override
    public void onRenderStatusChanged(MultiRemoteRendererClient multiClient, RemoteRendererClient client, int currentSpp, int targetSpp) {
    }

    @Override
    public void onMaxParallelInstancesChanged(MultiRemoteRendererClient multiClient, int newValue) {
    }
}
