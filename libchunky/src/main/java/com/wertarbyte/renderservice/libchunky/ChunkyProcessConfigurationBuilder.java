/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import java.io.File;

/**
 * A builder for {@link ChunkyProcessConfig}s.
 */
public class ChunkyProcessConfigurationBuilder {
    private int jvmMaxMemory;
    private int jvmMinMemory;
    private File chunkyJar;
    private File chunkyLibraryDirectory;
    private File defaultTexturePack;
    private File sceneDirectory;
    private String sceneName;
    private int targetSpp;
    private int threadCount;
    private int dumpFrequency;

    ChunkyProcessConfigurationBuilder() {
    }

    public ChunkyProcessConfigurationBuilder setJvmMaxMemory(int jvmMaxMemory) {
        this.jvmMaxMemory = jvmMaxMemory;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setJvmMinMemory(int jvmMinMemory) {
        this.jvmMinMemory = jvmMinMemory;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setChunkyJar(File chunkyJar) {
        this.chunkyJar = chunkyJar;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setChunkyLibraryDirectory(File chunkyLibraryDirectory) {
        this.chunkyLibraryDirectory = chunkyLibraryDirectory;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setDefaultTexturePack(File texturePack) {
        this.defaultTexturePack = texturePack;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setSceneDirectory(File sceneDirectory) {
        this.sceneDirectory = sceneDirectory;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setSceneName(String sceneName) {
        this.sceneName = sceneName;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setTargetSpp(int targetSpp) {
        this.targetSpp = targetSpp;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setThreadCount(int threadCount) {
        this.threadCount = threadCount;
        return this;
    }

    public ChunkyProcessConfigurationBuilder setDumpFrequency(int dumpFrequency) {
        this.dumpFrequency = dumpFrequency;
        return this;
    }

    public ChunkyProcessConfig build() {
        return new ChunkyProcessConfig(
                jvmMaxMemory, jvmMinMemory,
                chunkyJar, chunkyLibraryDirectory,
                defaultTexturePack,
                sceneDirectory, sceneName,
                targetSpp, threadCount, dumpFrequency);
    }
}
