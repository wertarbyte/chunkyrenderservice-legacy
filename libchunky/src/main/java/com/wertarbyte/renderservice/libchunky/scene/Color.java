/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonObject;

public class Color {
    private final double red;
    private final double green;
    private final double blue;

    public Color(double r, double g, double b) {
        red = r;
        green = g;
        blue = b;
    }

    public Color(String hexColor) {
        if (hexColor.length() == 7) {
            red = Integer.valueOf(hexColor.substring(1, 3), 16) / 255.0;
            green = Integer.valueOf(hexColor.substring(3, 5), 16) / 255.0;
            blue = Integer.valueOf(hexColor.substring(5, 7), 16) / 255.0;
        } else if (hexColor.length() == 4) {
            red = Integer.valueOf(hexColor.charAt(1) + "" + hexColor.charAt(1), 16) / 255.0;
            green = Integer.valueOf(hexColor.charAt(2) + "" + hexColor.charAt(2), 16) / 255.0;
            blue = Integer.valueOf(hexColor.charAt(3) + "" + hexColor.charAt(3), 16) / 255.0;
        } else {
            throw new IllegalArgumentException("Invalid hex color: " + hexColor);
        }
    }

    public double getRed() {
        return red;
    }

    public double getGreen() {
        return green;
    }

    public double getBlue() {
        return blue;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.addProperty("red", getRed());
        json.addProperty("green", getGreen());
        json.addProperty("blue", getBlue());
        return json;
    }

    public static Color fromJson(JsonObject json) {
        return new Color(json.get("red").getAsDouble(),
                json.get("green").getAsDouble(),
                json.get("blue").getAsDouble());
    }
}
