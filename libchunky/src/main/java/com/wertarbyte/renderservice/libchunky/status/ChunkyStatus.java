/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.status;

public abstract class ChunkyStatus extends RenderStatus {
    private String rawLine;

    public abstract double getProgressPercentage();

    public final String getRawLine() {
        return this.rawLine;
    }

    protected ChunkyStatus(String line) {
        super(true, false);
        this.rawLine = line;
    }

    public static ChunkyStatus parse(String line) {
        ChunkyStatus status;

        status = RenderingStatus.parse(line);
        if (status != null)
            return status;

        status = LoadingStatus.parse(line);
        if (status != null)
            return status;

        status = SavingStatus.parse(line);
        if (status != null)
            return status;

        return new UnknownChunkyStatus(line);
    }

    @Override
    public String toString() {
        return getRawLine();
    }
}
