/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import com.google.gson.Gson;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;
import com.wertarbyte.renderservice.libchunky.status.*;
import com.wertarbyte.renderservice.libchunky.util.StringUtil;
import org.apache.commons.math3.util.FastMath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import se.llbit.chunky.main.Chunky;
import se.llbit.j99.sp.PPScanner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A chunky wrapper that uses separate processes.
 */
public class ChunkyProcessWrapper implements ChunkyWrapper {
    private static final Logger LOGGER = LogManager.getLogger(ChunkyProcessWrapper.class);

    private final List<RenderListener> listeners = new CopyOnWriteArrayList<>();
    private Process process;

    private File sceneDirectory;
    private SceneDescription scene;
    private File texturePack;

    private BufferedImage latestImage;
    private ChunkyRenderDump latestDump;

    private RenderStatus status = new RenderStatus(false, false);
    private RenderStatus previousStatus = status;

    //measure sps
    private RenderingStatus lastRenderingStatus;
    private long previousTime;
    private double samplesPerSecond;

    private int targetSpp;
    private int threadCount;
    private int jvmMinMemory = 512;
    private int jvmMaxMemory = 2048;

    public void setSceneDirectory(File sceneDirectory) {
        this.sceneDirectory = sceneDirectory;
    }

    @Override
    public SceneDescription getScene() {
        return scene;
    }

    /**
     * Sets the scene. Note that this may override an existing scene file in the scene directory.
     *
     * @param scene scene
     */
    public void setScene(SceneDescription scene) {
        this.scene = scene;
    }

    @Override
    public void setTexturepack(File texturepack) {
        this.texturePack = texturepack;
    }

    @Override
    public void render() throws IOException {
        try {
            try (FileWriter writer = new FileWriter(new File(sceneDirectory, scene.getName() + ".json"))) {
                (new Gson()).toJson(scene.toJson(), writer);
            }
        } catch (IOException e) {
            throw new IOException("Could not save scene", e);
        }

        List<String> args = new ArrayList<>(Arrays.asList(
                "java",
                "-Xmx" + jvmMaxMemory + "M",
                "-Xms" + jvmMinMemory + "M",
                "-cp", buildChunkyClassPath(),
                ChunkyArgumentFixer.class.getName(),
                "-render", scene.getName(),
                "-scene-dir", sceneDirectory.getAbsolutePath(),
                "-target", String.valueOf(targetSpp),
                "-threads", String.valueOf(threadCount)));

        if (texturePack != null) {
            args.add("-texture");
            args.add(texturePack.getAbsolutePath());

            if (!texturePack.isFile()) {
                throw new IOException("Texturepath doesn't exist: " + texturePack.getAbsolutePath());
            }
        }

        LOGGER.info("Starting chunky: " + StringUtil.join(args, " "));

        process = new ProcessBuilder(args).start();

        final StringBuilder errors = new StringBuilder();
        Thread errorStreamReader = new Thread(new Runnable() {
            @Override
            public void run() {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
                    String line;
                    while (!Thread.currentThread().isInterrupted() && (line = reader.readLine()) != null) {
                        errors.append(line);
                    }
                } catch (IOException e) {
                }
            }
        });
        errorStreamReader.setDaemon(true);
        errorStreamReader.start();

        status = new RenderStatus(true, false);

        for (RenderListener listener : listeners) {
            listener.onStatusChanged(ChunkyTask.LOADING);
        }

        boolean started = false;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String line;
            while (!Thread.currentThread().isInterrupted() && (line = reader.readLine()) != null) {
                previousStatus = status;
                status = ChunkyStatus.parse(line);

                if (status instanceof RenderingStatus) {
                    long time = System.currentTimeMillis();
                    if (lastRenderingStatus != null) {
                        double deltaT = (time - previousTime) / 1000.0;
                        double deltaSpp = ((RenderingStatus) status).getCurrentSpp() - lastRenderingStatus.getCurrentSpp();
                        if (deltaT > 0) {
                            if (deltaSpp > 0) {
                                samplesPerSecond = (deltaSpp * scene.getWidth() * scene.getHeight()) / deltaT;
                                previousTime = time;
                            }
                        }
                    } else {
                        previousTime = time;
                    }
                    this.lastRenderingStatus = (RenderingStatus) status;

                    for (RenderListener listener : listeners) {
                        listener.onRenderStatusChanged(((RenderingStatus) status).getCurrentSpp(),
                                ((RenderingStatus) status).getTargetSPP());
                    }
                } else if (status instanceof LoadingStatus) {
                    started = true;

                    for (RenderListener listener : listeners) {
                        listener.onStatusChanged(ChunkyTask.LOADING);
                    }
                } else if (status instanceof SavingStatus) {
                    for (RenderListener listener : listeners) {
                        listener.onStatusChanged(ChunkyTask.SAVING);
                    }
                } else {
                    for (RenderListener listener : listeners) {
                        listener.onStatusChanged(ChunkyTask.UNKNOWN);
                    }
                }
            }

            if (!started) {
                throw new IOException("Could not start chunky, parameters: " + StringUtil.join(args, " ") + ", errors: " + errors);
            }


            try {
                if (process.waitFor() != 0) {
                    throw new IOException("Chunky exited with code " + process.exitValue() + ", started with parameters: " + StringUtil.join(args, " ") + ", errors: " + errors);
                }
            } catch (InterruptedException e) {
                return;
            }

            try {
                latestImage = ImageIO.read(new File(sceneDirectory, scene.getName() + "-" + targetSpp + ".png"));
            } catch (IOException e) {
                throw new IOException("Could not read rendered image", e);
            }

            try {
                latestDump = new ChunkyRenderDump(new File(sceneDirectory, scene.getName() + ".dump"));
            } catch (IOException e) {
                throw new IOException("Could not read render dump", e);
            }

            for (RenderListener listener : listeners) {
                listener.onFinished();
            }
        }
    }

    @Override
    public RenderStatus getStatus() {
        return status;
    }

    @Override
    public RenderStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * Gets the latest rendering status.
     *
     * @return latest rendering status
     */
    public RenderingStatus getLastRenderingStatus() {
        return lastRenderingStatus;
    }

    private String buildChunkyClassPath() {
        StringBuilder classPath = new StringBuilder();
        // Add the class path of classes in jars that chunky needs:
        for (Class clazz : new Class[]{
                Chunky.class,
                PPScanner.class,
                FastMath.class,
                ChunkyArgumentFixer.class,
                Gson.class
        }) {
            classPath
                    .append(clazz.getProtectionDomain().getCodeSource().getLocation().getFile().replace("%20", " "))
                    .append(File.pathSeparator);
        }
        return classPath.substring(0, classPath.length() - File.pathSeparator.length());
    }

    @Override
    public void stop() {
        if (process != null) {
            process.destroy();
        }
    }

    @Override
    public BufferedImage getImage() {
        return latestImage;
    }

    @Override
    public ChunkyRenderDump getDump() {
        return latestDump;
    }

    @Override
    public double getSamplesPerSecond() {
        return samplesPerSecond;
    }

    @Override
    public void addListener(RenderListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(RenderListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void setTargetSpp(int targetSpp) {
        this.targetSpp = targetSpp;
    }

    @Override
    public int getThreadCount() {
        return threadCount;
    }

    @Override
    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public void setJvmMinMemory(int jvmMinMemory) {
        this.jvmMinMemory = jvmMinMemory;
    }

    public void setJvmMaxMemory(int jvmMaxMemory) {
        this.jvmMaxMemory = jvmMaxMemory;
    }
}
