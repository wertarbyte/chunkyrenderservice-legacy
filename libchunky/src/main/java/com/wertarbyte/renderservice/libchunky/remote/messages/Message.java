/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import java.io.*;

/**
 * A message that is used for communication between the renderer and the server.
 */
public abstract class Message {
    protected static final byte SET_TARGET_SPP = 1;
    protected static final byte SET_THREAD_COUNT = 2;
    protected static final byte SET_SCENE = 3;
    protected static final byte SEND_SCENE_FILES = 4;
    protected static final byte SEND_SKYMAP = 6;
    protected static final byte SEND_TEXTUREPACK = 7;

    protected static final byte RENDERING_STATUS = 42;
    protected static final byte RENDER_STATUS = 43;
    protected static final byte FINISHED = 44;
    protected static final byte FAILURE = 45;

    protected static final byte START = 100;
    protected static final byte STOP = 101;
    protected static final byte WE_ARE_DONE = 102;
    protected static final byte ADDITIONAL_DATA = 103;
    protected static final byte HEARTBEAT = 104;

    public static Message read(DataInputStream inputStream) throws IOException {
        switch (inputStream.readByte()) {
            case SET_TARGET_SPP:
                return new SetTargetSppMessage(inputStream);
            case SET_THREAD_COUNT:
                return new SetThreadCountMessage(inputStream);
            case SET_SCENE:
                return new SceneMessage(inputStream);
            case SEND_SCENE_FILES:
                return new SceneFilesMessage(inputStream);
            case SEND_SKYMAP:
                return new SkymapMessage(inputStream);
            case SEND_TEXTUREPACK:
                return new TexturepackMessage(inputStream);
            case RENDERING_STATUS:
                return new RenderingStatusMessage(inputStream);
            case RENDER_STATUS:
                return new RenderStatusMessage(inputStream);
            case FINISHED:
                return new FinishedMessage(inputStream);
            case FAILURE:
                return new FailureMessage();
            case START:
                return new StartMessage(inputStream);
            case STOP:
                return new StopMessage(inputStream);
            case WE_ARE_DONE:
                return new WeAreDoneMessage();
            case ADDITIONAL_DATA:
                return new AdditionalDataMessage(inputStream);
        }
        return null;
    }

    public static void write(DataOutputStream outputStream, Message... messages) throws IOException {
        for (Message message : messages) {
            outputStream.writeByte(message.getId());
            message.writeTo(outputStream);
        }
        outputStream.flush();
    }

    protected abstract byte getId();

    abstract void writeTo(DataOutputStream outputStream) throws IOException;

    protected static void sendFile(DataOutputStream outputStream, File source) throws IOException {
        outputStream.writeLong(source.length());
        try (FileInputStream fis = new FileInputStream(source);
             BufferedInputStream bis = new BufferedInputStream(fis)) {
            int count;
            byte[] bytes = new byte[4096];
            while ((count = bis.read(bytes)) > 0) {
                outputStream.write(bytes, 0, count);
            }
        }
    }

    protected static void readFile(DataInputStream inputStream, File destination) throws IOException {
        long bytesLeft = inputStream.readLong();
        try (FileOutputStream fos = new FileOutputStream(destination);
             BufferedOutputStream bos = new BufferedOutputStream(fos)) {
            int count;
            byte[] bytes = new byte[4096];
            while ((count = inputStream.read(bytes, 0, (int) Math.min(bytes.length, bytesLeft))) > 0) {
                bos.write(bytes, 0, count);
                bytesLeft -= count;
            }
        }

        if (bytesLeft > 0) {
            throw new IOException("File not received completely");
        }
    }
}
