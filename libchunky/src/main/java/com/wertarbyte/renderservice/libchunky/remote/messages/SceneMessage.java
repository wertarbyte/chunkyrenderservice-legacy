/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.remote.messages;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wertarbyte.renderservice.libchunky.scene.SceneDescription;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Message to send a scene description.
 */
public class SceneMessage extends Message {
    private static Gson gson = new Gson();
    private final SceneDescription scene;

    public SceneMessage(SceneDescription scene) {
        this.scene = scene;
    }

    SceneMessage(DataInputStream inputStream) throws IOException {
        int length = inputStream.readInt();
        byte[] bytes = new byte[length];

        int i = 0;
        int count;
        while ((count = inputStream.read(bytes, i, length - i)) > 0) {
            i += count;
        }

        if (i == length) {
            scene = new SceneDescription(gson.fromJson(new String(bytes, StandardCharsets.UTF_8), JsonObject.class));
        } else {
            throw new IOException("String not read completely");
        }
    }

    @Override
    protected byte getId() {
        return Message.SET_SCENE;
    }

    @Override
    void writeTo(DataOutputStream outputStream) throws IOException {
        String json = gson.toJson(scene.toJson());
        outputStream.writeInt(json.length());
        outputStream.write(json.getBytes(StandardCharsets.UTF_8));
    }

    public SceneDescription getScene() {
        return scene;
    }
}
