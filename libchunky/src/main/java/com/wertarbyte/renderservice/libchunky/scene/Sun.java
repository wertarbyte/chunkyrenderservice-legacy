/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.scene;

import com.google.gson.JsonObject;

public class Sun {
    private double altitude = 1;
    private double azimuth = 6;
    private Color color = new Color(1, 1, 1);
    private double intensity = 1;

    public double getAltitude() {
        return this.altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getAzimuth() {
        return this.azimuth;
    }

    public void setAzimuth(double azimuth) {
        this.azimuth = azimuth;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getIntensity() {
        return this.intensity;
    }

    public void setIntensity(double intensity) {
        this.intensity = intensity;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();

        json.addProperty("altitude", getAltitude());
        json.addProperty("azimuth", getAzimuth());
        json.addProperty("intensity", getIntensity());
        json.add("color", getColor().toJson());

        return json;
    }

    public static Sun fromJson(JsonObject json) {
        Sun sun = new Sun();
        sun.setAltitude(json.get("altitude").getAsDouble());
        sun.setAzimuth(json.get("azimuth").getAsDouble());
        sun.setIntensity(json.get("intensity").getAsDouble());
        sun.setColor(Color.fromJson(json.getAsJsonObject("color")));
        return sun;
    }
}
