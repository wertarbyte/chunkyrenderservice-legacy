/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin.apiclient;

import com.google.gson.reflect.TypeToken;
import com.wertarbyte.renderservice.api.CommonApiClient;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.plugin.RenderServicePlugin;
import com.wertarbyte.renderservice.util.httpapi.JsonApiClient;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.bukkit.Location;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PluginApiClient extends CommonApiClient {
    private List<RenderPreset> presets;

    /**
     * Creates a new render job.
     *
     * @param player   nickname of the player who creates the job
     * @param location location of the player's eyes
     * @return a creator class that is used to add more parameters and submit the job
     * @throws java.io.IOException if initializing the job creator fails
     */
    public PictureJobCreator createJob(String player, Location location) throws IOException {
        PictureJobCreator job = new PictureJobCreator(this);

        int regionX = location.getBlockX() / 16 / 32;
        int regionZ = location.getBlockZ() / 16 / 32;
        for (int x = regionX - 1; x <= regionX + 1; x++) {
            for (int z = regionZ - 1; z <= regionZ + 1; z++) {
                job.addRegionFile(new File(location.getWorld().getWorldFolder(), "region" + File.separatorChar + "r." + x + "." + z + ".mca"));
            }
        }

        job.addLevelDat(new File(location.getWorld().getWorldFolder(), "level.dat"));

        job.setCreator(player);
        job.setLocation(location);
        job.setChunkRadius(32);

        return job;
    }

    /**
     * Creates a new video render job.
     *
     * @param player   nickname of the player who creates the job
     * @param location location of the player's eyes
     * @return a creator class that is used to add more parameters and submit the job
     * @throws java.io.IOException if initializing the job creator fails
     */
    public VideoJobCreator createVideoJob(String player, Location location) throws IOException {
        VideoJobCreator job = new VideoJobCreator(this);

        int regionX = location.getBlockX() / 16 / 32;
        int regionZ = location.getBlockZ() / 16 / 32;
        for (int x = regionX - 1; x <= regionX + 1; x++) {
            for (int z = regionZ - 1; z <= regionZ + 1; z++) {
                job.addRegionFile(new File(location.getWorld().getWorldFolder(), "region" + File.separatorChar + "r." + x + "." + z + ".mca"));
            }
        }

        job.addLevelDat(new File(location.getWorld().getWorldFolder(), "level.dat"));

        job.setCreator(player);
        job.setLocation(location);
        job.setChunkRadius(32);
        job.setType(RenderJobType.VIDEO);

        return job;
    }

    JsonApiClient getClient() {
        return client;
    }


    @Override
    protected String getUserAgent() {
        return "RenderService Plugin v" + RenderServicePlugin.VERSION;
    }

    public List<RenderPreset> getPresets() throws ApiClientException {
        if (presets == null) {
            presets = this.getClient().getFromJson("/presets", new TypeToken<ArrayList<RenderPreset>>() {
            }.getType());
        }
        return presets;
    }
}
