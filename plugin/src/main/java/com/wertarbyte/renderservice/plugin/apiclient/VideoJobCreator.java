/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin.apiclient;

import com.wertarbyte.renderservice.api.Keyframe;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.api.VideoJobConfiguration;
import com.wertarbyte.renderservice.util.video.InterpolatedFrames;
import com.wertarbyte.renderservice.util.video.KeyframeList;
import org.bukkit.Location;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Creator for video render jobs.
 */
public class VideoJobCreator extends JobCreator {
    private List<Keyframe> keyframes = new ArrayList<>();

    VideoJobCreator(PluginApiClient pluginApiClient) throws IOException {
        super(pluginApiClient);
        setType(RenderJobType.VIDEO);
    }

    @Override
    public void setType(RenderJobType type) {
        if (type != RenderJobType.VIDEO && type != RenderJobType.SPHERICAL_VIDEO) {
            throw new IllegalArgumentException(type + " is not a video type");
        }
        super.setType(type);
    }

    public void addKeyframe(Location location, double time) {
        keyframes.add(new Keyframe(time,
                location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch()));
    }

    @Override
    protected VideoJobConfiguration getJobConfiguration() {
        return VideoJobConfiguration.fromJobConfiguration(super.getJobConfiguration(), keyframes);
    }

    public InterpolatedFrames getInterpolatedKeyframes() {
        return new KeyframeList(keyframes).interpolate();
    }
}
