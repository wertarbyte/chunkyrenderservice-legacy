/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin;

import com.wertarbyte.renderservice.api.JobCreationHandler;
import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.plugin.apiclient.PluginApiClient;
import com.wertarbyte.renderservice.plugin.apiclient.VideoJobCreator;
import com.wertarbyte.renderservice.util.video.InterpolatedFrames;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class VideoCreationHandler implements CommandExecutor, Listener {
    private final RenderServicePlugin plugin;
    private final PluginApiClient api;
    private Map<Player, VideoJobCreator> videoCreators = new HashMap<>();

    public VideoCreationHandler(RenderServicePlugin plugin, PluginApiClient api) {
        this.plugin = plugin;
        this.api = api;

        plugin.getCommand("video").setExecutor(this);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            final Player player = (Player) commandSender;

            if (args.length == 2) {
                if (args[0].equals("key")) {
                    addKeyframe(player, Double.valueOf(args[1]));
                    return true;
                }
                return false;
            } else if (args.length >= 1) {
                switch (args[0]) {
                    case "finish":
                        if (!plugin.handlePayment(player, plugin.getRenderPrice(player))) {
                            return true;
                        }
                        submitVideo(player, args[1],
                                Arrays.asList(args).contains("sphere") ? RenderJobType.SPHERICAL_VIDEO : RenderJobType.VIDEO,
                                new JobCreationHandler() {
                                    @Override
                                    public void onSuccess(RenderJob job) {
                                        plugin.onJobCreated(player, job);
                                    }

                                    @Override
                                    public void onFailure() {
                                        plugin.onJobCreationFailed(player);
                                    }
                                });
                        return true;
                    case "play":
                        play(player);
                        return true;
                    case "cancel":
                        cancel(player);
                        RenderServicePlugin.prefix().append("Video creation canceled").sendTo(player);
                        return true;
                    default:
                        return false;
                }
            } else {
                //cancel creation of previous video (if any)
                cancel(player);

                try {
                    start(player);
                    RenderServicePlugin.prefix().append("Video started. Use ").append("/video key <time>").darkAqua()
                            .append(" to add keyframes and ").append("/video finish <preset> [sphere]").darkAqua()
                            .append(" to create the video. Use ").append("/video play").darkAqua().append(" to play a preview.")
                            .sendTo(player);
                } catch (IOException e) {
                    plugin.getLogger().log(Level.SEVERE, "Could not start the video", e);
                    RenderServicePlugin.prefix().append("The video could not be started. Please try again.").red().sendTo(player);
                }
                return true;
            }
        }

        return false;
    }

    public void start(Player player) throws IOException {
        VideoJobCreator creator = api.createVideoJob(player.getName(), player.getLocation().add(0, 1.62, 0));
        videoCreators.put(player, creator);
    }

    public void cancel(Player player) {
        VideoJobCreator creator = videoCreators.get(player);
        if (creator != null) {
            creator.cancel();
        }
    }

    public void play(final Player player) {
        VideoJobCreator video = videoCreators.get(player);
        if (video != null) {
            final InterpolatedFrames keyframes = video.getInterpolatedKeyframes();

            for (double t = 0; t <= keyframes.getLength(); t += 0.1) {
                final double tCopy = t;
                Bukkit.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.teleport(new Location(player.getWorld(),
                                keyframes.getX(tCopy),
                                keyframes.getY(tCopy) - 1.26, //substract eye height (teleport location is the foot position)
                                keyframes.getZ(tCopy),
                                (float) keyframes.getYaw(tCopy),
                                (float) keyframes.getPitch(tCopy)));
                    }
                }, (long) (t * 20));
            }
        }
    }

    public void addKeyframe(Player player, double time) {
        VideoJobCreator videoCreator = videoCreators.get(player);
        if (videoCreator != null) {
            videoCreator.addKeyframe(player.getLocation().add(0, 1.62, 0), time); //add eye height (player location is the foot position)
        }
    }

    public void submitVideo(Player player, String preset, RenderJobType type, final JobCreationHandler handler) {
        player.getWorld().save();
        final VideoJobCreator jobCreator = videoCreators.remove(player);
        if (jobCreator != null) {
            jobCreator.setPreset(preset);
            jobCreator.setType(type);

            Bukkit.getServer().getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                @Override
                public void run() {
                    try {
                        final RenderJob job = jobCreator.submit();
                        Bukkit.getServer().getScheduler().runTask(plugin, new Runnable() {
                            @Override
                            public void run() {
                                handler.onSuccess(job);
                            }
                        });
                    } catch (Exception e) {
                        plugin.getLogger().log(Level.SEVERE, "Could not create job", e);
                        Bukkit.getServer().getScheduler().runTask(plugin, new Runnable() {
                            @Override
                            public void run() {
                                handler.onFailure();
                            }
                        });
                    }
                }
            });
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (!event.getFrom().getWorld().equals(event.getTo().getWorld())) {
            cancel(event.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        cancel(event.getPlayer());
    }

    public void onDisable() {
        //cancel all pending video job creators
        for (Map.Entry<Player, VideoJobCreator> creator : videoCreators.entrySet()) {
            creator.getValue().cancel();
        }

        videoCreators.clear();
    }
}
