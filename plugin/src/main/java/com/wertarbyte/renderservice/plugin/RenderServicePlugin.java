/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin;

import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.plugin.apiclient.PluginApiClient;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import de.craften.plugins.mcguilib.text.TextBuilder;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class RenderServicePlugin extends JavaPlugin {
    /**
     * The version of this plugin that is reported to the master server. The version required by the master server
     * increases on breaking changes or when new features are implemented.
     */
    public static final int VERSION = 1;
    private static Economy economy;

    private PluginApiClient api;
    private VideoCreationHandler videoCreationHandler;
    private PictureCreationHandler pictureCreationHandler;

    @Override
    public void onEnable() {
        // Create empty config file if it doesn't exist
        saveDefaultConfig();

        if (getConfig().getDouble("renderprice", 0) > 0 && !setupEconomy()) {
            getLogger().warning("Vault not found but render price is set. Plugin will be disabled, fix your config!");
            getPluginLoader().disablePlugin(this);
        }

        api = new PluginApiClient();
        videoCreationHandler = new VideoCreationHandler(this, api);
        pictureCreationHandler = new PictureCreationHandler(this, api);
    }

    @Override
    public void onDisable() {
        videoCreationHandler.onDisable();
        pictureCreationHandler.onDisable();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, final String[] args) {
        if (command.getName().equals("rs")) {
            if (args.length == 1 && args[0].equalsIgnoreCase("reload")
                    && sender.hasPermission("renderservice.reload")) {
                reload();
                prefix().append("Configuration reloaded.").sendTo(sender);
                return true;
            } else if (args[0].equalsIgnoreCase("presets")) {
                try {
                    TextBuilder text = prefix().append("Available presets: ");
                    boolean first = true;
                    for (RenderPreset preset : api.getPresets()) {
                        if (!first) {
                            text.append(", ");
                        }
                        first = false;
                        text.append(preset.getName());
                    }
                    text.sendTo(sender);
                } catch (ApiClientException e) {
                    prefix().append("The available presets could not be retrieved.").red().sendTo(sender);
                }
            }
        }

        return false;
    }

    /**
     * Withdraws the given amount of money from the given player. This method also sends messages about the transaction
     * to the player.
     *
     * @param player player
     * @param amount amount of money
     * @return true if the transaction was successful (or not needed), false if not
     */
    public boolean handlePayment(Player player, double amount) {
        if (hasEconomy() && amount > 0) {
            prefix().append("That's ").append(economy.format(amount)).darkAqua().append(", please!").sendTo(player);
            EconomyResponse payment = economy.withdrawPlayer(player, amount);
            if (payment.transactionSuccess()) {
                return true;
            } else {
                prefix().append("Payment failed. ").red().append(payment.errorMessage).red().sendTo(player);
                return false;
            }
        } else {
            return true;
        }
    }

    void onJobCreated(Player player, RenderJob job) {
        prefix().append("Your render job was successfully created! See it at ")
                .append(job.getUrl()).darkAqua().sendTo(player);

        getServer().broadcast(prefix().append(String.format("%s created a new render job (#%d).",
                player.getDisplayName(), job.getId())).getSingleLine(), "renderservice.notify");
    }

    void onJobCreationFailed(Player player) {
        double pricePayed = getRenderPrice(player);

        if (hasEconomy() && pricePayed > 0) {
            economy.depositPlayer(player, pricePayed);
            prefix().append("Sorry, an error occured while creating your render job.").red()
                    .append("The payment was reversed.").red().sendTo(player);
        } else {
            prefix().append("Sorry, an error occured while creating your render job.").red().sendTo(player);
        }
    }

    private void reload() {
        reloadConfig();
        api = new PluginApiClient();
    }

    private boolean setupEconomy() {
        try {
            RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
            if (economyProvider != null) {
                economy = economyProvider.getProvider();
            }
            return (economy != null);
        } catch (NoClassDefFoundError e) {
            return false;
        }
    }

    private boolean hasEconomy() {
        return (economy != null);
    }

    public double getRenderPrice(Player player) {
        if (!hasEconomy() || player.hasPermission("renderservice.free")) {
            return 0;
        }
        return getConfig().getDouble("renderprice");
    }

    public static TextBuilder prefix() {
        return TextBuilder.create().append("[").white().append("RenderService").darkAqua().append("] ").white();
    }
}
