/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin.apiclient;

import com.wertarbyte.renderservice.api.RenderJobType;

import java.io.IOException;

/**
 * Creator for picture render jobs.
 */
public class PictureJobCreator extends JobCreator {
    PictureJobCreator(PluginApiClient pluginApiClient) throws IOException {
        super(pluginApiClient);
        setType(RenderJobType.PICTURE);
    }

    @Override
    public void setType(RenderJobType type) {
        if (type != RenderJobType.SPHERE && type != RenderJobType.PICTURE && type != RenderJobType.STEREO_PICTURE) {
            throw new IllegalArgumentException(type + " is not a picture type");
        }
        super.setType(type);
    }

    @Override
    public void setFocusDistance(double distance) {
        super.setFocusDistance(distance);
    }
}
