/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin;

import com.google.common.collect.Lists;
import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.plugin.apiclient.PictureJobCreator;
import com.wertarbyte.renderservice.plugin.apiclient.PluginApiClient;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

public class PictureCreationHandler implements CommandExecutor, Listener {
    private final RenderServicePlugin plugin;
    private final PluginApiClient api;
    private Set<Player> focusingPlayers = new HashSet<>();
    private Map<Player, Vector> focusPoints = new HashMap<>();

    public PictureCreationHandler(RenderServicePlugin plugin, PluginApiClient api) {
        this.plugin = plugin;
        this.api = api;

        plugin.getCommand("render").setExecutor(this);
        plugin.getCommand("focus").setExecutor(this);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if (command.getName().equals("render")) {
                int maxJobs = plugin.getConfig().getInt("maxJobs");
                int jobsByPlayer;
                try {
                    jobsByPlayer = api.getPlayerData(player.getName()).getCurrentJobsCount();
                } catch (ApiClientException e) {
                    plugin.getLogger().log(Level.SEVERE, "Could not create job", e);
                    RenderServicePlugin.prefix().append("An error occured while creating your render job.").red().sendTo(player);
                    return true;
                }
                if ((jobsByPlayer >= 0 && jobsByPlayer < maxJobs)
                        || player.hasPermission("renderservice.bypasslimit")) {
                    if (!plugin.handlePayment(player, plugin.getRenderPrice(player))) {
                        return true;
                    }

                    String preset = args.length >= 1 ? args[0] : "";
                    RenderJobType type = RenderJobType.PICTURE;
                    if (Lists.newArrayList(args).contains("3d")) {
                        type = RenderJobType.STEREO_PICTURE;
                    } else if (Lists.newArrayList(args).contains("sphere")) {
                        type = RenderJobType.SPHERE;
                    }
                    createPictureJob(player, player.getLocation().add(0, 1.62, 0), preset, getFocalOffset(player), type);

                } else if (jobsByPlayer >= maxJobs) {
                    RenderServicePlugin.prefix().append("Sorry, you already have ")
                            .append(jobsByPlayer + "").append("pending renders and need to wait for them to complete before you may start a new one.")
                            .sendTo(player);
                }
                if (focusPoints.remove(player) != null) {
                    RenderServicePlugin.prefix().append("Focus point was reset.").sendTo(player);
                }
                return true;
            } else if (command.getName().equals("focus")) {
                focusingPlayers.add(player);
                RenderServicePlugin.prefix().append("Right-click on a block to set the focus point.").sendTo(player);
                return true;
            }
        }

        return false;
    }

    private void createPictureJob(final Player player, final Location location, final String preset, final Double focalOffset, final RenderJobType type) {
        location.getWorld().save();
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PictureJobCreator jobCreator = api.createJob(player.getName(), location);
                    if (focalOffset != null) {
                        jobCreator.setFocusDistance(focalOffset);
                    }
                    jobCreator.setPreset(preset);
                    jobCreator.setType(type);
                    final RenderJob job = jobCreator.submit();

                    plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            plugin.onJobCreated(player, job);
                        }
                    });
                } catch (Exception e) {
                    plugin.getLogger().log(Level.SEVERE, "Could not create job", e);
                    plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            plugin.onJobCreationFailed(player);
                        }
                    });
                }
            }
        });
    }

    @EventHandler
    public void onBlockClick(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player player = event.getPlayer();
            if (focusingPlayers.contains(player)) {
                Vector focus = event.getClickedBlock().getLocation().toVector();
                focusPoints.put(player, focus);
                focusingPlayers.remove(player);

                RenderServicePlugin.prefix().append("Focus point set to ")
                        .append(focus.toString()).darkAqua()
                        .sendTo(player);
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (!event.getFrom().getWorld().equals(event.getTo().getWorld())) {
            focusingPlayers.remove(event.getPlayer());
            focusPoints.remove(event.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        focusingPlayers.remove(event.getPlayer());
        focusPoints.remove(event.getPlayer());
    }

    private Vector getFocusVector(Player player) {
        return focusPoints.get(player);
    }

    /**
     * Calculates the focal offset of a player. That is the distance between the layer that is parallel to the camera
     * layer and contains the focused point. If focus is not set, null is returned.
     *
     * @param player Player to calculate the focal offset for
     * @return Focal offset, or null if the player didn't set the focus
     */
    public Double getFocalOffset(Player player) {
        Vector focusPoint = getFocusVector(player);
        if (focusPoint == null)
            return null;

        Vector camDirection = player.getLocation().getDirection();

        //layer E: camDirection * (x - focusPoint) = 0
        // <=>  E: camDirection * x = camDirection * focusPoint =: a
        double a = camDirection.dot(focusPoint);

        //calculate the distance between the camera and the focused layer
        return Math.abs((player.getLocation().add(0, 1.62, 0).toVector().dot(camDirection) - a) / camDirection.length());
    }

    public void onDisable() {
        focusingPlayers.clear();
        focusPoints.clear();
    }
}
