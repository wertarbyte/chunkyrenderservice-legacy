/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.plugin.apiclient;

import com.google.gson.Gson;
import com.wertarbyte.renderservice.api.JobConfiguration;
import com.wertarbyte.renderservice.api.RenderJob;
import com.wertarbyte.renderservice.api.RenderJobType;
import com.wertarbyte.renderservice.plugin.RenderServicePlugin;
import com.wertarbyte.renderservice.plugin.exceptions.CreateJobException;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.bukkit.Location;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarOutputStream;

import java.io.*;
import java.nio.charset.Charset;
import java.util.zip.GZIPOutputStream;

/**
 * Creator for render jobs.
 */
abstract class JobCreator {
    private PluginApiClient pluginApiClient;
    private String creator;
    private String world;
    private double x;
    private double y;
    private double z;
    private double pitch;
    private double yaw;
    private double focusDistance;
    private boolean hasFocus = false;
    private RenderJobType type = RenderJobType.PICTURE;
    private int chunkRadius;
    private String preset;
    private File chunkTarFile;
    private TarOutputStream chunkTarGzStream;

    JobCreator(PluginApiClient pluginApiClient) throws IOException {
        this.pluginApiClient = pluginApiClient;
        chunkTarFile = File.createTempFile("renderservice", "chunks");
        chunkTarGzStream = new TarOutputStream(new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(chunkTarFile))));
    }

    /**
     * Submits this job.
     *
     * @return the created job
     * @throws com.wertarbyte.renderservice.plugin.exceptions.CreateJobException       if creating the job fails for a client-side, not api-related reason
     * @throws com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException if an api error occures or the job can't be created due to the api
     */
    public RenderJob submit() throws CreateJobException, ApiClientException {
        Gson gson = new Gson();
        try {
            chunkTarGzStream.flush();
            chunkTarGzStream.close();
        } catch (IOException e) {
            //ignore
        }

        JobConfiguration config = getJobConfiguration();

        HttpEntity httpEntity = MultipartEntityBuilder.create()
                .addBinaryBody("regions", chunkTarFile, ContentType.create("application/gzip"), chunkTarFile.getName())
                .setCharset(Charset.forName("UTF-8"))
                .addTextBody("data", gson.toJson(config))
                .build();

        try {
            HttpResponse response = pluginApiClient.getClient().post("/jobs", httpEntity, 10000);
            if (!chunkTarFile.delete()) {
                RenderServicePlugin.getPlugin(RenderServicePlugin.class).getLogger().warning("Could not delete temporary file.");
            }
            HttpEntity respEntity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == 200 && respEntity != null) {
                return gson.fromJson(new InputStreamReader(respEntity.getContent()), RenderJob.class);
            } else {
                if (respEntity != null)
                    throw new ApiClientException(EntityUtils.toString(respEntity, "UTF-8"), response.getStatusLine().getStatusCode());
                else
                    throw new ApiClientException(response.getStatusLine().getReasonPhrase(), response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            throw new CreateJobException(e);
        } finally {
            if (!chunkTarFile.delete()) {
                RenderServicePlugin.getPlugin(RenderServicePlugin.class).getLogger().warning("Could not delete temporary file.");
            }
        }
    }

    /**
     * Cancels the creation of this job.
     */
    public void cancel() {
        try {
            chunkTarGzStream.close();
        } catch (IOException e) {
            //ignore
        }
        if (!chunkTarFile.delete()) {
            RenderServicePlugin.getPlugin(RenderServicePlugin.class).getLogger().warning("Could not delete temporary file.");
        }
    }

    protected JobConfiguration getJobConfiguration() {
        return new JobConfiguration(creator, world, x, y, z, pitch, yaw, chunkRadius,
                hasFocus ? focusDistance : null, preset, type);
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setLocation(Location location) {
        this.world = location.getWorld().getName();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
    }

    public void setChunkRadius(int chunkRadius) {
        this.chunkRadius = chunkRadius;
    }

    public void setPreset(String preset) {
        this.preset = preset;
    }

    protected void setFocusDistance(double distance) {
        hasFocus = true;
        focusDistance = distance;
    }

    public void setType(RenderJobType type) {
        this.type = type;
    }

    void addLevelDat(File file) throws IOException {
        //It's okay to throw if the file doesn't exist. Job without level.dat? Mon dieu!
        addFileToTar(chunkTarGzStream, file, "level.dat");
    }

    void addRegionFile(File file) throws IOException {
        if (file.exists())
            addFileToTar(chunkTarGzStream, file, "region/" + file.getName());
    }

    private void addFileToTar(TarOutputStream tar, File file, String name) throws IOException {
        tar.putNextEntry(new TarEntry(file, name));
        BufferedInputStream origin = new BufferedInputStream(new FileInputStream(file));
        int count;
        byte data[] = new byte[2084];
        while ((count = origin.read(data)) != -1) {
            tar.write(data, 0, count);
        }
        tar.flush();
        origin.close();
    }
}
