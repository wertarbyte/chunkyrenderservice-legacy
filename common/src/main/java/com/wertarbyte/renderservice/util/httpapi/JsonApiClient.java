/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util.httpapi;

import com.google.gson.Gson;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;

public class JsonApiClient {
    /**
     * Root URL of the API.
     */
    private final String API;

    /**
     * Creates a new API client.
     *
     * @param apiUrl the root URL of the API
     */
    public JsonApiClient(String apiUrl) {
        API = apiUrl;
    }

    protected void requestHook(HttpRequestBase request) {
    }

    /**
     * Sends a GET request to the given API address and returns the response, regardless of the status code.
     * <p/>
     * This method is used by all other get methods of this class.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param timeout    Timeout for request and connection in milliseconds
     * @param parameters GET parameters to send
     * @return Result of the API call
     * @throws ApiClientException if the request fails
     */
    protected HttpResponse getResponse(String apiAddress, int timeout, String... parameters) throws ApiClientException {
        HttpClient httpClient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(timeout)
                .setConnectTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();

        URI uri;
        try {
            URIBuilder builder = new URIBuilder(API + apiAddress);
            for (int i = 0; i < parameters.length / 2; i++) {
                builder.setParameter(parameters[2 * i], parameters[2 * i + 1]);
            }
            uri = builder.build();
        } catch (URISyntaxException e) {
            throw new ApiClientException("Could not build URI", e);
        }

        HttpGet httpGet = new HttpGet(uri);
        httpGet.setConfig(requestConfig);
        requestHook(httpGet);

        try {
            return httpClient.execute(httpGet);
        } catch (IOException e) {
            throw new ApiClientException("Connection error", e);
        }
    }

    /**
     * Sends a GET request to the given API address and returns the HttpResponse.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param timeout    Timeout for request and connection in milliseconds
     * @param parameters GET parameters to send
     * @return Result of the API call
     * @throws ApiClientException If the request failed or the status code isn't 2xx
     */
    public HttpResponse get(String apiAddress, int timeout, String... parameters) throws ApiClientException {
        HttpResponse response = getResponse(apiAddress, timeout, parameters);

        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode >= 200 && statusCode <= 299) {
            return response;
        } else {
            throw new ApiClientException("GET request to " + apiAddress + " failed", statusCode);
        }
    }

    /**
     * Sends a GET request to the given API address and returns the HttpResponse.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param parameters GET parameters to send
     * @return Result of the API call
     * @throws ApiClientException If the request failed
     */
    public HttpResponse get(String apiAddress, String... parameters) throws ApiClientException {
        return get(apiAddress, 2000, parameters);
    }

    /**
     * Sends a GET request to the given address and converts the JSON result to the given output type.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param type       Type the result should be converted to
     * @param timeout    Timeout for request and connection in milliseconds
     * @param parameters GET parameters to send
     * @param <T>        Type the result should be converted to
     * @return Result of the API call, converted to an instance of the given type
     * @throws com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException If the request or the JSON conversion failed.
     */
    public <T> T getFromJson(String apiAddress, Class<T> type, int timeout, String... parameters) throws ApiClientException {
        HttpResponse response = getResponse(apiAddress, timeout, parameters);
        HttpEntity respEntity = response.getEntity();
        if (response.getStatusLine().getStatusCode() == 200 && respEntity != null) {
            try {
                return (new Gson()).fromJson(new InputStreamReader(respEntity.getContent()), type);
            } catch (IOException e) {
                throw new ApiClientException("Connection error", e);
            }
        } else {
            throw new ApiClientException("GET request to " + apiAddress + " failed", response.getStatusLine().getStatusCode());
        }
    }

    /**
     * Sends a GET request to the given address and converts the JSON result to the given output type.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param type       Type the result should be converted to
     * @param parameters GET parameters to send
     * @param <T>        Type the result should be converted to
     * @return Result of the API call, converted to an instance of the given type
     * @throws com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException If the request or the JSON conversion failed.
     */
    public <T> T getFromJson(String apiAddress, Class<T> type, String... parameters) throws ApiClientException {
        return getFromJson(apiAddress, type, 2000, parameters);
    }

    /**
     * Sends a GET request to the given address and converts the JSON result to the given output type.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param type       Type the result should be converted to
     * @param timeout    Timeout for request and connection in milliseconds
     * @param parameters GET parameters to send
     * @param <T>        Type the result should be converted to
     * @return Result of the API call, converted to an instance of the given type
     * @throws ApiClientException If the request or the JSON conversion failed.
     */
    public <T> T getFromJson(String apiAddress, Type type, int timeout, String... parameters) throws ApiClientException {
        HttpResponse response = getResponse(apiAddress, timeout, parameters);
        HttpEntity respEntity = response.getEntity();
        if (response.getStatusLine().getStatusCode() == 200 && respEntity != null) {
            try {
                return (new Gson()).fromJson(new InputStreamReader(respEntity.getContent()), type);
            } catch (IOException e) {
                throw new ApiClientException("Connection error", e);
            }
        } else {
            throw new ApiClientException("GET request to " + apiAddress + " failed", response.getStatusLine().getStatusCode());
        }
    }

    /**
     * Sends a GET request to the given address and converts the JSON result to the given output type.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param type       Type the result should be converted to
     * @param parameters GET parameters to send
     * @param <T>        Type the result should be converted to
     * @return Result of the API call, converted to an instance of the given type
     * @throws ApiClientException If the request or the JSON conversion failed.
     */
    public <T> T getFromJson(String apiAddress, Type type, String... parameters) throws ApiClientException {
        return getFromJson(apiAddress, type, 2000, parameters);
    }

    /**
     * Sends a GET request to the given API address and returns the result as a stream.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param timeout    Timeout for request and connection in milliseconds
     * @param parameters GET parameters to send
     * @return Result of the API call
     * @throws ApiClientException If the request failed
     */
    public InputStream getAsStream(String apiAddress, int timeout, String... parameters) throws ApiClientException {
        HttpResponse response = get(apiAddress, timeout, parameters);

        HttpEntity respEntity = response.getEntity();
        if (respEntity != null) {
            try {
                return respEntity.getContent();
            } catch (IOException e) {
                throw new ApiClientException("Connection error", e);
            }
        } else {
            throw new ApiClientException("Empty response", response.getStatusLine().getStatusCode());
        }
    }

    /**
     * Sends a GET request to the given API address and returns the result as a stream.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param parameters GET parameters to send
     * @return Result of the API call
     * @throws ApiClientException If the request failed
     */
    public InputStream getAsStream(String apiAddress, String... parameters) throws ApiClientException {
        return getAsStream(apiAddress, 2000, parameters);
    }

    /**
     * Sends a PUT request to the given API address and returns the response.
     * <p/>
     * This method is used by all other put methods in this class.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param data       data to submit using form/multipart encoding
     * @param timeout    Timeout for request and connection in milliseconds
     * @return Response of the request if it succeeded
     * @throws ApiClientException If the request failed
     */
    public HttpResponse put(String apiAddress, HttpEntity data, int timeout) throws ApiClientException {
        HttpClient httpClient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(timeout)
                .setConnectTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();

        HttpPut httpPut = new HttpPut(API + apiAddress);
        httpPut.setConfig(requestConfig);
        requestHook(httpPut);

        if (data != null)
            httpPut.setEntity(data);

        HttpResponse response;
        try {
            response = httpClient.execute(httpPut);
        } catch (IOException e) {
            throw new ApiClientException("connection error", e);
        }

        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode >= 200 && statusCode <= 299) {
            return response;
        } else {
            throw new ApiClientException("Request failed", statusCode);
        }
    }

    /**
     * Sends a PUT request to the given API address and returns the result as a String.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param data       data to submit using form/multipart encoding
     * @return Response of the request if it succeeded
     * @throws ApiClientException If the request failed
     */
    public HttpResponse put(String apiAddress, HttpEntity data) throws ApiClientException {
        return put(apiAddress, data, 2000);
    }

    /**
     * Sends a POST request to the given API address and returns the result as a String.
     * <p/>
     * This method is used by all other post methods in this class.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param data       data to submit using form/multipart encoding
     * @param timeout    Timeout for request and connection in milliseconds
     * @return Response of the request if it succeeded
     * @throws ApiClientException If the request failed
     */
    public HttpResponse post(String apiAddress, HttpEntity data, int timeout) throws ApiClientException {
        HttpClient httpClient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(timeout)
                .setConnectTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();

        HttpPost httpPost = new HttpPost(API + apiAddress);
        httpPost.setConfig(requestConfig);
        requestHook(httpPost);

        if (data != null)
            httpPost.setEntity(data);

        try {
            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 299) {
                return response;
            } else {
                throw new ApiClientException(response.getStatusLine().getReasonPhrase(), response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            throw new ApiClientException("connection error", e);
        }
    }

    /**
     * Sends a POST request to the given API address and returns the result as a String.
     *
     * @param apiAddress API address to call (starting with a slash)
     * @param data       data to submit using form/multipart encoding
     * @return Response of the request if it succeeded
     * @throws ApiClientException If the request failed
     */
    public HttpResponse post(String apiAddress, HttpEntity data) throws ApiClientException {
        return post(apiAddress, data, 2000);
    }
}
