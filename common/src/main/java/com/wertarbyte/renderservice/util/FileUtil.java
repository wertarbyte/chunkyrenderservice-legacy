/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util;

import java.io.*;

public class FileUtil {
    /**
     * Copies a file. This may overwrite the target file.
     *
     * @param source file to copy
     * @param dest   target file
     * @throws IOException if the source file doesn't exist or another error occurred
     */
    public static void copy(File source, File dest) throws IOException {
        move(new FileInputStream(source), new FileOutputStream(dest));
    }

    /**
     * Writes the given stream into a file.
     *
     * @param source stream to write down
     * @param dest   target file
     * @throws IOException if an error occured while writing the file or reading the stream
     */
    public static void writeFile(InputStream source, File dest) throws IOException {
        move(source, new FileOutputStream(dest));
    }

    public static void readFile(File source, OutputStream dest) throws IOException {
        move(new FileInputStream(source), dest);
    }

    /**
     * Reads the given file into the given stream.
     *
     * @param source file to read
     * @param dest   stream to write into
     * @throws IOException if an error occured while writing in the stream or reading the file
     */
    private static void move(InputStream source, OutputStream dest) throws IOException {
        try {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = source.read(buffer)) > 0) {
                dest.write(buffer, 0, length);
            }
        } finally {
            if (source != null)
                source.close();
            if (dest != null)
                dest.close();
        }
    }

    /**
     * Copies the given path to the given destination.
     *
     * @param src  Path to copy
     * @param dest Destination path
     * @throws IOException
     */
    public static void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {
            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile, destFile);
            }
        } else {
            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
        }
    }

    /**
     * Deletes a directory recursively.
     *
     * @param dir directory to delete
     * @return true if the directory was successfully deleted, false if not
     */
    public static boolean deleteDirectory(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDirectory(new File(dir, aChildren));
                if (!success) {
                    return false;
                }
            }
        }
        
        return dir.delete();
    }
}
