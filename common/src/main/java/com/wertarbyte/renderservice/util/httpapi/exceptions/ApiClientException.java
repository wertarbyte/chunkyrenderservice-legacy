/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util.httpapi.exceptions;

/**
 * An exception that is thrown when an error occures while using the API.
 */
public class ApiClientException extends ApiException {
    /**
     * Creates a new instance of this exception for a client side error.
     *
     * @param message a message to describe this exception
     * @param cause   exception that caused this exception
     */
    public ApiClientException(String message, Exception cause) {
        super(message, cause, false);
    }

    /**
     * Creates a new instance of this exception.
     *
     * @param message    A message to A message to describe this exception
     * @param statusCode The HTTP status code that the API returned
     */
    public ApiClientException(String message, int statusCode) {
        super(statusCode + " " + message, statusCode >= 500 && statusCode <= 599);
    }

    protected ApiClientException(String message, boolean isServerSide) {
        super(message, isServerSide);
    }
}
