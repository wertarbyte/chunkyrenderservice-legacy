/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util.video;

import com.wertarbyte.renderservice.api.Keyframe;
import com.wertarbyte.renderservice.util.spline.SplineInterpolator;
import com.wertarbyte.renderservice.util.spline.PolynomialSplineFunction;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Interpolation of camera positions and orientations based on {@link com.wertarbyte.renderservice.api.Keyframe}s.
 */
public class InterpolatedFrames {
    private final PolynomialSplineFunction x;
    private final PolynomialSplineFunction y;
    private final PolynomialSplineFunction z;
    private final PolynomialSplineFunction pitch;
    private final PolynomialSplineFunction yaw;
    private final double length;

    /**
     * Interpolates the given keyframes and creates a new InterpolatedFrames instance.
     *
     * @param keyframes keyframes
     */
    InterpolatedFrames(List<Keyframe> keyframes) {
        Collections.sort(keyframes, new Comparator<Keyframe>() {
            @Override
            public int compare(Keyframe a, Keyframe b) {
                return Double.compare(a.getTime(), b.getTime());
            }
        });
        this.length = keyframes.get(keyframes.size() - 1).getTime();

        double[] t = new double[keyframes.size()];
        double[] xValues = new double[keyframes.size()];
        double[] yValues = new double[keyframes.size()];
        double[] zValues = new double[keyframes.size()];
        double[] pitchValues = new double[keyframes.size()];
        double[] yawValues = new double[keyframes.size()];

        int i = 0;
        for (Keyframe frame : keyframes) {
            t[i] = frame.getTime();
            xValues[i] = frame.getX();
            yValues[i] = frame.getY();
            zValues[i] = frame.getZ();
            pitchValues[i] = frame.getPitch();
            yawValues[i] = frame.getYaw();
            i++;
        }

        SplineInterpolator interpolator = new SplineInterpolator();
        this.x = interpolator.interpolate(t, xValues);
        this.y = interpolator.interpolate(t, yValues);
        this.z = interpolator.interpolate(t, zValues);
        this.pitch = interpolator.interpolate(t, pitchValues);
        this.yaw = interpolator.interpolate(t, yawValues);
    }

    public double getX(double t) {
        return x.value(t);
    }

    public double getY(double t) {
        return y.value(t);
    }

    public double getZ(double t) {
        return z.value(t);
    }

    public double getPitch(double t) {
        return pitch.value(t);
    }

    public double getYaw(double t) {
        return yaw.value(t);
    }

    /**
     * Gets the length of the video.
     *
     * @return length of the video
     */
    public double getLength() {
        return length;
    }
}
