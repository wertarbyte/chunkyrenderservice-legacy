/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util.httpapi.exceptions;

public abstract class ApiException extends Exception {
    private boolean isServerSide;

    /**
     * Creates a new instance of this exception.
     *
     * @param message      a message to describe the reason
     * @param cause        exception that caused this exception
     * @param isServerSide whether the reason for this exception is server- or client-side
     */
    public ApiException(String message, Exception cause, boolean isServerSide) {
        super(message, cause);
        this.isServerSide = isServerSide;
    }

    /**
     * Creates a new instance of this exception.
     *
     * @param message      a message to describe the reason
     * @param isServerSide whether the reason for this exception is server- or client-side
     */
    public ApiException(String message, boolean isServerSide) {
        super(message);
        this.isServerSide = isServerSide;
    }

    /**
     * Checks if this is a server side exception (meaning that it's not the client's fault that it was thrown).
     *
     * @return true if it's the server's fault that this exception was thrown, false if not
     */
    public final boolean isServerSide() {
        return isServerSide;
    }
}
