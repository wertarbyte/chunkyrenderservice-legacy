/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util;

import java.util.ArrayList;
import java.util.List;

public class MinecraftUtil {
	/**
	 * Gets the chunk position at the given block position.
	 * 
	 * @param x
	 *            X-coordinate of a block position
	 * @param z
	 *            Z-coordinate of a block position
	 * @return two-dimensional array with x and z position (in that order) of
	 *         the chunk
	 */
	public static int[] getChunkAtPosition(int x, int z) {
		int[] chunk = new int[2];
		chunk[0] = (int) Math.floor(x / 16);
		chunk[1] = (int) Math.floor(z / 16);
		return chunk;
	}

	/**
	 * Gets a list with chunks around the given block position in the given
	 * radius of chunks.
	 * 
	 * @param x
	 *            X-coordinate of a block position
	 * @param z
	 *            Z-coordinate of a block position
	 * @param radius
	 *            radius of chunks that should be included (0 means that only
	 *            the chunk with the given position is included)
	 * @return List with two-dimensional arrays with x and z position (in that
	 *         order) of the chunks
	 */
	public static List<int[]> getChunksAround(int x, int z, int radius) {
		List<int[]> chunks = new ArrayList<int[]>();

		int[] centerChunk = getChunkAtPosition(x, z);
		
		for (int a = centerChunk[0] - radius; a <= centerChunk[0] + radius; a++){
			for (int b = centerChunk[1] - radius; b <= centerChunk[1] + radius; b++){
				chunks.add(new int[] { a, b });
			}
				
		}
		return chunks;
	}
}
