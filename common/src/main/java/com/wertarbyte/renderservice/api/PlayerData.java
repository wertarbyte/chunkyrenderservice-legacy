/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

import java.util.List;

/**
 * Information about a player.
 */
public class PlayerData {
    private List<RenderJob> jobs;

    public PlayerData(List<RenderJob> jobs) {
        this.jobs = jobs;
    }

    /**
     * Gets all jobs of this player.
     *
     * @return List with all jobs of this player
     */
    public List<RenderJob> getJobs() {
        return jobs;
    }

    /**
     * Gets the number of all running and pending jobs created by this player.
     *
     * @return number of running and pending jobs created by this player
     */
    public int getCurrentJobsCount() {
        int count = 0;
        for (RenderJob job : getJobs()) {
            if (job.getStatus() == RenderJobStatus.NEW ||
                    job.getStatus() == RenderJobStatus.READY)
                count++;
        }
        return count;
    }
}
