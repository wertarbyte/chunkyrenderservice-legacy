/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

import java.util.List;

/**
 * Configuration for a video job.
 */
public class VideoJobConfiguration extends JobConfiguration {
    private List<Keyframe> keyframes;

    public VideoJobConfiguration(String creator, String world, double x, double y, double z, double pitch, double yaw, int chunkRadius, Double focusDistance, String preset, RenderJobType type, List<Keyframe> keyframes) {
        super(creator, world, x, y, z, pitch, yaw, chunkRadius, focusDistance, preset, type);
        this.keyframes = keyframes;
    }

    public List<Keyframe> getKeyframes() {
        return keyframes;
    }

    public static VideoJobConfiguration fromJobConfiguration(JobConfiguration config, List<Keyframe> keyframes) {
        return new VideoJobConfiguration(config.getCreator(), config.getWorld(), config.getX(), config.getY(), config.getZ(),
                config.getPitch(), config.getYaw(), config.getChunkRadius(), config.getFocusDistance(), config.getPreset(),
                config.getType(), keyframes);
    }
}
