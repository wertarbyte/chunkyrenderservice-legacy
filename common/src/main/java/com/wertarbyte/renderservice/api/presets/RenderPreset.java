/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api.presets;

import com.wertarbyte.renderservice.api.presets.entities.Camera;
import com.wertarbyte.renderservice.api.presets.entities.PostprocessMethod;
import com.wertarbyte.renderservice.api.presets.entities.SkyMode;
import com.wertarbyte.renderservice.api.presets.entities.Sun;

/**
 * A rendering configuration for a job.
 */
public class RenderPreset {
    private String name;
    private Sun sun;
    private PostprocessMethod postprocess;
    private double exposure;
    private boolean emittersEnabled;
    private double emitterIntensity;
    private boolean sunEnabled;
    private boolean cloudsEnabled;
    private boolean stillWater;
    private boolean clearWater;
    private boolean biomeColorsEnabled;
    private double fogDensity;
    private String fogColor = null;
    private Camera camera;
    private double skyYaw;
    private boolean focusSupported = false;
    private SkyMode skyMode = SkyMode.SKYMAP_PANORAMIC;
    private double waterOpacity = 0.42;
    private String waterColor = null;
    private double waterVisibility = 9.0;
    private int waterHeight = 0;

    public RenderPreset() {
    }

    public RenderPreset(String name, Sun sun, PostprocessMethod postprocess, double exposure, boolean emittersEnabled, double emitterIntensity, boolean sunEnabled, boolean cloudsEnabled, boolean stillWater, boolean clearWater, boolean biomeColorsEnabled, double fogDensity, String fogColor, Camera camera, double skyYaw, boolean focusSupported, SkyMode skyMode, double waterOpacity, String waterColor, double waterVisibility, int waterHeight) {
        this.name = name;
        this.sun = sun;
        this.postprocess = postprocess;
        this.exposure = exposure;
        this.emittersEnabled = emittersEnabled;
        this.emitterIntensity = emitterIntensity;
        this.sunEnabled = sunEnabled;
        this.cloudsEnabled = cloudsEnabled;
        this.stillWater = stillWater;
        this.clearWater = clearWater;
        this.biomeColorsEnabled = biomeColorsEnabled;
        this.fogDensity = fogDensity;
        this.fogColor = fogColor;
        this.camera = camera;
        this.skyYaw = skyYaw;
        this.focusSupported = focusSupported;
        this.skyMode = skyMode;
        this.waterOpacity = waterOpacity;
        this.waterColor = waterColor;
        this.waterVisibility = waterVisibility;
        this.waterHeight = waterHeight;
    }

    public String getName() {
        return name;
    }

    public Sun getSun() {
        return sun;
    }

    public PostprocessMethod getPostprocess() {
        return postprocess;
    }

    public double getExposure() {
        return exposure;
    }

    public boolean isEmittersEnabled() {
        return emittersEnabled;
    }

    public double getEmitterIntensity() {
        return emitterIntensity;
    }

    public boolean isSunEnabled() {
        return sunEnabled;
    }

    public boolean isCloudsEnabled() {
        return cloudsEnabled;
    }

    public boolean isStillWater() {
        return stillWater;
    }

    public boolean isClearWater() {
        return clearWater;
    }

    public boolean isBiomeColorsEnabled() {
        return biomeColorsEnabled;
    }

    public double getFogDensity() {
        return fogDensity;
    }

    public String getFogColor() {
        return fogColor;
    }

    public Camera getCamera() {
        return camera;
    }

    public double getSkyYaw() {
        return skyYaw;
    }

    public boolean isFocusSupported() {
        return focusSupported;
    }

    public SkyMode getSkyMode() {
        return skyMode;
    }

    public double getWaterOpacity() {
        return waterOpacity;
    }

    public String getWaterColor() {
        return waterColor;
    }

    public double getWaterVisibility() {
        return waterVisibility;
    }

    public int getWaterHeight() {
        return waterHeight;
    }
}
