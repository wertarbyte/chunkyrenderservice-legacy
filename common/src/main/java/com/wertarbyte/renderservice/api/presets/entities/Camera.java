/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api.presets.entities;

/**
 * Camera (focus) preset configuration
 */
public class Camera {
    private double fov;
    private double dof;
    private boolean infDof;
    private double focalOffset;

    public Camera() {
    }

    public Camera(double fov, double dof, boolean infDof, double focalOffset) {
        this.fov = fov;
        this.dof = dof;
        this.infDof = infDof;
        this.focalOffset = focalOffset;

        if (Double.isInfinite(dof)) {
            this.dof = 0; //inf can't be serialized to JSON
        }
    }

    public double getFov() {
        return fov;
    }

    public double getDof() {
        return dof;
    }

    public boolean isInfDof() {
        return infDof;
    }

    public double getFocalOffset() {
        return focalOffset;
    }
}
