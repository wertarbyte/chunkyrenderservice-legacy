/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

import com.wertarbyte.renderservice.util.httpapi.JsonApiClient;
import com.wertarbyte.renderservice.util.httpapi.exceptions.ApiClientException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * Provides commonly used API calls.
 */
public abstract class CommonApiClient {
    public static final String URL = "http://s6.wertarbyte.com:9876";

    /**
     * HTTP API client to be used to do the API calls.
     */
    protected final JsonApiClient client;

    /**
     * Creates a new client for the Render Service API.
     */
    public CommonApiClient() {
        client = new RenderServiceJsonApiClient();
    }

    /**
     * Gets the render job with the given id, without render settings.
     *
     * @param id id of the render job to get
     * @return RenderJob or null if no render job with the given id exists
     * @throws ApiClientException if the request fails
     */
    public RenderJob getJobById(int id) throws ApiClientException {
        return client.getFromJson("/jobs/" + id, RenderJob.class);
    }

    /**
     * Gets information about a player.
     *
     * @param player Nickname of a player
     * @return Information about the player
     * @throws ApiClientException
     */
    public PlayerData getPlayerData(String player) throws ApiClientException {
        return client.getFromJson("/players/" + player, PlayerData.class);
    }

    public Versions getVersions() throws ApiClientException {
        return client.getFromJson("/versions", Versions.class);
    }

    /**
     * A API client that handles RenderService-specific headers.
     */
    private class RenderServiceJsonApiClient extends JsonApiClient {

        public RenderServiceJsonApiClient() {
            super(CommonApiClient.URL);
        }

        @Override
        protected void requestHook(HttpRequestBase request) {
            request.setHeader("User-Agent", CommonApiClient.this.getUserAgent());
        }

        @Override
        protected HttpResponse getResponse(String apiAddress, int timeout, String... parameters) throws ApiClientException {
            HttpResponse response = super.getResponse(apiAddress, timeout, parameters);
            checkDeprecated(response); //throw if deprecated
            return response;
        }

        @Override
        public HttpResponse put(String apiAddress, HttpEntity data, int timeout) throws ApiClientException {
            HttpResponse response = super.put(apiAddress, data, timeout);
            checkDeprecated(response); //throw if deprecated
            return response;
        }

        @Override
        public HttpResponse post(String apiAddress, HttpEntity data, int timeout) throws ApiClientException {
            HttpResponse response = super.post(apiAddress, data, timeout);
            checkDeprecated(response); //throw if deprecated
            return response;
        }

        /**
         * Checks if the given response includes a "x-renderservice: deprecated client" header and throws a
         * {@link DeprecatedApiClientException} if so.
         *
         * @param response response to check
         * @throws DeprecatedApiClientException if this client is deprecated according to the response
         */
        private void checkDeprecated(HttpResponse response) throws DeprecatedApiClientException {
            for (Header header : response.getHeaders("x-renderservice")) {
                if (header.getValue().equalsIgnoreCase("deprecated client")) {
                    throw new DeprecatedApiClientException("The server requires a newer client version.");
                }
            }
        }
    }

    /**
     * Gets the user agent to use for all requests.
     *
     * @return user agent for all requests
     */
    protected abstract String getUserAgent();
}
