/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api.account;

import java.util.Date;
import java.util.UUID;

/**
 * A transaction.
 */
public class Transaction {
    private int points;
    private UUID fromTo;
    private Integer jobId;
    private Date date;

    public Transaction(int points, int jobId, Date date) {
        this.points = points;
        this.jobId = jobId;
        this.date = date;
    }

    public Transaction(int points, UUID fromTo, Date date) {
        this.points = points;
        this.fromTo = fromTo;
        this.date = date;
    }

    public int getPoints() {
        return points;
    }

    public UUID getFromTo() {
        return fromTo;
    }

    public Integer getJobId() {
        return jobId;
    }

    public Date getDate() {
        return date;
    }
}
