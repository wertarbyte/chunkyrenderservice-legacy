/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

import java.util.Date;

/**
 * A render job.
 */
public class RenderJob {
    private int id;
    private String creator;
    private Date creationDate;
    private double x;
    private double y;
    private double z;
    private String world;
    private double pitch;
    private double yaw;
    private double focus;
    private boolean isFocusSet;
    private int chunkRadius;
    private String preset;
    private int width;
    private int height;
    private Date lastPicDate;
    private RenderJobStatus status;
    private int targetSpp;
    private RenderJobType type;

    public RenderJob(int id, String creator, Date createTime, String world, double x, double y, double z, double pitch, double yaw, double focus, boolean hasFocus, int chunkRadius, RenderJobStatus status, int targetSpp, String preset, Date lastPicTime, int width, int height, RenderJobType type) {
        this.id = id;
        this.creator = creator;
        this.creationDate = createTime;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
        this.focus = focus;
        this.isFocusSet = hasFocus;
        this.chunkRadius = chunkRadius;
        this.preset = preset;
        this.width = width;
        this.height = height;
        this.status = status;
        this.lastPicDate = lastPicTime;
        this.type = type;
    }

    /**
     * Gets the ID of this job.
     *
     * @return ID of this job
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the name of the player who created this job.
     *
     * @return name of the creator of this job
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Gets the creation date of this job.
     *
     * @return creation date of this job
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Gets the X coordinate of the camera's position.
     *
     * @return X coordinate of the camera's position
     */
    public double getX() {
        return x;
    }

    /**
     * Gets the Y coordinate of the camera's position.
     *
     * @return Y coordinate of the camera's position
     */
    public double getY() {
        return y;
    }

    /**
     * Gets the Z coordinate of the camera's position.
     *
     * @return Z coordinate of the camera's position
     */
    public double getZ() {
        return z;
    }

    /**
     * Gets the world this job was created in.
     *
     * @return world this job was created in
     */
    public String getWorld() {
        return world;
    }

    /**
     * Gets the camera's pitch.
     *
     * @return The camera's pitch
     */
    public double getPitch() {
        return pitch;
    }

    /**
     * Gets the camera's yaw.
     *
     * @return The camera's yaw
     */
    public double getYaw() {
        return yaw;
    }

    /**
     * Gets the radius of chunks to load when rendering this job.
     *
     * @return Radius of chunks to load when rendering this job
     */
    public int getChunkRadius() {
        return chunkRadius;
    }

    /**
     * Gets the preset to use when rendering this job.
     *
     * @return The preset to use when rendering this job
     */
    public String getPreset() {
        return preset;
    }

    /**
     * Gets the width of the picture of this job.
     *
     * @return the width of the picture of this job
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the picture of this job.
     *
     * @return the width of the picture of this job
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets the focus depth of this job
     *
     * @return the focus depth of this job
     */
    public double getFocus() {
        return focus;
    }

    /**
     * Checks if this job has a focus point set.
     *
     * @return true if this job has a focus point set, false if not
     */
    public boolean isFocusSet() {
        return isFocusSet;
    }

    /**
     * Gets the URL of this job on the website.
     *
     * @return URL of this job on the website
     */
    public String getUrl() {
        return "http://rs.wertarbyte.com/gallery/" + getId();
    }

    @Override
    public String toString() {
        return String.format("Job %d", getId());
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RenderJob) {
            RenderJob other = (RenderJob) obj;
            if (getId() == other.getId())
                return true;
        }
        return false;
    }

    /**
     * Gets the date of the last saving of a picture for this job.
     *
     * @return date when the last picture for this job was saved
     */
    public Date getLastPicDate() {
        return lastPicDate;
    }

    /**
     * Gets the current status of this job.
     *
     * @return current status
     */
    public RenderJobStatus getStatus() {
        return status;
    }

    /**
     * Gets the type of this job.
     *
     * @return type of this job
     */
    public RenderJobType getType() {
        return type;
    }

    /**
     * Gets the SPP target of this job.
     *
     * @return SPP target of this job
     */
    public int getTargetSpp() {
        return targetSpp;
    }

}
