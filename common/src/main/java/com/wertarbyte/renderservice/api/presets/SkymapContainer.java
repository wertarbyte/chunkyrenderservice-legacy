/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api.presets;

import com.wertarbyte.renderservice.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class SkymapContainer {
    private Format format;
    private InputStream inputStream;

    /**
     * Creates a new skymap container.
     *
     * @param format      Format of the skymap image
     * @param imageStream Stream of the skymap image
     */
    public SkymapContainer(Format format, InputStream imageStream) {
        this.format = format;
        this.inputStream = imageStream;
    }

    /**
     * Gets the format of the skymap.
     *
     * @return The format of the skymap
     */
    public Format getFormat() {
        return format;
    }

    /**
     * Saves the image into the given file. Notice that this can only be done once!
     *
     * @param file File to save the image into
     * @throws IOException If saving the file failed or the image was already saved into a file
     */
    public void saveTo(File file) throws IOException {
        FileUtil.writeFile(inputStream, file);
    }

    /**
     * Closes the input stream of the skymap. Only do this if you don't want to save the skymap!
     */
    public void dispose() {
        try {
            inputStream.close();
        } catch (IOException e) {
            //ignore
        }
    }

    /**
     * The file format of a skymap.
     */
    public enum Format {
        /**
         * Png file
         */
        Png,
        /**
         * Jpeg file
         */
        Jpeg;

        public String getFileExtension() {
            switch (this) {
                case Png:
                    return ".png";
                case Jpeg:
                    return ".jpg";
            }
            throw new RuntimeException();
        }
    }
}
