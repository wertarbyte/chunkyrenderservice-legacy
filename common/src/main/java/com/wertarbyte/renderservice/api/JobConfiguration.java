/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

/**
 * The configuration of a job.
 */
public class JobConfiguration {
    private String creator;
    private String world;
    private double x;
    private double y;
    private double z;
    private double pitch;
    private double yaw;
    private int chunkRadius;
    private Double focusDistance;
    private String preset;
    private RenderJobType type;

    public JobConfiguration(String creator, String world, double x, double y, double z, double pitch, double yaw, int chunkRadius, Double focusDistance, String preset, RenderJobType type) {
        this.creator = creator;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
        this.chunkRadius = chunkRadius;
        this.focusDistance = focusDistance;
        this.preset = preset;
        this.type = type;
    }

    public String getCreator() {
        return creator;
    }

    public String getWorld() {
        return world;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getPitch() {
        return pitch;
    }

    public double getYaw() {
        return yaw;
    }

    public int getChunkRadius() {
        return chunkRadius;
    }

    public Double getFocusDistance() {
        return focusDistance;
    }

    public String getPreset() {
        return preset;
    }

    public RenderJobType getType() {
        return type;
    }
}
