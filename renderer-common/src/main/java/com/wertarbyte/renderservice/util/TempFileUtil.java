/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.util;

import com.wertarbyte.renderservice.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Utility methods for temporary {@link java.io.File}s.
 */
public class TempFileUtil {
    private TempFileUtil() {
    }

    /**
     * Puts the given input stream into a new temporary file. The file is automatically deleted on exit.
     *
     * @param inputStream input stream to read from
     * @return temporary file with the content of the input stream written into it
     * @throws java.io.IOException if creating the temporary file or reading the input stream fails
     */
    public static File extractTemporary(InputStream inputStream) throws IOException {
        File temp = File.createTempFile("tmp", null);
        temp.deleteOnExit();
        FileUtil.writeFile(inputStream, temp);
        return temp;
    }

    /**
     * Creates a temporary directory that will automatically be deleted on exit.
     *
     * @return a temporary directory
     * @throws java.io.IOException if creating the temporary directory fails
     */
    public static File createTemporaryDirectory() throws IOException {
        final File directory = Files.createTempDirectory(null).toFile();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                FileUtil.deleteDirectory(directory);
            }
        }));
        return directory;
    }
}
