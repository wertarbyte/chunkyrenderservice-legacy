package com.wertarbyte.renderservice.util.http;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * An input stream that counts the transferred bytes.
 */
public class CountingInputStream extends FilterInputStream {
    private final TransferProgressListener listener;
    private long transferred;
    private final long size;

    public CountingInputStream(InputStream in, TransferProgressListener listener, long size) {
        super(in);
        this.listener = listener;
        this.transferred = 0;
        this.size = size;
    }

    @Override
    public int read() throws IOException {
        int readByte = in.read();
        if (readByte != -1) {
            listener.transferred(++transferred, size);
        }
        return readByte;
    }
}
