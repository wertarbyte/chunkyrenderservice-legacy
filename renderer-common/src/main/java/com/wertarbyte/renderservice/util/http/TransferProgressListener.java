package com.wertarbyte.renderservice.util.http;

/**
 * A listener for byte transfering processes (i.e. upload or download).
 */
public interface TransferProgressListener {
    /**
     * Gets called when the progress changes.
     *
     * @param transferredBytes number of total transferred bytes
     * @param totalBytes       total bytes to transfer, -1 if unknown
     */
    void transferred(long transferredBytes, long totalBytes);
}
