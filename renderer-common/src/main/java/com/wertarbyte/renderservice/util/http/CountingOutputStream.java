package com.wertarbyte.renderservice.util.http;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * An output stream that counts the transferred bytes.
 */
class CountingOutputStream extends FilterOutputStream {
    private final TransferProgressListener listener;
    private long transferred;
    private long size;

    CountingOutputStream(final OutputStream out, final TransferProgressListener listener, long size) {
        super(out);
        this.listener = listener;
        this.transferred = 0;
        this.size = size;
    }

    @Override
    public void write(final byte[] b, final int off, final int len) throws IOException {
        //// NO, double-counting, as super.write(byte[], int, int) delegates to write(int).
        //super.write(b, off, len);
        out.write(b, off, len);
        this.transferred += len;
        this.listener.transferred(this.transferred, size);
    }

    @Override
    public void write(final int b) throws IOException {
        out.write(b);
        this.transferred++;
        this.listener.transferred(this.transferred, size);
    }

}
