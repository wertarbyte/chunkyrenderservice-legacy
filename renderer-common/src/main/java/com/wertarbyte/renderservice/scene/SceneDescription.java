/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.scene;

import com.google.gson.JsonObject;
import com.wertarbyte.renderservice.api.presets.RenderPreset;
import com.wertarbyte.renderservice.libchunky.scene.Color;
import com.wertarbyte.renderservice.libchunky.scene.SkyMode;

public class SceneDescription extends com.wertarbyte.renderservice.libchunky.scene.SceneDescription {
    public SceneDescription() {
    }

    public SceneDescription(JsonObject jsonObject) {
        super(jsonObject);
    }

    /**
     * Applies the configuration of the given preset on these settings.
     *
     * @param preset Preset to apply on these settings
     */
    public void applyPreset(RenderPreset preset) {
        setExposure(preset.getExposure());

        switch (preset.getPostprocess()) {
            case GAMMA_CORRECTION:
                setPostProcess(com.wertarbyte.renderservice.libchunky.scene.PostprocessMethod.GammaCorrection);
                break;
            case TONE_MAP:
                setPostProcess(com.wertarbyte.renderservice.libchunky.scene.PostprocessMethod.ToneMap);
                break;
            case NONE:
            default:
                setPostProcess(com.wertarbyte.renderservice.libchunky.scene.PostprocessMethod.None);
                break;
        }

        setEmittersEnabled(preset.isEmittersEnabled());
        setEmitterIntensity(preset.getEmitterIntensity());
        setSunEnabled(preset.isSunEnabled());

        setStillWater(preset.isStillWater());
        setWaterOpacity(preset.getWaterOpacity());
        if (preset.getWaterColor() != null) {
            setWaterColor(new Color(preset.getWaterColor()));
        }
        setWaterVisibility(preset.getWaterVisibility());
        setWaterHeight(preset.getWaterHeight());

        setBiomeColorsEnabled(preset.isBiomeColorsEnabled());

        if (preset.getFogColor() != null) {
            setFogColor(new Color(preset.getFogColor()));
        }
        setFogDensity(preset.getFogDensity());

        getCamera().setFov(preset.getCamera().getFov());
        getCamera().setDof(preset.getCamera().isInfDof() ? Double.POSITIVE_INFINITY : preset.getCamera().getDof());

        getSun().setAltitude(preset.getSun().getAltitude());
        getSun().setAzimuth(preset.getSun().getAzimuth());
        getSun().setIntensity(preset.getSun().getIntensity());
        if (preset.getSun().getColor() != null) {
            getSun().setColor(new Color(preset.getSun().getColor()));
        }

        getSky().setCloudsEnabled(preset.isCloudsEnabled());
        getSky().setSkyYaw(preset.getSkyYaw());

        switch (preset.getSkyMode()) {
            case SKYMAP_PANORAMIC:
                getSky().setMode(SkyMode.SKYMAP_PANORAMIC);
                break;
            case SKYMAP_SPHERICAL:
                getSky().setMode(SkyMode.SKYMAP_SPHERICAL);
                break;
            case SIMULATED:
                getSky().setMode(SkyMode.SIMULATED);
                break;
        }
    }
}
