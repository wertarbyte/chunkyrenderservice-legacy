/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

import java.util.UUID;

/**
 * An assignment for a renderer.
 */
public class Assignment {
    private final UUID id;
    private final UUID renderer;
    private final RenderTask task;
    private final int spp;
    private int currentSpp = 0;

    public Assignment(UUID id, UUID renderer, RenderTask task, int spp) {
        this.id = id;
        this.renderer = renderer;
        this.task = task;
        this.spp = spp;
    }

    /**
     * Gets the unique ID of this assignment.
     *
     * @return unique ID of this assignment
     */
    public UUID getId() {
        return id;
    }

    /**
     * Gets the unique ID of the renderer this assignment is assigned to.
     *
     * @return the unique ID of the renderer this assignment is assigned to
     */
    public UUID getRendererId() {
        return renderer;
    }

    public int getSpp() {
        return spp;
    }

    public int getCurrentSpp() {
        return currentSpp;
    }

    public void setCurrentSpp(int currentSpp) {
        this.currentSpp = currentSpp;
    }

    public RenderTask getTask() {
        return task;
    }

    @Override
    public String toString() {
        return "Assignment " + id + " (Task " + task.getId() + ")";
    }
}