/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

import java.util.Date;

/**
 * A render task. This contains everything that is required to render something.
 */
public class RenderTask {
    private int id;
    private int jobId;
    private double x;
    private double y;
    private double z;
    private String world;
    private double pitch;
    private double yaw;
    private double focus;
    private boolean isFocusSet;
    private int chunkRadius;
    private String preset;
    private int width;
    private int height;
    private Status status;
    private int targetSpp;
    private int currentSpp;
    private Date lastPicDate;

    public RenderTask(int id, int jobId, double x, double y, double z, String world, double pitch, double yaw, double focus, boolean isFocusSet, int chunkRadius, String preset, int width, int height, Status status, int targetSpp, int currentSpp, Date lastPicDate) {
        this.id = id;
        this.jobId = jobId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
        this.pitch = pitch;
        this.yaw = yaw;
        this.focus = focus;
        this.isFocusSet = isFocusSet;
        this.chunkRadius = chunkRadius;
        this.preset = preset;
        this.width = width;
        this.height = height;
        this.status = status;
        this.targetSpp = targetSpp;
        this.currentSpp = currentSpp;
        this.lastPicDate = lastPicDate;
    }

    public int getId() {
        return id;
    }

    public int getJobId() {
        return jobId;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String getWorld() {
        return world;
    }

    public double getPitch() {
        return pitch;
    }

    public double getYaw() {
        return yaw;
    }

    public double getFocus() {
        return focus;
    }

    public boolean isFocusSet() {
        return isFocusSet;
    }

    public int getChunkRadius() {
        return chunkRadius;
    }

    public String getPreset() {
        return preset;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Status getStatus() {
        return status;
    }

    public int getTargetSpp() {
        return targetSpp;
    }

    public int getCurrentSpp() {
        return currentSpp;
    }

    public Date getLastPicDate() {
        return lastPicDate;
    }

    @Override
    public String toString() {
        return "Task " + getId() + " (Job " + getJobId() + ")";
    }

    public enum Status {
        /**
         * The task is new and needs to be initialized.
         */
        NEW,
        /**
         * The task is ready to be rendered.
         */
        READY,
        /**
         * The task is finished (i.e. reached the spp target).
         */
        FINISHED,
        /**
         * The task was canceled.
         */
        CANCELED
    }
}
