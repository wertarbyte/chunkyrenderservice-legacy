/*
 * Copyright (c) 2013-2015 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of Wertarbyte RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.api;

/**
 * Renderer statistics.
 */
public class RendererInfo {
    private final double samplesPerSecond;
    private final int runningProcesses;
    private final int runningThreads;
    private final int unusedProcesses;

    public RendererInfo(double samplesPerSecond, int runningProcesses, int runningThreads, int unusedProcesses) {
        this.samplesPerSecond = samplesPerSecond;
        this.runningProcesses = runningProcesses;
        this.runningThreads = runningThreads;
        this.unusedProcesses = unusedProcesses;
    }

    public double getSamplesPerSecond() {
        return samplesPerSecond;
    }

    public int getRunningProcesses() {
        return runningProcesses;
    }

    public int getRunningThreads() {
        return runningThreads;
    }

    public int getUnusedProcesses() {
        return unusedProcesses;
    }
}
